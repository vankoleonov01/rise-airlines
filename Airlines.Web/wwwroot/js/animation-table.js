﻿document.addEventListener("DOMContentLoaded", function () {
    const table = document.querySelector('table');
    const rows = table.querySelectorAll('tbody tr');
    const toggleButton = document.getElementById('toggleButton');
    const tableSection = document.getElementById('tableSection');
    const formSection = document.getElementById('formSection');
    const showMoreButton = document.getElementById('showMoreButton');
    let additionalRowsVisible = false;

    toggleButton.addEventListener('click', function () {
        if (tableSection.style.display === 'none') {
            tableSection.style.display = 'block';
            formSection.style.display = 'none';
            toggleButton.textContent = 'Show Form';
        } else {
            tableSection.style.display = 'none';
            formSection.style.display = 'block';
            toggleButton.textContent = 'Show Table';
        }
    });

    function showHideRows() {
        for (let i = 3; i < rows.length; i++) {
            if (!additionalRowsVisible) {
                rows[i].style.display = 'none';
                showMoreButton.textContent = 'Show More';
            } else {
                rows[i].style.display = '';
                showMoreButton.textContent = 'Show Less';
            }
        }
        additionalRowsVisible = !additionalRowsVisible;
    }

    window.addEventListener('load', function () {
        if (rows.length > 3) {
            showMoreButton.addEventListener('click', showHideRows);
            showHideRows();
        }
    });
});