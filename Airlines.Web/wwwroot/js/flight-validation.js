﻿document.addEventListener("DOMContentLoaded", function () {
    const form = document.querySelector('.form');
    const submitButton = document.getElementById('submit-form-button');
    const fromAirportInput = document.getElementById('flight-from-airport');
    const toAirportInput = document.getElementById('flight-to-airport');
    const flightNumberInput = document.getElementById('flight-number');
    const departureDateInput = document.getElementById('flight-departure-datetime');
    const arrivalDateInput = document.getElementById('flight-arrival-datetime');
    const flightNumberError = document.getElementById('flight-number-error');
    const fromAirportError = document.getElementById('from-airport-error');
    const toAirportError = document.getElementById('to-airport-error');
    const departureDateError = document.getElementById('departure-date-error');
    const arrivalDateError = document.getElementById('arrival-date-error');
    const now = new Date();

    function hideAllErrors() {
        flightNumberError.style.display = 'none';
        fromAirportError.style.display = 'none';
        toAirportError.style.display = 'none';
        departureDateError.style.display = 'none';
        arrivalDateError.style.display = 'none';
        submitButton.disabled = true;
    }
    function validateForm() {
        let isFlightNumberValid = true;
        let isFromAirportValid = true;
        let isToAirportValid = true;
        let isDepartureDateValid = true;
        let isArrivalDateValid = true;

        if ((flightNumberInput.value.trim() === '')) {
            flightNumberError.textContent = "Flight number is required.";
            flightNumberError.style.display = 'block';
            isFlightNumberValid = false;
        }
        else if ((flightNumberInput.value.length > 10)) {
            flightNumberError.textContent = "Flight number must be less than 10 characters.";
            flightNumberError.style.display = 'block';
            isFlightNumberValid = false;
        }
        else {
            flightNumberError.style.display = 'none';
        }

        if (fromAirportInput.value === '') {
            fromAirportError.textContent = "Departure Airport is required.";
            fromAirportError.style.display = 'block';
            isFromAirportValid = false;
        }
        else if (fromAirportInput.value.length > 3) {
            fromAirportError.textContent = "Airport code must be 3 characters.";
            fromAirportError.style.display = 'block';
            isFromAirportValid = false;
        }
        else {
            fromAirportError.style.display = 'none';
        }

        if (toAirportInput.value === '') {
            toAirportError.textContent = "Destination Airport is required.";
            toAirportError.style.display = 'block';
            isToAirportValid = false; s
        }
        else if (toAirportInput.value.length > 3) {
            toAirportError.textContent = "Airport code must be 3 characters.";
            toAirportError.style.display = 'block';
            isToAirportValid = false;
        }
        else {
            toAirportError.style.display = 'none';
        }

        if (departureDateInput.value === '') {
            departureDateError.textContent = "Departure date is required.";
            departureDateError.style.display = 'block';
            isDepartureDateValid = false;
        }
        else if (departureDateInput.value >= arrivalDateInput.value) {
            departureDateError.textContent = "Departure date must be before arrival date.";
            departureDateError.style.display = 'block';
            isDepartureDateValid = false;
        }
        else {
            departureDateError.style.display = 'none';
        }

        if (arrivalDateInput.value === '') {
            arrivalDateError.textContent = "Arrival date is required.";
            arrivalDateError.style.display = 'block';
            isArrivalDateValid = false;
        }
        else if (new Date(departureDateInput.value) < now || new Date(arrivalDateInput.value) < now) {
            arrivalDateError.textContent = "Departure and Arrival dates must be in the present or future.";
            arrivalDateError.style.display = 'block';
            isArrivalDateValid = false;
        }
        else {
            arrivalDateError.style.display = 'none';
        }

        submitButton.disabled = !(isFlightNumberValid && isFromAirportValid && isToAirportValid && isArrivalDateValid && isDepartureDateValid);
    }

    flightNumberInput.addEventListener('input', validateForm);
    fromAirportInput.addEventListener('input', validateForm);
    toAirportInput.addEventListener('input', validateForm);
    departureDateInput.addEventListener('input', validateForm);
    arrivalDateInput.addEventListener('input', validateForm);

    submitButton.onclick = function (event) {
        validateForm();
        if (!submitButton.disabled) {
            return true;
        } else {
            event.preventDefault();
            return false;
        }
    };
    hideAllErrors();
});