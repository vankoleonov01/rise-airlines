﻿document.addEventListener("DOMContentLoaded", function () {
    const form = document.querySelector('.form');
    const submitButton = document.getElementById('submit-form-button');
    const airlineNameInput = document.getElementById('airline-name');
    const airlineFoundedInput = document.getElementById('airline-founded');
    const airlineFleetSizeInput = document.getElementById('airline-fleet-size');
    const airlineDescriptionInput = document.getElementById('airline-description');

    const airlineNameError = document.getElementById('airline-name-error');
    const foundedError = document.getElementById('airline-founded-error');
    const fleetSizeError = document.getElementById('airline-fleet-size-error');
    const descriptionError = document.getElementById('airline-description-error');
    const now = new Date();

    function hideAllErrors() {
        airlineNameError.style.display = 'none';
        foundedError.style.display = 'none';
        fleetSizeError.style.display = 'none';
        descriptionError.style.display = 'none';
        submitButton.disabled = true;
    }

    function validateForm() {
        let isNameValid = true;
        let isFoundedValid = true;
        let isFleetSizeValid = true;
        let isDescriptionValid = true;

        if (airlineNameInput.value.trim() === '') {
            airlineNameError.textContent = "Airline name is required.";
            airlineNameError.style.display = 'block';
            isNameValid = false;
        } else if (airlineNameInput.value.length > 50) {
            airlineNameError.textContent = "Airline name must be less than 50 characters.";
            airlineNameError.style.display = 'block';
            isNameValid = false;
        } else {
            airlineNameError.style.display = 'none';
        }

        if (airlineFoundedInput.value.trim() === '') {
            foundedError.textContent = "Founded date is required.";
            foundedError.style.display = 'block';
            isFoundedValid = false;
        } else if (new Date(airlineFoundedInput.value) > now) {
            foundedError.textContent = "Founded date cannot be in the future.";
            foundedError.style.display = 'block';
            isFoundedValid = false;
        } else {
            foundedError.style.display = 'none';
        }

        if (airlineFleetSizeInput.value.trim() === '') {
            fleetSizeError.textContent = "Fleet size is required.";
            fleetSizeError.style.display = 'block';
            isFleetSizeValid = false;
        } else if (airlineFleetSizeInput.value < 0) {
            fleetSizeError.textContent = "Fleet size must be greater than 0.";
            fleetSizeError.style.display = 'block';
            isFleetSizeValid = false;
        } else {
            fleetSizeError.style.display = 'none';
        }

        if (airlineDescriptionInput.value.trim() === '') {
            descriptionError.textContent = "Description is required.";
            descriptionError.style.display = 'block';
            isDescriptionValid = false;
        } else if (airlineDescriptionInput.value.length > 500) {
            descriptionError.textContent = "Description must be less than 500 characters.";
            descriptionError.style.display = 'block';
            isDescriptionValid = false;
        } else {
            descriptionError.style.display = 'none';
        }

        submitButton.disabled = !(isNameValid && isFoundedValid && isFleetSizeValid && isDescriptionValid);
    }

    airlineNameInput.addEventListener('input', validateForm);
    airlineFoundedInput.addEventListener('input', validateForm);
    airlineFleetSizeInput.addEventListener('input', validateForm);
    airlineDescriptionInput.addEventListener('input', validateForm);

    submitButton.onclick = function (event) {
        validateForm();
        if (!submitButton.disabled) {
            return true;
        } else {
            event.preventDefault();
            return false;
        }
    };

    hideAllErrors();
});