using Airlines.Business.Mapping;
using Airlines.Business.Services;
using Airlines.Business.Services.Interfaces;
using Airlines.Persistence.InMemory.Entities;
using Airlines.Persistence.InMemory.EntitiesRepository;
using Airlines.Persistence.InMemory.EntitiesRepository.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Airlines.Web;
public static class Program
{
    public static void Main(string[] args)
    {
        var builder = WebApplication.CreateBuilder(args);

        ConfigureServices(builder.Services);

        var app = builder.Build();

        if (!app.Environment.IsDevelopment())
        {
            _ = app.UseExceptionHandler("/Home/Error");
            _ = app.UseHsts();
        }

        _ = app.UseHttpsRedirection();
        _ = app.UseStaticFiles();

        _ = app.UseRouting();

        _ = app.UseAuthorization();

        _ = app.MapControllerRoute(
            name: "default",
            pattern: "{controller=Home}/{action=Index}/{id?}");

        app.Run();
    }

    private static void ConfigureServices(IServiceCollection services)
    {
        _ = services.AddControllersWithViews();

        _ = services.AddScoped<IAirportRepository, AirportRepository>();
        _ = services.AddScoped<IAirportService, AirportService>();
        _ = services.AddAutoMapper(typeof(AirportMapper));

        _ = services.AddDbContext<RiseAirlinesDBContext>(options =>
            options.UseSqlServer(Persistence.InMemory.Configuration.ConfigurationManager.GetConnectionString("DefaultConnection")));

        _ = services.AddScoped<IAirlineRepository, AirlineRepository>();
        _ = services.AddScoped<IAirlineService, AirlineService>();
        _ = services.AddAutoMapper(typeof(AirlineMapper));

        _ = services.AddScoped<IFlightRepository, FlightRepository>();
        _ = services.AddScoped<IFlightService, FlightService>();
        _ = services.AddAutoMapper(typeof(FlightMapper));
    }
}