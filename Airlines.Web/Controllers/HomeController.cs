using Airlines.Web.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using Airlines.Business.Services.Interfaces;

namespace Airlines.Web.Controllers;

public class HomeController(
    IAirportService airportService,
    IAirlineService airlineService,
    IFlightService flightService)
    : Controller
{
    public async Task<IActionResult> Index()
    {
        var airportsCount = await airportService.GetTotalAirportsCount();
        var airlinesCount = await airlineService.GetTotalAirlinesCount();
        var flightsCount = await flightService.GetTotalFlightsCount();

        ViewBag.AirportsCount = airportsCount;
        ViewBag.AirlinesCount = airlinesCount;
        ViewBag.FlightsCount = flightsCount;

        return View();
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error() => View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
}