﻿using Airlines.Business.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Airport = Airlines.Business.Dto.AirportDto;

namespace Airlines.Web.Controllers;

public class AirportController(IAirportService airportService) : Controller
{
    private const int _defaultPage = 1;
    private const int _pageSize = 10;
    public async Task<IActionResult> Index(int page = _defaultPage)
    {
        var totalAirports = await airportService.GetTotalAirportsCount();
        var totalPages = (int)Math.Ceiling((double)totalAirports / _pageSize);
        page = Math.Max(1, Math.Min(page, totalPages));
        var airports = await airportService.GetAirports(page, _pageSize);

        ViewBag.TotalPages = totalPages;
        ViewBag.CurrentPage = page;
        return View(airports);
    }


    [HttpGet]
    public IActionResult Create() => View();

    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Create([Bind("Name, Country, City, Code, RunwaysCount, Founded")] Airport airport)
    {
        try
        {
            if (ModelState.IsValid)
            {
                await airportService.AddAirport(airport);
                return RedirectToAction("Index");
            }
        }
        catch (Exception)
        {
            ModelState.AddModelError(string.Empty, "An error occurred while creating the airport.");
        }

        return View(airport);
    }

    [HttpGet]
    public async Task<IActionResult> Edit(int id)
    {
        var airport = await airportService.GetAirportByIdAsync(id);
        return airport == null ? NotFound() : View(airport);
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Edit(int id, [Bind("AirportId, Name, Country, City, Code, RunwaysCount, Founded")] Airport airport)
    {
        if (id != airport.AirportId)
        {
            return NotFound();
        }

        if (ModelState.IsValid)
        {
            var existingAirport = await airportService.GetAirportByIdAsync(id);
            if (existingAirport == null)
            {
                return NotFound();
            }
            await airportService.UpdateAirport(airport);
            return RedirectToAction("Index");
        }
        return View(airport);
    }

    [HttpPost]
    public async Task<IActionResult> DeleteSelected(int[]? selectedAirports)
    {
        if (selectedAirports != null && selectedAirports.Length > 0)
        {
            foreach (int id in selectedAirports)
            {
                await airportService.DeleteAirport(id);
            }
            return RedirectToAction("Index");
        }
        else
        {
            return RedirectToAction("Index");
        }
    }

    [HttpPost]
    public async Task<IActionResult> DeleteConfirmed(int id)
    {
        await airportService.DeleteAirport(id);
        return RedirectToAction("Index");
    }

    [HttpPost]
    public async Task<IActionResult> SearchAndFilter(string filterCriteria, string filterValue, DateOnly? founded)
    {
        var airports = await airportService.SearchAndFilterAirports(filterCriteria, filterValue, founded);
        return View("Index", airports);
    }
}