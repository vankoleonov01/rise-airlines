﻿using Microsoft.AspNetCore.Mvc;
using Flight = Airlines.Business.Dto.FlightDto;
using Airlines.Business.Services.Interfaces;
using Airlines.Business.Services;

namespace Airlines.Web.Controllers;

public class FlightController(IFlightService flightService) : Controller
{
    private const int _defaultPage = 1;
    private const int _pageSize = 10;
    public async Task<IActionResult> Index(int page = _defaultPage)
    {
        var totalFlights = await flightService.GetTotalFlightsCount();
        var totalPages = (int)Math.Ceiling((double)totalFlights / _pageSize);
        page = Math.Max(1, Math.Min(page, totalPages));
        var flights = await flightService.GetFlights(page, _pageSize);

        ViewBag.TotalPages = totalPages;
        ViewBag.CurrentPage = page;
        return View(flights);
    }

    [HttpGet]
    public IActionResult Create() => View();

    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Create([Bind("FlightNumber, FromAirport, ToAirport, DepartureDateTime, ArrivalDateTime")] Flight flight)
    {
        try
        {
            if (ModelState.IsValid)
            {
                await flightService.AddFlight(flight);
                return RedirectToAction("Index");
            }
        }
        catch (Exception)
        {
            ModelState.AddModelError(string.Empty, "An error occurred while creating the flight.");
        }

        return View(flight);
    }

    [HttpGet]
    public async Task<IActionResult> Edit(int id)
    {
        var flight = await flightService.GetFlightByIdAsync(id);
        return flight == null ? NotFound() : View(flight);
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Edit(int id, [Bind("FlightId,FlightNumber,FromAirport,ToAirport,DepartureDateTime,ArrivalDateTime")] Flight flight)
    {
        if (id != flight.FlightId)
        {
            return NotFound();
        }

        if (ModelState.IsValid)
        {
            var existingFlight = await flightService.GetFlightByIdAsync(id);
            if (existingFlight == null)
            {
                return NotFound();
            }
            await flightService.UpdateFlight(flight);
            return RedirectToAction("Index");
        }

        return View(flight);
    }

    [HttpPost]
    public async Task<IActionResult> DeleteSelected(int[]? selectedFlights)
    {
        if (selectedFlights != null && selectedFlights.Length > 0)
        {
            foreach (int id in selectedFlights)
            {
                await flightService.DeleteFlight(id);
            }
            return RedirectToAction("Index");
        }
        else
        {
            return RedirectToAction("Index");
        }
    }

    [HttpPost]
    public async Task<IActionResult> DeleteConfirmed(int id)
    {
        await flightService.DeleteFlight(id);
        return RedirectToAction("Index");
    }

    [HttpPost]
    public async Task<IActionResult> SearchAndFilter(string filterCriteria, string filterValue, DateTime? departureDate, DateTime? arrivalDate)
    {
        var flights = await flightService.SearchAndFilterFlights(filterCriteria, filterValue, departureDate, arrivalDate);
        return View("Index", flights);
    }
}