﻿using Microsoft.AspNetCore.Mvc;
using Airline = Airlines.Business.Dto.AirlineDto;
using Airlines.Business.Services.Interfaces;

namespace Airlines.Web.Controllers;

public class AirlineController(IAirlineService airlineService) : Controller
{
    private const int _defaultPage = 1;
    private const int _pageSize = 10;

    public async Task<IActionResult> Index(int page = _defaultPage)
    {
        var totalAirlines = await airlineService.GetTotalAirlinesCount();
        var totalPages = (int)Math.Ceiling((double)totalAirlines / _pageSize);
        page = Math.Max(1, Math.Min(page, totalPages));
        var airlines = await airlineService.GetAirlines(page, _pageSize);

        ViewBag.TotalPages = totalPages;
        ViewBag.CurrentPage = page;
        return View(airlines);
    }

    [HttpGet]
    public IActionResult Create() => View();

    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Create([Bind("Name, Founded, FleetSize, Description")] Airline airline)
    {
        try
        {
            if (ModelState.IsValid)
            {
                _ = await airlineService.AddAirline(airline);
                return RedirectToAction("Index");
            }
        }
        catch (Exception)
        {
            ModelState.AddModelError(string.Empty, "An error occurred while creating the airline.");
        }

        return View(airline);
    }

    [HttpGet]
    public async Task<IActionResult> Edit(int id)
    {
        var airline = await airlineService.GetAirlineByIdAsync(id);
        return airline == null ? NotFound() : View(airline);
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Edit(int id, [Bind("AirlineId,Name,Founded,FleetSize,Description")] Airline airline)
    {
        if (id != airline.AirlineId)
        {
            return NotFound();
        }

        if (ModelState.IsValid)
        {
            var existingAirline = await airlineService.GetAirlineByIdAsync(id);
            if (existingAirline == null)
            {
                return NotFound();
            }
            await airlineService.UpdateAirline(airline);
            return RedirectToAction("Index");
        }
        return View(airline);
    }

    [HttpPost]
    public async Task<IActionResult> DeleteSelected(int[]? selectedAirlines)
    {
        if (selectedAirlines != null && selectedAirlines.Length > 0)
        {
            foreach (int id in selectedAirlines)
            {
                await airlineService.DeleteAirline(id);
            }
            return RedirectToAction("Index");
        }
        else
        {
            return RedirectToAction("Index");
        }
    }

    [HttpPost]
    public async Task<IActionResult> DeleteConfirmed(int id)
    {
        await airlineService.DeleteAirline(id);
        return RedirectToAction("Index");
    }

    [HttpPost]
    public async Task<IActionResult> SearchAndFilter(string filterCriteria, string filterValue, DateOnly? founded)
    {
        var airlines = await airlineService.SearchAndFilterAirlines(filterCriteria, filterValue, founded);
        return View("Index", airlines);
    }
}