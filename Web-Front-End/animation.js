﻿document.addEventListener("DOMContentLoaded", () => {
    const mainDiv = document.querySelector("main div");
    const tableSection = document.getElementById("tableSection");
    const goUpButton = createGoUpButton();
    const goUpTableButton = createGoUpTableButton();
    const goDownButton = createGoDownButton();
    const fadeInSection = document.createElement("section");
    fadeInSection.classList.add("fade-in-section");
    mainDiv.appendChild(fadeInSection);
    fadeInSection.style.opacity = 0;
    fadeInSection.style.transition = "opacity 1s ease-in-out";
    setTimeout(() => {
        fadeInSection.style.opacity = 1;
    }, 100);

    mainDiv.appendChild(goUpButton);
    mainDiv.appendChild(goDownButton);
    tableSection.appendChild(goUpTableButton);

    goUpButton.addEventListener("click", scrollToTop);
    goDownButton.addEventListener("click", scrollToBottom);
    window.addEventListener("scroll", toggleGoUpButtonVisibility);
    window.addEventListener("scroll", toggleGoDownButtonVisibility);
    tableSection.addEventListener('scroll', toggleGoUpTableButtonVisibility);
    goUpTableButton.addEventListener('click', scrollToTopTable);

    function toggleGoUpTableButtonVisibility() {
        const scrollPosition = tableSection.scrollTop;
        const scrollThreshold = 20;

        if (scrollPosition > scrollThreshold) {
            goUpTableButton.style.display = "block";
        } else {
            goUpTableButton.style.display = "none";
        }
    }

    function scrollToTopTable() {
        tableSection.scrollTop = 0;
    }

    function createGoUpButton() {
        const button = document.createElement("button");
        button.textContent = "Go Up";
        button.id = "goUpButton";
        button.style.display = "none";
        return button;
    }

    function createGoUpTableButton() {
        const button = document.createElement("button");
        button.textContent = "Go Up";
        button.id = "goUpTableButton";
        button.style.display = "none";
        return button;
    }

    function createGoDownButton() {
        const button = document.createElement("button");
        button.textContent = "Go Down";
        button.id = "goDownButton";
        button.style.display = "none";
        return button;
    }

    function scrollToTop() {
        window.scrollTo({
            top: 0,
            behavior: 'smooth'
        });
    }

    function scrollToBottom() {
        window.scrollTo({
            top: document.body.scrollHeight,
            behavior: 'smooth'
        });
    }

    function toggleGoUpButtonVisibility() {
        const scrollPosition = window.scrollY || document.documentElement.scrollTop;
        if (scrollPosition > 20) {
            goUpButton.style.display = "block";
        } else {
            goUpButton.style.display = "none";
        }
    }

    function toggleGoDownButtonVisibility() {
        const scrollPosition = window.scrollY || document.documentElement.scrollTop;
        if (scrollPosition < document.body.scrollHeight - window.innerHeight - 20) {
            goDownButton.style.display = "block";
        } else {
            goDownButton.style.display = "none";
        }
    }
});
