﻿document.addEventListener("DOMContentLoaded", function () {
    const form = document.querySelector('.form');
    const submitButton = document.getElementById('submit-form-button');
    const airportNameInput = document.getElementById('airport-name');
    const airportCountryInput = document.getElementById('airport-country');
    const airportCityInput = document.getElementById('airport-city');
    const airportCodeInput = document.getElementById('airport-code');
    const airportRunwaysCountInput = document.getElementById('airport-runways-count');
    const airportFoundedInput = document.getElementById('airport-founded');
    const airportNameError = document.getElementById('airport-name-error');
    const countryError = document.getElementById('airport-country-error');
    const cityError = document.getElementById('airport-city-error');
    const codeError = document.getElementById('airport-code-error');
    const runwaysCountError = document.getElementById('airport-runways-count-error');
    const foundedError = document.getElementById('airport-founded-error');
    const now = new Date();

    function hideAllErrors() {
        airportNameError.style.display = 'none';
        countryError.style.display = 'none';
        cityError.style.display = 'none';
        codeError.style.display = 'none';
        runwaysCountError.style.display = 'none';
        foundedError.style.display = 'none';
        submitButton.disabled = true;
    }
    function validateForm() {
        let isAirportNameValid = true;
        let isCountryValid = true;
        let isCityValid = true;
        let isCodeValid = true;
        let isRunwaysCountValid = true;
        let isFoundedValid = true;


        if (airportNameInput.value.trim() === '') {
            airportNameError.textContent = "Airport name is required.";
            airportNameError.style.display = 'block';
            isAirportNameValid = false;
        } else if (airportNameInput.value.length > 50) {
            airportNameError.textContent = "Airport name must be less than 50 characters.";
            airportNameError.style.display = 'block';
            isAirportNameValid = false;
        } else {
            airportNameError.style.display = 'none';
        }

        if (airportCountryInput.value.trim() === '') {
            countryError.textContent = "Country is required.";
            countryError.style.display = 'block';
            isCountryValid = false;
        }
        else if (airportCountryInput.value.length > 50) {
            countryError.textContent = "Country must be less than 50 characters.";
            countryError.style.display = 'block';
            isCountryValid = false;
        }
        else {
            countryError.style.display = 'none';
        }

        if (airportCityInput.value.trim() === '') {
            cityError.textContent = "City is required.";
            cityError.style.display = 'block';
            isCityValid = false;
        }
        else if (airportCityInput.value.length > 50) {
            cityError.textContent = "City must be less than 50 characters.";
            cityError.style.display = 'block';
            isCityValid = false;
        }
        else {
            cityError.style.display = 'none';
        }

        if (airportCodeInput.value.trim() === '') {
            codeError.textContent = "Airport code is required.";
            codeError.style.display = 'block';
            isCodeValid = false;
        }
        else if (airportCodeInput.value.length > 3) {
            codeError.textContent = "Airport code must be 3 characters.";
            codeError.style.display = 'block';
            isCodeValid = false;
        }
        else {
            codeError.style.display = 'none';
        }

        if (airportRunwaysCountInput.value.trim() === '') {
            runwaysCountError.textContent = "Runways count is required.";
            runwaysCountError.style.display = 'block';
            isRunwaysCountValid = false;
        }

        else if (airportRunwaysCountInput.value < 0) {
            runwaysCountError.textContent = "Runways count must be greater than 0.";
            runwaysCountError.style.display = 'block';
            isRunwaysCountValid = false;
        }
        else {
            runwaysCountError.style.display = 'none';
        }

        if (airportFoundedInput.value.trim() === '') {
            foundedError.textContent = "Founded date is required.";
            foundedError.style.display = 'block';
            isFoundedValid = false;
        }

        else if (new Date(airportFoundedInput.value) > now) {
            foundedError.textContent = "Founded date cannot be in the future.";
            foundedError.style.display = 'block';
            isValid = false;
        }
        else {
            foundedError.style.display = 'none';
        }
        submitButton.disabled = !(isAirportNameValid && isCityValid && isCodeValid && isCountryValid && isRunwaysCountValid && isFoundedValid);
    }

    if (form) {
        airportNameInput.addEventListener('input', validateForm);
        airportCountryInput.addEventListener('input', validateForm);
        airportCityInput.addEventListener('input', validateForm);
        airportCodeInput.addEventListener('input', validateForm);
        airportRunwaysCountInput.addEventListener('input', validateForm);
        airportFoundedInput.addEventListener('input', validateForm);

        form.addEventListener('submit', function (event) {
            event.preventDefault();
            validateForm();
            if (submitButton && !submitButton.disabled) {
                form.submit();
            }
        });

        hideAllErrors();
    }
});