const apiUrl = 'https://localhost:7087/api';

async function fetchData(url, options = {}) {
    try {
        const response = await fetch(url, options);
        if (!response.ok) {
            throw new Error(`HTTP error! Status: ${response.status}`);
        }
        return await response.json();
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
}

function makeRequest(url, options) {
    return new Promise(async (resolve, reject) => {
        try {
            const response = await fetch(url, options);
            if (!response.ok) {
                throw new Error(response.statusText);
            }
            const data = await response.json();
            resolve(data);
        } catch (error) {
            reject(error);
        }
    });
}

async function getAllAirlines() {
    try {
        return await makeRequest(`${apiUrl}/airline`, {});
    } catch (error) {
        console.error('Error fetching airlines:', error);
        throw error;
    }
}

async function getAirlineById(id) {
    try {
        return await makeRequest(`${apiUrl}/airline/${id}`, {});
    } catch (error) {
        console.error(`Error fetching airline with ID ${id}:`, error);
        throw error;
    }
}

async function createAirline(airlineData) {
    try {
        return await makeRequest(`${apiUrl}/airline`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(airlineData),
        });
    } catch (error) {
        console.error('Error creating airline:', error);
        throw error;
    }
}

async function editAirline(airlineData) {
    try {
        return await makeRequest(`${apiUrl}/airline/${airlineData.airlineId}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(airlineData),
        });
    } catch (error) {
        console.error('Error editing airline:', error);
        throw error;
    }
}

async function deleteAirline(id) {
    try {
        return await makeRequest(`${apiUrl}/airline/${id}`, {
            method: 'DELETE',
        });
    } catch (error) {
        console.error(`Error deleting airline with ID ${id}:`, error);
        throw error;
    }
}

async function getAirlineCount() {
    try {
        return await makeRequest(`${apiUrl}/airline/counts`, {});
    } catch (error) {
        console.error('Error fetching airline count:', error);
        throw error;
    }
}

async function getAllAirports() {
    try {
        return await makeRequest(`${apiUrl}/airport`, {});
    } catch (error) {
        console.error('Error fetching airports:', error);
        throw error;
    }
}

async function getAirportById(id) {
    try {
        return await makeRequest(`${apiUrl}/airport/${id}`, {});
    } catch (error) {
        console.error(`Error fetching airport with ID ${id}:`, error);
        throw error;
    }
}

async function createAirport(airportData) {
    try {
        return await makeRequest(`${apiUrl}/airport`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(airportData),
        });
    } catch (error) {
        console.error('Error creating airport:', error);
        throw error;
    }
}

async function editAirport(airportData) {
    try {
        return await makeRequest(`${apiUrl}/airport/${airportData.airportId}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(airportData),
        });
    } catch (error) {
        console.error('Error editing airport:', error);
        throw error;
    }
}

async function deleteAirport(id) {
    try {
        return await makeRequest(`${apiUrl}/airport/${id}`, {
            method: 'DELETE',
        });
    } catch (error) {
        console.error(`Error deleting airport with ID ${id}:`, error);
        throw error;
    }
}

async function getAirportCount() {
    try {
        return await makeRequest(`${apiUrl}/airport/counts`, {});
    } catch (error) {
        console.error('Error fetching airport count:', error);
        throw error;
    }
}

async function getAllFlights() {
    try {
        return await makeRequest(`${apiUrl}/flight`, {});
    } catch (error) {
        console.error('Error fetching flights:', error);
        throw error;
    }
}

async function getFlightById(id) {
    try {
        return await makeRequest(`${apiUrl}/flight/${id}`, {});
    } catch (error) {
        console.error(`Error fetching flight with ID ${id}:`, error);
        throw error;
    }
}
async function createFlight(flightData) {
    try {
        return await makeRequest(`${apiUrl}/flight`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(flightData),
        });
    } catch (error) {
        console.error('Error creating flight:', error);
        throw error;
    }
}

async function editFlight(flightData) {
    try {
        return await makeRequest(`${apiUrl}/flight/${flightData.flightId}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(flightData),
        });
    } catch (error) {
        console.error('Error editing flight:', error);
        throw error;
    }
}

async function deleteFlight(id) {
    try {
        return await makeRequest(`${apiUrl}/flight/${id}`, {
            method: 'DELETE',
        });
    } catch (error) {
        console.error(`Error deleting flight with ID ${id}:`, error);
        throw error;
    }
}

async function getFlightCount() {
    try {
        return await makeRequest(`${apiUrl}/flight/counts`, {});
    } catch (error) {
        console.error('Error fetching flight count:', error);
        throw error;
    }
}

window.getAllAirlines = getAllAirlines;
window.getAirlineById = getAirlineById;
window.editAirline = editAirline;
window.createAirline = createAirline;
window.deleteAirline = deleteAirline;
window.getAirlineCount = getAirlineCount;

window.getAllAirports = getAllAirports;
window.getAirportById = getAirportById;
window.editAirport = editAirport;
window.createAirport = createAirport;
window.deleteAirport = deleteAirport;
window.getAirportCount = getAirportCount;

window.getAllFlights = getAllFlights;
window.getFlightById = getFlightById;
window.editFlight = editFlight;
window.createFlight = createFlight;
window.deleteFlight = deleteFlight;
window.getFlightCount = getFlightCount;