﻿document.addEventListener("DOMContentLoaded", function () {
    const form = document.querySelector('.form');
    const submitButton = document.querySelector('.form input[type="submit"]');
    const fromAirportInput = document.getElementById('flight-from-airport');
    const toAirportInput = document.getElementById('flight-to-airport');
    const flightNumberInput = document.getElementById('flight-number');
    const departureDateInput = document.getElementById('flight-departure-datetime');
    const arrivalDateInput = document.getElementById('flight-arrival-datetime');
    const flightNumberError = document.getElementById('flight-number-error');
    const fromAirportError = document.getElementById('from-airport-error');
    const toAirportError = document.getElementById('to-airport-error');
    const departureDateError = document.getElementById('departure-date-error');
    const arrivalDateError = document.getElementById('arrival-date-error');
    const now = new Date();

    function hideAllErrors() {
        if (flightNumberError) flightNumberError.style.display = 'none';
        if (fromAirportError) fromAirportError.style.display = 'none';
        if (toAirportError) toAirportError.style.display = 'none';
        if (departureDateError) departureDateError.style.display = 'none';
        if (arrivalDateError) arrivalDateError.style.display = 'none';
        if (submitButton) submitButton.disabled = true;
    }

    function validateForm() {
        let isFlightNumberValid = true;
        let isFromAirportValid = true;
        let isToAirportValid = true;
        let isDepartureDateValid = true;
        let isArrivalDateValid = true;

        if (flightNumberInput.value.trim() === '') {
            flightNumberError.textContent = "Flight number is required.";
            flightNumberError.style.display = 'block';
            isFlightNumberValid = false;
        } else if (flightNumberInput.value.length > 10) {
            flightNumberError.textContent = "Flight number must be less than 10 characters.";
            flightNumberError.style.display = 'block';
            isFlightNumberValid = false;
        } else {
            flightNumberError.style.display = 'none';
        }

        if (fromAirportInput.value.trim() === '') {
            fromAirportError.textContent = "Departure Airport is required.";
            fromAirportError.style.display = 'block';
            isFromAirportValid = false;
        } else if (fromAirportInput.value.length !== 3) {
            fromAirportError.textContent = "Airport code must be 3 characters.";
            fromAirportError.style.display = 'block';
            isFromAirportValid = false;
        } else {
            fromAirportError.style.display = 'none';
        }

        if (toAirportInput.value.trim() === '') {
            toAirportError.textContent = "Destination Airport is required.";
            toAirportError.style.display = 'block';
            isToAirportValid = false;
        } else if (toAirportInput.value.length !== 3) {
            toAirportError.textContent = "Airport code must be 3 characters.";
            toAirportError.style.display = 'block';
            isToAirportValid = false;
        } else {
            toAirportError.style.display = 'none';
        }

        if (departureDateInput.value.trim() === '') {
            departureDateError.textContent = "Departure date is required.";
            departureDateError.style.display = 'block';
            isDepartureDateValid = false;
        } else if (new Date(departureDateInput.value) >= new Date(arrivalDateInput.value)) {
            departureDateError.textContent = "Departure date must be before arrival date.";
            departureDateError.style.display = 'block';
            isDepartureDateValid = false;
        } else {
            departureDateError.style.display = 'none';
        }

        if (arrivalDateInput.value.trim() === '') {
            arrivalDateError.textContent = "Arrival date is required.";
            arrivalDateError.style.display = 'block';
            isArrivalDateValid = false;
        } else if (new Date(arrivalDateInput.value) <= now) {
            arrivalDateError.textContent = "Arrival date must be in the future.";
            arrivalDateError.style.display = 'block';
            isArrivalDateValid = false;
        } else {
            arrivalDateError.style.display = 'none';
        }

        submitButton.disabled = !(isFlightNumberValid && isFromAirportValid && isToAirportValid && isDepartureDateValid && isArrivalDateValid);
    }

    if (form) {
        flightNumberInput.addEventListener('input', validateForm);
        fromAirportInput.addEventListener('input', validateForm);
        toAirportInput.addEventListener('input', validateForm);
        departureDateInput.addEventListener('input', validateForm);
        arrivalDateInput.addEventListener('input', validateForm);

        form.addEventListener('submit', function (event) {
            event.preventDefault();
            validateForm();
            if (submitButton && !submitButton.disabled) {
                form.submit();
            }
        });

        hideAllErrors();
    }
});
