using Airlines.Persistence.InMemory.Management;
using Airlines.Persistence.InMemory.Models;
using Airlines.Persistence.InMemory.Models.Interfaces;
using Xunit.Abstractions;
namespace Airlines.UnitTest;
[Collection("Sequential")]

public class DataManagementTests(ITestOutputHelper output)
{
    private readonly ITestOutputHelper _output = output;
    [Fact]
    public void AirlineExists_WhenAirlineExists_ShouldReturnTrue()
    {
        // Arrange
        var airlines = new List<IAirline> { new Airline("United"), new Airline("Emirates") };
        var dataManagement = new DataManagement([], airlines, [], [], [], []);

        // Act
        var result = dataManagement.AirlineExists("United");

        // Assert
        Assert.True(result);
    }

    [Fact]
    public void AirlineExists_WhenAirlineDoesNotExist_ShouldReturnFalse()
    {
        // Arrange
        var airlines = new List<IAirline> { new Airline("United"), new Airline("Emirates") };
        var dataManagement = new DataManagement([], airlines, [], [], [], []);

        // Act
        var result = dataManagement.AirlineExists("Delta");

        // Assert
        Assert.False(result);
    }

    [Fact]
    public void ListAirportsInCityOrCountry_WhenCityMatches_ShouldReturnMatchingAirports()
    {
        // Arrange
        var airports = new List<IAirport>
            {
                new Airport("JFK", "John F. Kennedy International Airport", "New York", "USA"),
                new Airport("LAX", "Los Angeles International Airport", "Los Angeles", "USA"),
                new Airport("PEK", "Beijing Capital International Airport", "Beijing", "China")
            };
        var dataManagement = new DataManagement(airports, [], [], [], [], []);

        // Act
        var result = dataManagement.ListAirportsInCityOrCountry("New York", "City").ToList();

        // Assert
        _ = Assert.Single(result);
        Assert.Contains(result, airport => airport.City == "New York");
    }

    [Fact]
    public void ListAirportsInCityOrCountry_WhenCountryMatches_ShouldReturnMatchingAirports()
    {
        // Arrange
        var airports = new List<IAirport>
            {
                new Airport("JFK", "John F. Kennedy International Airport", "New York", "USA"),
                new Airport("LAX", "Los Angeles International Airport", "Los Angeles", "USA"),
                new Airport("PEK", "Beijing Capital International Airport", "Beijing", "China")
            };
        var dataManagement = new DataManagement(airports, [], [], [], [], []);

        // Act
        var result = dataManagement.ListAirportsInCityOrCountry("USA", "Country").ToList();

        // Assert
        Assert.Equal(2, result.Count);
        Assert.Contains(result, airport => airport.Country == "USA");
    }
    [Fact]
    public void ProcessCommand_WhenActionExistAndAirlineExists_ShouldReturnTrue()
    {
        // Arrange
        var airlines = new List<IAirline> { new Airline("United"), new Airline("Emirates") };
        var dataManagement = new DataManagement([], airlines, [], [], [], []);
        var command = "exist United";

        // Act
        using StringWriter sw = new StringWriter();
        Console.SetOut(sw);
        dataManagement.ProcessCommand(command);
        var output = sw.ToString();

        // Assert
        Assert.Contains("True", output);
    }

    [Fact]
    public void ProcessCommand_WhenActionExistAndAirlineDoesNotExist_ShouldReturnFalse()
    {
        // Arrange
        var airlines = new List<IAirline> { new Airline("United"), new Airline("Emirates") };
        var dataManagement = new DataManagement([], airlines, [], [], [], []);
        var command = "exist Delta";

        // Act
        using StringWriter sw = new StringWriter();
        Console.SetOut(sw);
        dataManagement.ProcessCommand(command);
        var output = sw.ToString();

        // Assert
        Assert.Contains("False", output);
    }


    [Fact]
    public void ProcessCommand_WhenActionListAndCountryMatches_ShouldReturnMatchingAirports()
    {
        // Arrange
        var airports = new List<IAirport>
        {
                new Airport("JFK", "John F. Kennedy International Airport", "New York", "USA"),
                new Airport("LAX", "Los Angeles International Airport", "Los Angeles", "USA"),
                new Airport("PEK", "Beijing Capital International Airport", "Beijing", "China")
            };
        var dataManagement = new DataManagement(airports, [], [], [], [], []);
        var command = "list USA Country";

        // Act
        using StringWriter sw = new StringWriter();
        Console.SetOut(sw);
        dataManagement.ProcessCommand(command);
        var output = sw.ToString();

        // Assert
        Assert.Contains("JFK", output);
        Assert.Contains("LAX", output);
    }

    [Fact]
    public void ProcessCommand_WhenSortAndDataTypeIsAirlines_ShouldReturnSortedAirlines()
    {
        // Arrange
        var airlines = new List<IAirline> { new Airline("United"), new Airline("Emirates") };
        var dataManagement = new DataManagement([], airlines, [], [], [], []);
        var command = "sort Airlines";

        // Act
        using StringWriter sw = new StringWriter();
        Console.SetOut(sw);
        dataManagement.ProcessCommand(command);
        var output = sw.ToString();

        // Assert
        Assert.Contains("United", output);
        Assert.Contains("Emirates", output);
    }

    [Fact]
    public void IsAircraftCargoPossible_WhenAircraftExistsAndCargoIsPossible_ShouldReturnTrue()
    {
        // Arrange
        var flights = new List<IFlight> { new Flight("ABC123", "AA", "BB", "Airbus", 2.2, 2.1) };
        var cargo = new List<CargoAircraft> { new("Airbus", 100, 100, "cargo") };
        var cargoWeight = 50;
        var cargoVolume = 50;
        var flightID = "ABC123";
        var type = "cargo";

        // Act
        using StringWriter sw = new StringWriter();
        Console.SetOut(sw);
        var result = ReserveManagement.IsAircraftCargoPossible(flights, cargo, cargoWeight, cargoVolume, flightID, type);

        // Assert
        Assert.True(result);
    }

    [Fact]
    public void IsAircraftCargoPossible_WhenAircraftDoesNotExist_ShouldReturnFalse()
    {
        // Arrange
        var flights = new List<IFlight> { new Flight("ABC123", "AA", "BB", "Airbus", 2.1, 2.3) };
        var cargo = new List<CargoAircraft> { new("Boeing", 100, 100, "cargo") };
        var cargoWeight = 50;
        var cargoVolume = 50;
        var flightID = "ABC123";
        var type = "cargo";

        // Act
        using StringWriter sw = new StringWriter();
        Console.SetOut(sw);
        var result = ReserveManagement.IsAircraftCargoPossible(flights, cargo, cargoWeight, cargoVolume, flightID, type);

        // Assert
        Assert.False(result);
    }

    [Fact]
    public void IsAircraftPassengerPossible_WhenAircraftExistsAndPassengerIsPossible_ShouldReturnFalse()
    {
        // Arrange
        var flights = new List<IFlight> { new Flight("ABC123", "AA", "BB", "Airbus", 2.1, 2.3) };
        var passenger = new List<PassengerAircraft> { new("Airbus", 100, 100, 100, "passenger") };
        var smallBaggageCount = 10;
        var largeBaggageCount = 20;
        var seats = 50;
        var flightID = "ABC123";
        var type = "passenger";

        // Act
        using StringWriter sw = new StringWriter();
        Console.SetOut(sw);
        var result = ReserveManagement.IsAircraftPassengerPossible(flights, passenger, smallBaggageCount, largeBaggageCount, seats, flightID, type);

        // Assert
        Assert.False(result);
    }

    [Fact]
    public void IsAircraftPassengerPossible_WhenAircraftDoesNotExist_ShouldReturnFalse()
    {
        // Arrange
        var flights = new List<IFlight> { new Flight("ABC123", "AA", "BB", "Airbus", 2.1, 2.3) };
        var passenger = new List<PassengerAircraft> { new("Boeing", 100, 100, 100, "passenger") };
        var smallBaggageCount = 10;
        var largeBaggageCount = 20;
        var seats = 50;
        var flightID = "ABC123";
        var type = "passenger";

        // Act
        using StringWriter sw = new StringWriter();
        Console.SetOut(sw);
        var result = ReserveManagement.IsAircraftPassengerPossible(flights, passenger, smallBaggageCount, largeBaggageCount, seats, flightID, type);

        // Assert
        Assert.False(result);
    }

    [Fact]
    public void IsCargoReservationPossible_WhenAircraftExistsAndCargoIsPossible_ShouldReturnTrue()
    {
        // Arrange
        var flights = new List<IFlight> { new Flight("ABC123", "AA", "BB", "Airbus", 2.1, 2.3) };
        var cargo = new List<CargoAircraft> { new("Airbus", 100, 100, "cargo") };
        var flightId = "ABC123";
        var cargoWeight = 50;
        var cargoVolume = 50;
        var type = "cargo";

        // Act
        using StringWriter sw = new StringWriter();
        Console.SetOut(sw);
        var result = ReserveManagement.IsCargoReservationPossible(flights, cargo, flightId, cargoWeight, cargoVolume, type);

        // Assert
        _output.WriteLine(sw.ToString());
    }

    [Fact]
    public void IsCargoReservationPossible_WhenAircraftDoesNotExist_ShouldReturnTrue()
    {
        // Arrange
        var flights = new List<IFlight> { new Flight("ABC123", "AA", "BB", "Airbus", 2.1, 2.3) };
        var cargo = new List<CargoAircraft> { new("Boeing", 100, 100, "cargo") };
        var flightId = "ABC123";
        var cargoWeight = 50;
        var cargoVolume = 50;
        var type = "cargo";

        // Act
        using StringWriter sw = new StringWriter();
        Console.SetOut(sw);
        var result = ReserveManagement.IsCargoReservationPossible(flights, cargo, flightId, cargoWeight, cargoVolume, type);
        _output.WriteLine(sw.ToString());

    }

    [Fact]
    public void IsPassengerTicketPossible_WhenAircraftExistsAndPassengerIsPossible_ShouldReturnFalse()
    {
        // Arrange
        var flights = new List<IFlight> { new Flight("ABC123", "AA", "BB", "Airbus", 2.1, 2.3) };
        var passenger = new List<PassengerAircraft> { new("Airbus", 100, 100, 100, "passenger") };
        var flightId = 12.5;
        var smallBaggageCount = "10";
        var largeBaggageCount = 20;
        var seats = 50;
        var type = "ticket";

        // Act
        using StringWriter sw = new StringWriter();
        Console.SetOut(sw);
        var result = ReserveManagement.IsPassengerTicketPossible(flights, passenger, smallBaggageCount, largeBaggageCount, seats, flightId, type);

        // Assert
        Assert.False(result);
    }

    [Fact]
    public void IsPassengerTicketPossible_WhenAircraftDoesNotExist_ShouldReturnFalse()
    {
        // Arrange
        var flights = new List<IFlight> { new Flight("ABC123", "AA", "BB", "Airbus", 2.1, 2.3) };
        var passenger = new List<PassengerAircraft> { new("Boeing", 100, 100, 100, "passenger") };
        var flightId = 12.5;
        var smallBaggageCount = "10";
        var largeBaggageCount = 20;
        var seats = 50;
        var type = "ticket";

        // Act
        using StringWriter sw = new StringWriter();
        Console.SetOut(sw);
        var result = ReserveManagement.IsPassengerTicketPossible(flights, passenger, smallBaggageCount, largeBaggageCount, seats, flightId, type);

        // Assert
        Assert.False(result);
    }

    [Fact]
    public void BaggageCalculation_WhenBaggageIsUnderLimit_ShouldReturnTrue()
    {
        // Arrange
        var smallBaggageCount = 10;
        var largeBaggageCount = 20;

        // Act
        using StringWriter sw = new StringWriter();
        Console.SetOut(sw);
        var result = ReserveManagement.BaggageCalculation(smallBaggageCount, largeBaggageCount);

        // Assert
        Assert.True(result);
    }

    [Fact]
    public void BaggageCalculation_WhenBaggageIsOverLimit_ShouldReturnFalse()
    {
        // Arrange
        var smallBaggageCount = 20;
        var largeBaggageCount = 40;

        // Act
        using StringWriter sw = new StringWriter();
        Console.SetOut(sw);
        var result = ReserveManagement.BaggageCalculation(smallBaggageCount, largeBaggageCount);

        // Assert
        Assert.False(result);
    }

    [Fact]
    public void SortData_WhenSortingAirlinesDescending_ShouldReturnSortedAirlines()
    {
        // Arrange
        var airlines = new List<IAirline> { new Airline("United"), new Airline("Emirates") };
        var dataManagement = new DataManagement([], airlines, [], [], [], []);
        var dataType = "airlines";
        var sortOrder = "descending";

        // Redirect standard output to a StringWriter
        using StringWriter stringWriter = new StringWriter();
        Console.SetOut(stringWriter);

        // Act
        dataManagement.SortData(dataType, sortOrder);
        string consoleOutput = stringWriter.ToString().Trim();

        // Assert
        Assert.Contains("United", consoleOutput);
        Assert.Contains("Emirates", consoleOutput);
    }

    [Fact]
    public void SearchData_WhenSearchingForAirline_ShouldReturnMatchingAirline()
    {
        // Arrange
        var airlines = new List<IAirline> { new Airline("United"), new Airline("Emirates") };
        var dataManagement = new DataManagement([], airlines, [], [], [], []);
        var searchTerm = "United";

        // Redirect standard output to a StringWriter
        using StringWriter stringWriter = new StringWriter();
        Console.SetOut(stringWriter);

        // Act
        dataManagement.SearchData(searchTerm);
        string consoleOutput = stringWriter.ToString().Trim();

        // Assert
        Assert.Contains("United", consoleOutput);
    }
    [Fact]
    public void SearchAirport_WhenAirportExists_ShouldReturnMatchingAirport()
    {
        // Arrange
        var airports = new List<IAirport>
        {
            new Airport("JFK", "John F. Kennedy International Airport", "New York", "USA"),
            new Airport("LAX", "Los Angeles International Airport", "Los Angeles", "USA"),
            new Airport("PEK", "Beijing Capital International Airport", "Beijing", "China")
        };
        var dataManagement = new DataManagement(airports, [], [], [], [], []);
        var searchTerm = "JFK";

        // Act
        var result = dataManagement.SearchAirport(searchTerm);

        // Assert
        Assert.Null(result);
    }
    [Fact]
    public void SearchAirline_WhenAirlineExists_ShouldReturnMatchingAirline()
    {
        // Arrange
        var airlines = new List<IAirline> { new Airline("United"), new Airline("Emirates") };
        var dataManagement = new DataManagement([], airlines, [], [], [], []);
        var searchTerm = "United";

        // Act
        var result = dataManagement.SearchAirline(searchTerm);

        // Assert
        Assert.NotNull(result);
    }
    [Fact]
    public void SearchFlight_WhenFlightExists_ShouldReturnMatchingFlight()
    {
        // Arrange
        var flights = new List<IFlight> { new Flight("ABC123", "AA", "BB", "Airbus", 22.1, 2.3) };
        var dataManagement = new DataManagement([], [], flights, [], [], []);
        var searchTerm = "ABC123";

        // Act
        var result = dataManagement.SearchFlight(searchTerm);

        // Assert
        Assert.NotNull(result);
    }
    [Fact]
    public void SortData_WhenSortingAirportsAscending_ShouldReturnSortedAirports()
    {
        // Arrange
        var airports = new List<IAirport>
        {
            new Airport("JFK", "John F. Kennedy International Airport", "New York", "USA"),
            new Airport("LAX", "Los Angeles International Airport", "Los Angeles", "USA"),
            new Airport("PEK", "Beijing Capital International Airport", "Beijing", "China")
        };
        var dataManagement = new DataManagement(airports, [], [], [], [], []);
        var dataType = "airports";
        var sortOrder = "ascending";

        // Redirect standard output to a StringWriter
        using StringWriter stringWriter = new StringWriter();
        Console.SetOut(stringWriter);

        // Act
        dataManagement.SortData(dataType, sortOrder);
        string consoleOutput = stringWriter.ToString().Trim();

        // Assert
        Assert.Contains("JFK", consoleOutput);
        Assert.Contains("LAX", consoleOutput);
        Assert.Contains("PEK", consoleOutput);
    }

    [Fact]
    public void AddFlight_WhenCalled_ShouldAddFlightToRoute()
    {
        // Arrange
        var route = new LinkedList<IFlight>();
        var routeManagement = new RouteManagement(route);
        var flight = new Flight("A", "B", "C", "D", 22.5, 3.2);

        // Act
        using StringWriter sw = new StringWriter();
        Console.SetOut(sw);
        routeManagement.AddFlight(flight);

        // Assert
        _ = Assert.Single(route);
        Assert.Equal(flight, route?.First?.Value);
    }
    [Fact]
    public void ProcessCommand_WhenActionSortAndDataTypeIsFlights_ShouldReturnSortedFlights()
    {
        // Arrange
        var flights = new List<IFlight> { new Flight("ABC123", "AA", "BB", "Airbus", 150, 30) };
        var dataManagement = new DataManagement([], [], flights, [], [], []);
        var command = "sort Flights";

        // Act
        using StringWriter sw = new StringWriter();
        Console.SetOut(sw);
        dataManagement.ProcessCommand(command);
        var output = sw.ToString();

        // Assert
        Assert.Contains("Flight ABC123: From AA to BB with Airbus", output);
    }
    [Fact]
    public void ProcessCommand_WhenActionSearchAndSearchTermIsFlight_ShouldReturnMatchingFlight()
    {
        // Arrange
        var flights = new List<IFlight> { new Flight("ABC123", "AA", "BB", "Airbus", 2000, 10) };
        var dataManagement = new DataManagement([], [], flights, [], [], []);
        var command = "search Flight ABC123";

        // Act
        using StringWriter sw = new StringWriter();
        Console.SetOut(sw);
        dataManagement.ProcessCommand(command);
        var output = sw.ToString();

        // Assert
        Assert.Contains("Flight ABC123: From AA to BB with Airbus", output);
    }
    [Fact]
    public void Aircraft_Constructor_InitializesProperties()
    {
        // Arrange
        string model = "Boeing 747";
        string type = "Commercial";

        // Act
        Aircraft aircraft = new Aircraft(model, type);

        // Assert
        Assert.Equal(model, aircraft.Model);
        Assert.Equal(type, aircraft.Type);
    }
    [Fact]
    public void Aircraft_ToString_ReturnsModel()
    {
        // Arrange
        string model = "Boeing 747";
        string type = "Commercial";
        Aircraft aircraft = new Aircraft(model, type);

        // Act
        string result = aircraft.ToString();

        // Assert
        Assert.Equal(model, result);
    }
    [Fact]
    public void Airline_Constructor_InitializesProperties()
    {
        // Arrange
        string name = "United";

        // Act
        Airline airline = new Airline(name);

        // Assert
        Assert.Equal(name, airline.Name);
    }
    [Fact]
    public void Airline_ToString_ReturnsName()
    {
        // Arrange
        string name = "United";
        Airline airline = new Airline(name);

        // Act
        string result = airline.ToString();

        // Assert
        Assert.Equal(name, result);
    }
    [Fact]
    public void Airport_Constructor_InitializesProperties()
    {
        // Arrange
        string identifier = "JFK";
        string name = "John F. Kennedy International Airport";
        string city = "New York";
        string country = "USA";

        // Act
        Airport airport = new Airport(identifier, name, city, country);

        // Assert
        Assert.Equal(identifier, airport.Identifier);
        Assert.Equal(name, airport.Name);
        Assert.Equal(city, airport.City);
        Assert.Equal(country, airport.Country);
    }
    [Fact]
    public void Airport_ToString_ReturnsIdentifierNameCityCountry()
    {
        // Arrange
        string identifier = "JFK";
        string name = "John F. Kennedy International Airport";
        string city = "New York";
        string country = "USA";
        Airport airport = new Airport(identifier, name, city, country);

        // Act
        string result = airport.ToString();

        // Assert
        Assert.Equal($"{identifier}, {name}, {city}, {country}", result);
    }
    [Fact]
    public void CargoAircraft_Constructor_InitializesProperties()
    {
        // Arrange
        string name = "Boeing 747";
        double weight = 100;
        double volume = 100;
        string model = "cargo";

        // Act
        CargoAircraft cargoAircraft = new CargoAircraft(name, weight, volume, model);

        // Assert
        Assert.Equal(name, cargoAircraft.Model);
        Assert.Equal(weight, cargoAircraft.CargoWeight);
        Assert.Equal(volume, cargoAircraft.CargoVolume);
    }
    [Fact]
    public void CargoAircraft_ToString_ReturnsModelCargoWeightCargoVolume()
    {
        // Arrange
        string name = "Boeing 747";
        double weight = 100;
        double volume = 100;
        string model = "cargo";
        CargoAircraft cargoAircraft = new CargoAircraft(name, weight, volume, model);

        // Act
        string result = cargoAircraft.ToString();

        // Assert
        Assert.Equal($"Aircraft {name} with cargo weight {weight} kg and volume {volume} m³", result);
    }
    [Fact]
    public void Flight_Constructor_InitializesProperties()
    {
        // Arrange
        string identifier = "ABC123";
        string departureAirport = "AA";
        string arrivalAirport = "BB";
        string aircraft = "Airbus";
        double price = 100;
        double duration = 100;

        // Act
        Flight flight = new Flight(identifier, departureAirport, arrivalAirport, aircraft, price, duration);

        // Assert
        Assert.Equal(identifier, flight.Identifier);
        Assert.Equal(departureAirport, flight.DepartureAirport);
        Assert.Equal(arrivalAirport, flight.ArrivalAirport);
        Assert.Equal(aircraft, flight.Aircraft);
        Assert.Equal(price, flight.Price);
        Assert.Equal(duration, flight.Duration);
    }
    [Fact]
    public void Flight_ToString_Returns_Identifier_DepartureAirport_ArrivalAirport_Aircraft()
    {
        // Arrange
        string identifier = "ABC123";
        string departureAirport = "AA";
        string arrivalAirport = "BB";
        string aircraft = "Airbus";
        double price = 100;
        double duration = 100;
        Flight flight = new Flight(identifier, departureAirport, arrivalAirport, aircraft, price, duration);

        // Act
        string result = flight.ToString();

        // Assert
        Assert.Equal($"Flight {identifier}: From {departureAirport} to {arrivalAirport} with {aircraft}, Price {price}, Duration{duration}", result);
    }
    [Fact]
    public void PassengerAircraft_Constructor_InitializesProperties()
    {
        // Arrange
        string name = "Boeing 747";
        double weight = 100;
        double volume = 100;
        int seats = 100;
        string model = "passenger";

        // Act
        PassengerAircraft passengerAircraft = new PassengerAircraft(name, weight, volume, seats, model);

        // Assert
        Assert.Equal(name, passengerAircraft.Model);
        Assert.Equal(weight, passengerAircraft.CargoWeight);
        Assert.Equal(volume, passengerAircraft.CargoVolume);
        Assert.Equal(seats, passengerAircraft.Seats);
    }

    [Fact]
    public void PassengerAircraft_ToString_ReturnsModelSmallBaggageWeightLargeBaggageVolumeSeats()
    {
        // Arrange
        string name = "Boeing 747";
        double weight = 100;
        double volume = 100;
        int seats = 100;
        string model = "passenger";
        PassengerAircraft passengerAircraft = new PassengerAircraft(name, weight, volume, seats, model);

        // Act
        string result = passengerAircraft.ToString();

        // Assert
        Assert.Equal($"Aircraft {name} with small baggage weight {weight} kg, large baggage volume {volume} m³, and {seats} seats", result);
    }
    [Fact]
    public void PrivateAircraft_Constructor_InitializesProperties()
    {
        int seats = 8;
        string model = "Gulfstream G650";

        // Act
        PrivateAircraft privateAircraft = new PrivateAircraft(model, seats, model);

        // Assert
        Assert.Equal(model, privateAircraft.Model);
        Assert.Equal(seats, privateAircraft.Seats);
        Assert.Equal(model, privateAircraft.Model);
    }
    [Fact]
    public void PrivateAircraft_ToString_ReturnsModelSeats()
    {
        int seats = 8;
        string model = "Gulfstream G650";
        PrivateAircraft privateAircraft = new PrivateAircraft(model, seats, model);

        // Act
        string result = privateAircraft.ToString();

        // Assert
        Assert.Equal($"Aircraft {model} with {seats} seats", result);
    }

    [Fact]
    public void ReserveManagement_IsAircraftCargoPossible_WhenAircraftExistsAndCargoIsPossible_ShouldReturnTrue()
    {
        // Arrange
        var flights = new List<IFlight> { new Flight("ABC123", "AA", "BB", "Airbus", 300, 200) };
        var cargo = new List<CargoAircraft> { new("Airbus", 100, 100, "cargo") };
        var cargoWeight = 50;
        var cargoVolume = 50;
        var flightID = "ABC123";
        var type = "cargo";

        // Act
        using StringWriter sw = new StringWriter();
        Console.SetOut(sw);
        var result = ReserveManagement.IsAircraftCargoPossible(flights, cargo, cargoWeight, cargoVolume, flightID, type);

        // Assert
        Assert.True(result);
    }
    [Fact]
    public void ReserveManagement_IsAircraftCargoPossible_WhenAircraftDoesNotExist_ShouldReturnFalse()
    {
        // Arrange
        var flights = new List<IFlight> { new Flight("ABC123", "AA", "BB", "Airbus", 500, 100) };
        var cargo = new List<CargoAircraft> { new("Boeing", 100, 100, "cargo") };
        var cargoWeight = 50;
        var cargoVolume = 50;
        var flightID = "ABC123";
        var type = "cargo";

        // Act
        using StringWriter sw = new StringWriter();
        Console.SetOut(sw);
        var result = ReserveManagement.IsAircraftCargoPossible(flights, cargo, cargoWeight, cargoVolume, flightID, type);

        // Assert
        Assert.False(result);
    }
    [Fact]
    public void DataManagement_Constructor_InitializesProperties()
    {
        // Arrange
        var airports = new List<IAirport> { new Airport("JFK", "John F. Kennedy International Airport", "New York", "USA") };
        var airlines = new List<IAirline> { new Airline("United") };
        var flights = new List<IFlight> { new Flight("ABC123", "AA", "BB", "Airbus", 1000, 30) };
        var routes = new LinkedList<IFlight>();
        var cargoAircrafts = new List<CargoAircraft> { new("Airbus", 100, 100, "cargo") };
        var passengerAircrafts = new List<PassengerAircraft> { new("Airbus", 100, 100, 100, "passenger") };
        // Act
        DataManagement dataManagement = new DataManagement(airports, airlines, flights, routes, cargoAircrafts, passengerAircrafts);

        // Assert
        Assert.Equal(airports, dataManagement.Airports);
        Assert.Equal(airlines, dataManagement.Airlines);
        Assert.Equal(flights, dataManagement.Flights);
        Assert.Equal(routes, dataManagement.Routes);
        Assert.Equal(cargoAircrafts, dataManagement.CargoAircrafts);
        Assert.Equal(passengerAircrafts, dataManagement.PassengerAircrafts);
    }
    [Fact]
    public void DataManagement_AirlineExists_WhenAirlineExists_ShouldReturnTrue()
    {
        // Arrange
        var airlines = new List<IAirline> { new Airline("United"), new Airline("Emirates") };
        var dataManagement = new DataManagement([], airlines, [], [], [], []);

        // Act
        var result = dataManagement.AirlineExists("United");

        // Assert
        Assert.True(result);
    }
    [Fact]
    public void RouteManagement_ProcessRouteCommand_WhenActionNew_ShouldCreateNewRoute()
    {
        // Arrange
        var route = new LinkedList<IFlight>();
        var routeManagement = new RouteManagement(route);
        var tokens = new string[] { "route", "new" };

        // Act
        using StringWriter sw = new StringWriter();
        Console.SetOut(sw);
        routeManagement.ProcessRouteCommand(tokens, []);

        // Assert
        Assert.Empty(route);
    }
    [Fact]
    public void DataManagement_ProcessRouteCommand_WhenCalledWithInvalidCommand_ShouldReturnInvalidCommand()
    {
        // Arrange
        var route = new LinkedList<IFlight>();
        var routeManagement = new RouteManagement(route);
        var flights = new List<IFlight> { new Flight("A", "B", "C", "D", 50, 30) };
        var tokens = new string[] { "route" };

        // Act
        using StringWriter sw = new StringWriter();
        Console.SetOut(sw);
        routeManagement.ProcessRouteCommand(tokens, flights);
        sw.Close();

        // Assert
        Assert.Equal("Invalid route command.", sw.ToString().Trim());
    }
}