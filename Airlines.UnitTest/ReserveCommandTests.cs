﻿using Airlines.Business.Commands.Reserve;
using Airlines.Business.Commands.Route;
using Airlines.Persistence.InMemory.Management;
using Airlines.Persistence.InMemory.Models.Interfaces;
using Xunit.Abstractions;

namespace Airlines.UnitTest;
public class ReserveCommandTests(ITestOutputHelper output)
{
    private readonly ITestOutputHelper _output = output;

    [Fact]
    public void ReserveCargoCommand_Execute_ReservesCargo()
    {
        // Arrange
        var routeManager = new RouteManagement(new LinkedList<IFlight>());
        var command = new PrintRouteCommand(routeManager);

        // Act
        using var sw = new StringWriter();
        Console.SetOut(sw);
        command.Execute(null!, null!, null!);
        var actualOutput = sw.ToString();
        sw.Close();
        _output.WriteLine(actualOutput);
    }

    [Fact]
    public void CancelCargoCommand_Execute_CancelsCargo()
    {
        // Arrange
        var routeManager = new RouteManagement(new LinkedList<IFlight>());
        var command = new PrintRouteCommand(routeManager);

        // Act
        using var sw = new StringWriter();
        Console.SetOut(sw);
        command.Execute(null!, null!, null!);
        var actualOutput = sw.ToString();
        sw.Close();
        _output.WriteLine(actualOutput);
    }

    [Fact]
    public void ReservePassengerCommand_Execute_ReservesPassenger()
    {
        // Arrange
        var routeManager = new RouteManagement(new LinkedList<IFlight>());
        var command = new PrintRouteCommand(routeManager);

        // Act
        using var sw = new StringWriter();
        Console.SetOut(sw);
        command.Execute(null!, null!, null!);
        var actualOutput = sw.ToString();
        sw.Close();
        _output.WriteLine(actualOutput);
    }

    [Fact]
    public void ReserveCargoCommand_Constructor_NullCargoAircrafts_ThrowsArgumentNullException() => _ = Assert.Throws<ArgumentNullException>(() => new ReserveCargoCommand(null));

    [Fact]
    public void ReserveCargoCommand_Constructor_NullPassengerAircrafts_ThrowsArgumentNullException() => _ = Assert.Throws<ArgumentNullException>(() => new ReserveCargoCommand(null));

    [Fact]
    public void ReservePassengerCommand_Constructor_NullCargoAircrafts_ThrowsArgumentNullException() => _ = Assert.Throws<ArgumentNullException>(() => new ReservePassengerCommand(null));

    [Fact]
    public void ReservePassengerCommand_Constructor_NullPassengerAircrafts_ThrowsArgumentNullException() => _ = Assert.Throws<ArgumentNullException>(() => new ReservePassengerCommand(null));
}
