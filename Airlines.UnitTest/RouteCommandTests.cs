using Airlines.Business.Commands.Route;
using Airlines.Persistence.InMemory.Management;
using Airlines.Persistence.InMemory.Models;
using Airlines.Persistence.InMemory.Models.Interfaces;
using Xunit.Abstractions;

namespace Airlines.UnitTest;
public class RouteCommandTests(ITestOutputHelper output)
{
    private readonly ITestOutputHelper _output = output;

    [Fact]
    public void AddRouteCommand_Execute_AddsFlightToRoute()
    {
        // Arrange
        var flightIdentifier = "F1";
        var departureAirport = "Airport1";
        var arrivalAirport = "Airport2";
        var aircraft = "A320";
        var price = 100;
        var duration = 60;
        var routeManager = new RouteManagement(new LinkedList<IFlight>());
        _ = new AddRouteCommand(flightIdentifier, departureAirport, arrivalAirport, aircraft, price, duration, routeManager);

        // Assert
        _output.WriteLine("Add Route");
    }

    [Fact]
    public void RemoveRouteCommand_Execute_RemovesFlightFromRoute()
    {
        // Arrange
        var routeManager = new RouteManagement(new LinkedList<IFlight>());
        _ = new RemoveRouteCommand(routeManager);
        // Assert
        _output.WriteLine("Remove Route");
    }

    [Fact]
    public void PrintRouteCommand_Execute_PrintsRoute()
    {
        // Arrange
        var routeManager = new RouteManagement(new LinkedList<IFlight>());
        _ = new PrintRouteCommand(routeManager);

        // Assert
        _output.WriteLine("Something");
    }

    [Fact]
    public void FindPathToNode_WhenTreeIsNull_ReturnsEmptyPath()
    {
        // Arrange
        var flights = new List<IFlight>();
        var flightTrees = new Tree();
        var find = "Airport1";
        var findRouteCommand = new FindRouteCommand(flights, flightTrees, find);

        // Act
        var path = findRouteCommand.FindPathToNode(find);

        // Assert
        Assert.Empty(path);
    }

    [Fact]
    public void FindRoute()
    {
        // Arrange
        var flights = new List<IFlight>();
        var flightTrees = new Tree();
        var find = "Airport1";

        // Act
        var findRouteCommand = FindRouteCommand.FindRoute(flights, flightTrees, find);

        // Assert
        Assert.NotNull(findRouteCommand);
    }

    [Fact]
    public void FindPathRecursive_WhenCurrentIsNull_ReturnsFalse()
    {
        // Arrange
        var flights = new List<IFlight>();
        var flightTrees = new Tree();
        var find = "Airport1";
        _ = new FindRouteCommand(flights, flightTrees, find);

        // Act
        var result = FindRouteCommand.FindPathRecursive(null!, find, []);

        // Assert
        Assert.False(result);
    }

    [Fact]
    public void FindPathRecursive_WhenCurrentDataIsEqualToTargetData_ReturnsTrue()
    {
        // Arrange
        var flights = new List<IFlight>();
        var flightTrees = new Tree();
        var find = "Airport1";
        _ = new FindRouteCommand(flights, flightTrees, find);
        var node = new Node("Airport1");

        // Act
        var result = FindRouteCommand.FindPathRecursive(node, find, []);

        // Assert
        Assert.True(result);
    }

    [Fact]
    public void FindPathRecursive_WhenChildDataIsEqualToTargetData_ReturnsTrue()
    {
        // Arrange
        var flights = new List<IFlight>();
        var flightTrees = new Tree();
        var find = "Airport1";
        _ = new FindRouteCommand(flights, flightTrees, find);
        var node = new Node("Airport1");
        var child = new Node("Airport2");
        node.Children.Add(child);

        // Act
        var result = FindRouteCommand.FindPathRecursive(node, find, []);

        // Assert
        Assert.True(result);
    }

    [Fact]
    public void FindPathRecursive_WhenChildDataIsNotEqualToTargetData_ReturnsFalse()
    {
        // Arrange
        var flights = new List<IFlight>();
        var flightTrees = new Tree();
        var find = "Airport1";
        _ = new FindRouteCommand(flights, flightTrees, find);
        var node = new Node("Airport1");
        var child = new Node("Airport2");
        node.Children.Add(child);

        // Act
        var result = FindRouteCommand.FindPathRecursive(node, "Airport3", []);

        // Assert
        Assert.False(result);
    }

    [Fact]
    public void PrintPath_WhenPathIsNotEmpty_PrintsPath()
    {
        // Arrange
        var flights = new List<IFlight>();
        var flightTrees = new Tree();
        var find = "Airport1";
        var findRouteCommand = new FindRouteCommand(flights, flightTrees, find);
        var node = new Node("Airport1");
        var child = new Node("Airport2");
        node.Children.Add(child);
        flightTrees.Root = node;

        // Act
        findRouteCommand.PrintPath();

        // Assert
        _output.WriteLine("Airport1");
        _output.WriteLine("Airport2");
    }

    [Fact]
    public void PrintPath_WhenPathIsEmpty_PrintsNothing()
    {
        // Arrange
        var flights = new List<IFlight>();
        var flightTrees = new Tree();
        var find = "Airport1";
        var findRouteCommand = new FindRouteCommand(flights, flightTrees, find);

        // Act
        findRouteCommand.PrintPath();

        // Assert
        _output.WriteLine("");
    }

    [Fact]
    public void Execute_PrintPath()
    {
        // Arrange
        var flights = new List<IFlight>();
        var flightTrees = new Tree();
        var find = "Airport1";
        var findRouteCommand = new FindRouteCommand(flights, flightTrees, find);

        // Act
        findRouteCommand.Execute(null, null, null);

        // Assert
        _output.WriteLine("Airport1");
    }

    [Fact]
    public void CheckRoute()
    {
        // Arrange
        var flights = new List<IFlight>();
        var graph = new Graph();
        var startAirport = "Airport1";
        var departureAirport = "Airport2";

        // Act
        var checkRouteCommand = CheckRouteCommand.CheckRoute(flights, graph, startAirport, departureAirport);

        // Assert
        Assert.NotNull(checkRouteCommand);
    }

    [Fact]
    public void CheckAirports_WhenGraphVerticesIsEmpty_ReturnsFalse()
    {
        // Arrange
        var flights = new List<IFlight>();
        var graph = new Graph();
        var startAirport = "Airport1";
        var departureAirport = "Airport2";
        var checkRouteCommand = new CheckRouteCommand(flights, graph, startAirport, departureAirport);

        // Act
        var result = checkRouteCommand.CheckAirports();

        // Assert
        Assert.False(result);
    }

    [Fact]
    public void CheckAirports_WhenStartAirportIsNotInGraphVertices_ReturnsFalse()
    {
        // Arrange
        var flights = new List<IFlight>();
        var graph = new Graph();
        var startAirport = "Airport1";
        var departureAirport = "Airport2";
        var checkRouteCommand = new CheckRouteCommand(flights, graph, startAirport, departureAirport);
        graph.Vertices.Add(new Vertex("Airport2"));

        // Act
        var result = checkRouteCommand.CheckAirports();

        // Assert
        Assert.False(result);
    }

    [Fact]
    public void CheckAirports_WhenDepartureAirportIsNotInGraphVertices_ReturnsFalse()
    {
        // Arrange
        var flights = new List<IFlight>();
        var graph = new Graph();
        var startAirport = "Airport1";
        var departureAirport = "Airport2";
        var checkRouteCommand = new CheckRouteCommand(flights, graph, startAirport, departureAirport);
        graph.Vertices.Add(new Vertex("Airport1"));

        // Act
        var result = checkRouteCommand.CheckAirports();

        // Assert
        Assert.False(result);
    }

    [Fact]
    public void SearchRoute()
    {
        // Arrange
        var flights = new List<IFlight>();
        var graph = new Graph();
        var startAirport = "Airport1";
        var departureAirport = "Airport2";
        var byWeight = "10";

        // Act
        var searchRouteCommand = SearchRouteCommand.SearchRoute(flights, graph, startAirport, departureAirport, byWeight);

        // Assert
        Assert.NotNull(searchRouteCommand);
    }
    [Fact]
    public void FindPathByWeight_ShouldThrowArgumentException_WhenDepartureOrDestinationNotFound()
    {
        // Arrange
        var graph = new Graph();
        var findPathCommand = new FindPathCommand(graph);

        // Act & Assert
        _ = Assert.Throws<ArgumentException>(() => findPathCommand.FindPathByWeight("nonexistentAirport", "destinationAirport", "time"));
    }

    [Fact]
    public void FindPathByWeight_ThrowsArgumentNullException_WhenGraphIsNull() => _ = Assert.Throws<ArgumentNullException>(() => new FindPathCommand(null!));


    [Fact]
    public void RouteCommand_ThrowsArgumentException_WhenRouteIsNull()
    {
        // Arrange
        LinkedList<IFlight>? route = null;

        // Act
        _ = Assert.Throws<ArgumentNullException>(() => new RouteCommand(route));
    }

    [Fact]
    public void RouteCommand_Constructor_ThrowsArgumentNullException_WhenRouteIsNull() => _ = Assert.Throws<ArgumentNullException>(() => new RouteCommand(null));

    [Fact]
    public void RemoveRouteCommand_Constructor_ThrowsArgumentNullException_WhenRouteIsNull() => _ = Assert.Throws<ArgumentNullException>(() => new RemoveRouteCommand(null!));

    [Fact]
    public void PrintRouteCommand_Constructor_ThrowsArgumentNullException_WhenRouteIsNull() => _ = Assert.Throws<ArgumentNullException>(() => new PrintRouteCommand(null!));
}