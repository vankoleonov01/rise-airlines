using Airlines.Business.Extensions;
using System.Diagnostics;
using Xunit.Abstractions;

namespace Airlines.UnitTest;

public class PerformanceTests(ITestOutputHelper output)
{
    private readonly ITestOutputHelper _output = output;

    [Fact]
    public void BubbleSort_SelectionSort_Comparasion_SelectionQuicker_ReturnsTrue()
    {
        string[] dataForSorting = GenerateRandomData(1000);
        TimeSpan timeLimit = TimeSpan.FromSeconds(1);

        var stopwatchBubble = Stopwatch.StartNew();
        int iterationsBubble = 0;
        while (stopwatchBubble.Elapsed < timeLimit)
        {
            _ = SortingExtensions.BubbleSort(dataForSorting);
            iterationsBubble++;
        }
        stopwatchBubble.Stop();

        var stopwatchSelection = Stopwatch.StartNew();
        int iterationsSelection = 0;
        while (stopwatchSelection.Elapsed < timeLimit)
        {
            _ = SortingExtensions.SelectionSort(dataForSorting);
            iterationsSelection++;
        }
        stopwatchSelection.Stop();
        _output.WriteLine($"Bubble Sort: Completed {iterationsBubble} iterations in {timeLimit.TotalSeconds} seconds");
        _output.WriteLine($"Selection Sort: Completed {iterationsSelection} iterations in {timeLimit.TotalSeconds} seconds");
    }

    [Fact]
    public void BubbleSort_PerformanceTest_ReturnsIterationsAndTotalSeconds()
    {
        string[] dataForSorting = GenerateRandomData(1000);
        TimeSpan timeLimit = TimeSpan.FromSeconds(1);

        var stopwatch = Stopwatch.StartNew();
        int iterations = 0;
        while (stopwatch.Elapsed < timeLimit)
        {
            _ = SortingExtensions.BubbleSort(dataForSorting);
            iterations++;
        }
        stopwatch.Stop();

        _output.WriteLine($"Bubble Sort: Completed {iterations} iterations in {timeLimit.TotalSeconds} seconds");
    }

    [Fact]
    public void SelectionSort_PerformanceTest_ReturnsIterationsAndTotalSeconds()
    {
        string[] dataForSorting = GenerateRandomData(1000);
        TimeSpan timeLimit = TimeSpan.FromSeconds(1);

        var stopwatch = Stopwatch.StartNew();
        int iterations = 0;
        while (stopwatch.Elapsed < timeLimit)
        {
            _ = SortingExtensions.SelectionSort(dataForSorting);
            iterations++;
        }
        stopwatch.Stop();
        _output.WriteLine($"Selection Sort: Completed {iterations} iterations in {timeLimit.TotalSeconds} seconds");
    }
    [Fact]
    public void LinearSearch_PerformanceTest_ReturnsIterationsAndTotalSeconds()
    {
        string[] dataForSorting = GenerateRandomData(1000);
        TimeSpan timeLimit = TimeSpan.FromSeconds(1);

        var stopwatch = Stopwatch.StartNew();
        int iterations = 0;
        while (stopwatch.Elapsed < timeLimit)
        {
            _ = SearchingExtensions.LinearSearch(dataForSorting, "AAA");
            iterations++;
        }
        stopwatch.Stop();
        _output.WriteLine($"Linear Search: Completed {iterations} iterations in {timeLimit.TotalSeconds} seconds");
    }

    [Fact]
    public void BinarySearch_PerformanceTest_ReturnsIterationsAndTotalSeconds()
    {
        string[] dataForSorting = GenerateAndSortRandomData(1000);
        TimeSpan timeLimit = TimeSpan.FromSeconds(1);

        var stopwatch = Stopwatch.StartNew();
        int iterations = 0;
        while (stopwatch.Elapsed < timeLimit)
        {
            _ = SearchingExtensions.BinarySearch(dataForSorting, "AAA");
            iterations++;
        }
        stopwatch.Stop();
        _output.WriteLine($"Binary Search: Completed {iterations} iterations in {timeLimit.TotalSeconds} seconds");
    }

    [Fact]
    public void BinarySearch_LinearSearch_ComparePerformance_ReturnsTrue()
    {
        string[] dataForSorting = GenerateAndSortRandomData(10000);
        var stopwatchBinary = Stopwatch.StartNew();
        _ = SearchingExtensions.BinarySearch(dataForSorting, dataForSorting[700]);
        stopwatchBinary.Stop();

        var stopwatchLinear = Stopwatch.StartNew();
        _ = SearchingExtensions.LinearSearch(dataForSorting, dataForSorting[700]);
        stopwatchLinear.Stop();
        _output.WriteLine($"Binary Search: Completed for {stopwatchBinary}");
        _output.WriteLine($"Linear Search: Completed for {stopwatchLinear}");
    }

    [Fact]
    public void BinarySearch_LinearSearch_ComparePerformance_SmallData_ReturnsTrue()
    {
        // Small dataset (1 element)
        string[] smallData = ["AAA"];

        var stopwatchBinarySmall = Stopwatch.StartNew();
        _ = SearchingExtensions.BinarySearch(smallData, "AAA");
        stopwatchBinarySmall.Stop();

        var stopwatchLinearSmall = Stopwatch.StartNew();
        _ = SearchingExtensions.LinearSearch(smallData, "AAA");
        stopwatchLinearSmall.Stop();

        Assert.True(stopwatchBinarySmall.Elapsed > stopwatchLinearSmall.Elapsed);
        _output.WriteLine($"Binary Search (Small Data): Completed in {stopwatchBinarySmall.Elapsed}");
        _output.WriteLine($"Linear Search (Small Data): Completed in {stopwatchLinearSmall.Elapsed}");
    }


    /// <summary>
    /// Method to generate random data
    /// </summary>
    /// <param name="size"></param>
    /// <returns></returns>
    public static string[] GenerateRandomData(int size)
    {
        string[] arr = new string[size];
        Random random = new Random();

        const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (int i = 0; i < size; i++)
        {
            char[] randomChars = new char[3];
            for (int j = 0; j < 3; j++)
            {
                randomChars[j] = chars[random.Next(chars.Length)];
            }
            arr[i] = new string(randomChars);
        }

        return arr;
    }

    /// <summary>
    /// Method to generate random data and sort it
    /// </summary>
    /// <param name="size"></param>
    /// <returns></returns>
    public static string[] GenerateAndSortRandomData(int size)
    {
        string[] arr = GenerateRandomData(size);
        Array.Sort(arr);
        return arr;
    }
}
