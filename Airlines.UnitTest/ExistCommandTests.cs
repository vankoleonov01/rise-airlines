using Airlines.Business.Commands.Exist;
using Airlines.Persistence.InMemory.Models;
using Airlines.Persistence.InMemory.Models.Interfaces;

namespace Airlines.UnitTest;

[Collection("Sequential")]
public class ExistCommandTests
{
    [Fact]
    public void AirlineExists_CommandExecutedWithExistingAirline_ReturnsFalse()
    {
        // Arrange
        var airlineName = "Airline 1";
        var airlines = new List<IAirline>
            {
                new Airline("Airline 1"),
                new Airline("Airline 2")
            };
        var existCommand = new ExistCommand(airlineName, airlines);

        // Act
        var result = existCommand.AirlineExists(airlineName);

        // Assert
        Assert.False(result);
    }

    [Fact]
    public void AirlineExists_CommandExecutedWithNonExistingAirline_ReturnsFalse()
    {
        // Arrange
        var airlineName = "Nonexistent Airline";
        var airlines = new List<IAirline>
            {
                new Airline("Airline 1"),
                new Airline("Airline 2")
            };
        var existCommand = new ExistCommand(airlineName, airlines);

        // Act
        var result = existCommand.AirlineExists(airlineName);

        // Assert
        Assert.False(result);
    }
    [Fact]
    public void Constructor_NullAirlineName_ThrowsArgumentNullException()
    {
        // Arrange
        string airlineName = null!;
        var airlines = new List<IAirline>();

        // Act & Assert
        _ = Assert.Throws<ArgumentNullException>(() => new ExistCommand(airlineName!, airlines));
    }
    [Fact]
    public void Constructor_NullAirlinesList_ThrowsArgumentNullException()
    {
        // Arrange
        var airlineName = "Airline 1";
        List<IAirline> airlines = null!;

        // Act & Assert
        _ = Assert.Throws<ArgumentNullException>(() => new ExistCommand(airlineName, airlines!));
    }
}