﻿using Airlines.Persistence.InMemory;
using Airlines.Persistence.InMemory.Models.Interfaces;

namespace Airlines.UnitTest;

public class ReaderTests
{
    [Fact]
    public void ReadAirportsFromFileTest()
    {
        // Arrange
        string path = @"../../../airport_in.csv";

        // Act
        List<IAirport>? airports = FileReader.ReadAirportsFromFile(path);

        // Assert
        Assert.NotNull(airports);
        Assert.NotEmpty(airports);
    }

    [Fact]
    public void ReadAirlinesFromFileTest()
    {
        // Arrange
        string path = @"../../../airline_in.csv";

        // Act
        List<IAirline>? airlines = FileReader.ReadAirlinesFromFile(path);

        // Assert
        Assert.NotNull(airlines);
        Assert.NotEmpty(airlines);
    }

    [Fact]
    public void ReadFlightsFromFileTest()
    {
        // Arrange
        string path = @"../../../flight_in.csv";

        // Act
        List<IFlight>? flights = FileReader.ReadFlightsFromFile(path);

        // Assert
        Assert.NotNull(flights);
        Assert.NotEmpty(flights);
    }
    [Fact]
    public void ReadAircraftsFromFileTest()
    {
        // Arrange
        string path = @"../../../aircraft_in.csv";

        // Act
        var aircrafts = FileReader.ReadAircraftsFromFile(path);

        // Assert
        Assert.NotNull(aircrafts);
        Assert.NotEmpty(aircrafts);
    }
    [Fact]
    public void ReadFlightsRouteFileTest()
    {
        // Arrange
        string path = @"../../../flight_in.csv";
        // Act
        var flightsRoute = FileReader.ReadFlightsRouteFile(path);

        // Assert
        Assert.NotNull(flightsRoute);
    }
    [Fact]
    public void ReadFlightsGraphFileTest()
    {
        // Arrange
        string path = @"../../../flight_in.csv";
        // Act
        var flightsGraph = FileReader.ReadFlightsGraphFile(path);

        // Assert
        Assert.NotNull(flightsGraph);
    }
}