﻿using Airlines.Persistence.InMemory.Exceptions;
using Airlines.Persistence.InMemory.Models;
using Airlines.Persistence.InMemory.Models.Interfaces;
using Airlines.Persistence.InMemory.Repository;

namespace Airlines.UnitTest;
public class RepositoryTests
{
    [Fact]
    public void Add_WhenAirlineDoesNotExist_ShouldReturnTrue()
    {
        // Arrange
        var repository = new AirlineRepository();
        var airline = new Airline("Airline 1");

        // Act
        var result = repository.Add(airline);

        // Assert
        Assert.True(result);
    }

    [Fact]
    public void Add_WhenAirlineExists_ShouldReturnFalse()
    {
        // Arrange
        var repository = new AirlineRepository();
        var airline = new Airline("Airline 1");
        _ = repository.Add(airline);

        // Act
        var result = repository.Add(airline);

        // Assert
        Assert.False(result);
    }

    [Fact]
    public void Add_WhenAirlineExists_ShouldNotAddAirline()
    {
        // Arrange
        var repository = new AirlineRepository();
        var airline = new Airline("Airline 1");
        _ = repository.Add(airline);

        // Act
        _ = repository.Add(airline);

        // Assert
        Assert.Equal(1, repository.Size);
    }

    [Fact]
    public void Add_WhenAirlineDoesNotExist_ShouldAddAirline()
    {
        // Arrange
        var repository = new AirlineRepository();
        var airline = new Airline("Airline 1");

        // Act
        _ = repository.Add(airline);

        // Assert
        Assert.Equal(1, repository.Size);
    }

    [Fact]
    public void Delete_WhenAirlineExists_ShouldReturnTrue()
    {
        // Arrange
        var repository = new AirlineRepository();
        var airline = new Airline("Airline 1");
        _ = repository.Add(airline);

        // Act
        var result = repository.Delete(airline.Name);

        // Assert
        Assert.True(result);
    }

    [Fact]
    public void Delete_WhenAirlineDoesNotExist_ShouldReturnFalse()
    {
        // Arrange
        var repository = new AirlineRepository();

        // Act
        var result = repository.Delete("Airline 1");

        // Assert
        Assert.False(result);
    }

    [Fact]
    public void Delete_WhenAirlineExists_ShouldRemoveAirline()
    {
        // Arrange
        var repository = new AirlineRepository();
        var airline = new Airline("Airline 1");
        _ = repository.Add(airline);

        // Act
        _ = repository.Delete(airline.Name);

        // Assert
        Assert.Equal(0, repository.Size);
    }

    [Fact]
    public void Delete_WhenAirlineExists_ShouldRemoveAirlineByEntity()
    {
        // Arrange
        var repository = new AirlineRepository();
        var airline = new Airline("Airline 1");
        _ = repository.Add(airline);

        // Act
        _ = repository.Delete(airline);

        // Assert
        Assert.Equal(0, repository.Size);
    }

    [Fact]
    public void DeleteAll_WhenAirlineExists_ShouldRemoveAllAirlines()
    {
        // Arrange
        var repository = new AirlineRepository();
        var airline1 = new Airline("Airline 1");
        var airline2 = new Airline("Airline 2");
        _ = repository.Add(airline1);
        _ = repository.Add(airline2);

        // Act
        repository.DeleteAll();

        // Assert
        Assert.Equal(0, repository.Size);
    }

    [Fact]
    public void Exists_WhenAirlineExists_ShouldReturnTrue()
    {
        // Arrange
        var repository = new AirlineRepository();
        var airline = new Airline("Airline 1");
        _ = repository.Add(airline);

        // Act
        var result = repository.Exists(airline.Name);

        // Assert
        Assert.True(result);
    }

    [Fact]
    public void Exists_WhenAirlineDoesNotExist_ShouldReturnFalse()
    {
        // Arrange
        var repository = new AirlineRepository();

        // Act
        var result = repository.Exists("Airline 1");

        // Assert
        Assert.False(result);
    }

    [Fact]
    public void Exists_WhenAirlineExists_ShouldReturnTrueByEntity()
    {
        // Arrange
        var repository = new AirlineRepository();
        var airline = new Airline("Airline 1");
        _ = repository.Add(airline);

        // Act
        var result = repository.Exists(airline);

        // Assert
        Assert.True(result);
    }

    [Fact]
    public void Exists_WhenAirlineDoesNotExist_ShouldReturnFalseByEntity()
    {
        // Arrange
        var repository = new AirlineRepository();

        // Act
        var result = repository.Exists(new Airline("Airline 1"));

        // Assert
        Assert.False(result);
    }

    [Fact]
    public void GetById_WhenAirline_Exists_ShouldReturnAirline()
    {
        // Arrange
        var repository = new AirlineRepository();
        var airline = new Airline("Airline 1");
        _ = repository.Add(airline);

        // Act
        var result = repository.GetById(airline.Name);

        // Assert
        Assert.Equal(airline, result);
    }

    [Fact]
    public void GetById_WhenAirline_DoesNotExist_ShouldReturnNull()
    {
        // Arrange
        var repository = new AirlineRepository();

        // Act
        var result = repository.GetById("Airline 1");

        // Assert
        Assert.Null(result);
    }

    [Fact]
    public void GetAll_WhenAirlineExists_ShouldReturnAllAirlines()
    {
        // Arrange
        var repository = new AirlineRepository();
        var airline1 = new Airline("Airline 1");
        var airline2 = new Airline("Airline 2");
        _ = repository.Add(airline1);
        _ = repository.Add(airline2);

        // Act
        var result = repository.GetAll();

        // Assert
        Assert.Collection(result, airline => Assert.Equal(airline1, airline), airline => Assert.Equal(airline2, airline));
    }

    [Fact]
    public void Update_WhenAirlineExists_ShouldReturnTrue()
    {
        // Arrange
        var repository = new AirlineRepository();
        var airline = new Airline("Airline 1");
        _ = repository.Add(airline);

        // Act
        var result = repository.Update(airline);

        // Assert
        Assert.True(result);
    }

    [Fact]
    public void Update_WhenAirlineDoesNotExist_ShouldReturnFalse()
    {
        // Arrange
        var repository = new AirlineRepository();

        // Act
        var result = repository.Update(new Airline("Airline 1"));

        // Assert
        Assert.False(result);
    }

    [Fact]
    public void Size_WhenAirlineExists_ShouldReturnSize()
    {
        // Arrange
        var repository = new AirlineRepository();
        var airline1 = new Airline("Airline 1");
        var airline2 = new Airline("Airline 2");
        _ = repository.Add(airline1);
        _ = repository.Add(airline2);

        // Act
        var result = repository.Size;

        // Assert
        Assert.Equal(2, result);
    }

    [Fact]
    public void Size_WhenAirlineDoesNotExist_ShouldReturnZero()
    {
        // Arrange
        var repository = new AirlineRepository();

        // Act
        var result = repository.Size;

        // Assert
        Assert.Equal(0, result);
    }

    [Fact]
    public void Add_WhenAircraftDoesNotExist_ShouldReturnTrue()
    {
        // Arrange
        var repository = new AircraftRepository();
        var aircraft = new Aircraft("Aircraft 1", "Type 1");

        // Act
        var result = repository.Add(aircraft);

        // Assert
        Assert.True(result);
    }

    [Fact]
    public void Add_WhenAircraftExists_ShouldReturnTrue()
    {
        // Arrange
        var repository = new AircraftRepository();
        var aircraft = new Aircraft("Aircraft 1", "Type 1");
        _ = repository.Add(aircraft);

        // Act
        var result = repository.Add(aircraft);

        // Assert
        Assert.True(result);
    }

    [Fact]
    public void Add_WhenAircraftExists_ShouldNotAddAircraft()
    {
        // Arrange
        var repository = new AircraftRepository();
        var aircraft = new Aircraft("Aircraft 1", "Type 1");
        _ = repository.Add(aircraft);

        // Act
        _ = repository.Add(aircraft);

        // Assert
        Assert.Equal(1, repository.Size);
    }

    [Fact]
    public void Add_WhenAircraftDoesNotExist_ShouldAddAircraft()
    {
        // Arrange
        var repository = new AircraftRepository();
        var aircraft = new Aircraft("Aircraft 1", "Type 1");

        // Act
        _ = repository.Add(aircraft);

        // Assert
        Assert.Equal(1, repository.Size);
    }

    [Fact]
    public void Delete_WhenAircraftExists_ShouldReturnFalse()
    {
        // Arrange
        var repository = new AircraftRepository();
        var aircraft = new Aircraft("Aircraft 1", "Type 1");
        _ = repository.Add(aircraft);

        // Act
        var result = repository.Delete(aircraft.Model);

        // Assert
        Assert.False(result);
    }

    [Fact]
    public void Delete_WhenAircraftDoesNotExist_ShouldReturnFalse()
    {
        // Arrange
        var repository = new AircraftRepository();

        // Act
        var result = repository.Delete("Aircraft 1");

        // Assert
        Assert.False(result);
    }

    [Fact]
    public void Delete_WhenAircraftExists_ShouldRemoveAircraft()
    {
        // Arrange
        var repository = new AircraftRepository();
        var aircraft = new Aircraft("Aircraft 1", "Type 1");
        _ = repository.Add(aircraft);

        // Act
        _ = repository.Delete(aircraft.Model);

        // Assert
        Assert.Equal(1, repository.Size);
    }

    [Fact]
    public void Delete_WhenAircraftExists_ShouldRemoveAircraftByEntity()
    {
        // Arrange
        var repository = new AircraftRepository();
        var aircraft = new Aircraft("Aircraft 1", "Type 1");
        _ = repository.Add(aircraft);

        // Act
        _ = repository.Delete(aircraft);

        // Assert
        Assert.Equal(1, repository.Size);
    }

    [Fact]
    public void DeleteAll_WhenAircraftExists_ShouldRemoveAllAircrafts()
    {
        // Arrange
        var repository = new AircraftRepository();
        var aircraft1 = new Aircraft("Aircraft 1", "Type 1");
        var aircraft2 = new Aircraft("Aircraft 2", "Type 2");
        _ = repository.Add(aircraft1);
        _ = repository.Add(aircraft2);

        // Act
        repository.DeleteAll();

        // Assert
        Assert.Equal(0, repository.Size);
    }

    [Fact]
    public void Exists_WhenAircraftDoesntExists_ShouldReturnFalse()
    {
        // Arrange
        var repository = new AircraftRepository();
        var aircraft = new Aircraft("Aircraft 1", "Type 1");
        _ = repository.Add(aircraft);

        // Act
        var result = repository.Exists(aircraft.Model);

        // Assert
        Assert.False(result);
    }

    [Fact]
    public void Exists_WhenAircraftExists_ShouldReturnTrue()
    {
        // Arrange
        var repository = new AircraftRepository();

        // Act
        var result = repository.Exists("Aircraft 1");

        // Assert
        Assert.False(result);
    }

    [Fact]
    public void Exists_WhenAircraftDoesntExists_ShouldReturnFalseByEntity()
    {
        // Arrange
        var repository = new AircraftRepository();
        var aircraft = new Aircraft("Aircraft 1", "Type 1");
        _ = repository.Add(aircraft);

        // Act
        var result = repository.Exists(aircraft);

        // Assert
        Assert.False(result);
    }

    [Fact]
    public void Exists_WhenAircraftExists_ShouldReturnTrueByEntity()
    {
        // Arrange
        var repository = new AircraftRepository();

        // Act
        var result = repository.Exists(new Aircraft("Aircraft 1", "Type 1"));

        // Assert
        Assert.False(result);
    }

    [Fact]
    public void GetById_WhenAircraftDoesntExists_ShouldReturnNull()
    {
        // Arrange
        var repository = new AircraftRepository();
        var aircraft = new Aircraft("Aircraft 1", "Type 1");
        _ = repository.Add(aircraft);

        // Act
        var result = repository.GetById(aircraft.Model);

        // Assert
        Assert.Null(result);
    }

    [Fact]
    public void GetByFlight_WhenAircraftExists_ShouldReturnAircraft()
    {
        // Arrange
        var repository = new AircraftRepository();
        var aircraft = new Aircraft("Aircraft 1", "Type 1");
        _ = repository.Add(aircraft);
        var flight = new Flight("Flight 1", "Departure 1", "Arrival 1", "Aircraft 1", 100, 1);

        // Act
        var result = repository.GetByFlight(flight);

        // Assert
        Assert.Null(result);
    }

    [Fact]
    public void GetAll_WhenAircraftExists_ShouldReturnAllAircrafts()
    {
        // Arrange
        var repository = new AircraftRepository();
        var aircraft1 = new Aircraft("Aircraft 1", "Type 1");
        var aircraft2 = new Aircraft("Aircraft 2", "Type 2");
        _ = repository.Add(aircraft1);
        _ = repository.Add(aircraft2);

        // Act
        var result = repository.GetAll();

        // Assert
        Assert.Collection(result, aircraft => Assert.Equal(aircraft1, aircraft), aircraft => Assert.Equal(aircraft2, aircraft));
    }

    [Fact]
    public void Update_WhenAircraftExists_ShouldReturnFalse()
    {
        // Arrange
        var repository = new AircraftRepository();
        var aircraft = new Aircraft("Aircraft 1", "Type 1");
        _ = repository.Add(aircraft);

        // Act
        var result = repository.Update(aircraft);

        // Assert
        Assert.False(result);
    }

    [Fact]
    public void Update_WhenAircraftDoesntExists_ShouldReturnFalse()
    {
        // Arrange
        var repository = new AircraftRepository();

        // Act
        var result = repository.Update(new Aircraft("Aircraft 1", "Type 1"));

        // Assert
        Assert.False(result);
    }

    [Fact]
    public void Size_WhenAircraftExists_ShouldReturnSize()
    {
        // Arrange
        var repository = new AircraftRepository();
        var aircraft1 = new Aircraft("Aircraft 1", "Type 1");
        var aircraft2 = new Aircraft("Aircraft 2", "Type 2");
        _ = repository.Add(aircraft1);
        _ = repository.Add(aircraft2);

        // Act
        var result = repository.Size;

        // Assert
        Assert.Equal(2, result);
    }

    [Fact]
    public void Size_WhenAircraftDoesntExists_ShouldReturnZero()
    {
        // Arrange
        var repository = new AircraftRepository();

        // Act
        var result = repository.Size;

        // Assert
        Assert.Equal(0, result);
    }

    [Fact]
    public void NormalizeModelName_WhenModelNameExists_ShouldReturnNormalizedModelName()
    {
        // Arrange
        var modelName = "Model Name";

        // Act
        var result = Aircraft.NormalizeModelName(modelName);

        // Assert
        Assert.Equal("MODELNAME", result);
    }

    [Fact]
    public void ToString_WhenModelExists_ShouldReturnModel()
    {
        // Arrange
        var model = "Model Name";
        var aircraft = new Aircraft(model, "Type 1");

        // Act
        var result = aircraft.ToString();

        // Assert
        Assert.Equal(model, result);
    }

    [Fact]
    public void FlightValidation_WhenCheckExists_ShouldReturnTrue()
    {
        // Arrange
        var check = "Check";

        // Act
        var result = Flight.FlightValidation(check);

        // Assert
        Assert.True(result);
    }

    [Fact]
    public void FlightValidation_WhenCheckDoesntExists_ShouldReturnFalse()
    {
        // Arrange
        var check = "";

        // Act
        var result = Flight.FlightValidation(check);

        // Assert
        Assert.False(result);
    }

    [Fact]
    public void ToString_WhenFlightExists_ShouldReturnFlight()
    {
        // Arrange
        var flight = new Flight("Flight 1", "Departure 1", "Arrival 1", "Aircraft 1", 100, 1);

        // Act
        var result = flight.ToString();

        // Assert
        Assert.Equal("Flight Flight 1: From Departure 1 to Arrival 1 with Aircraft 1, Price 100, Duration1", result);
    }

    [Fact]
    public void Flight_WhenIdentifierExists_ShouldReturnFlight()
    {
        // Arrange
        var identifier = "Flight 1";
        var departureAirport = "Departure 1";
        var arrivalAirport = "Arrival 1";
        var aircraft = "Aircraft 1";
        var price = 100;
        var duration = 1;

        // Act
        var result = new Flight(identifier, departureAirport, arrivalAirport, aircraft, price, duration);

        // Assert
        Assert.Equal(identifier, result.Identifier);
        Assert.Equal(departureAirport, result.DepartureAirport);
        Assert.Equal(arrivalAirport, result.ArrivalAirport);
        Assert.Equal(aircraft, result.Aircraft);
        Assert.Equal(price, result.Price);
        Assert.Equal(duration, result.Duration);
    }

    [Fact]
    public void Flight_WhenIdentifierDoesntExists_ShouldThrowInvalidFlightCodeException()
    {
        // Arrange
        var identifier = "";
        var departureAirport = "Departure 1";
        var arrivalAirport = "Arrival 1";
        var aircraft = "Aircraft 1";
        var price = 100;
        var duration = 1;

        // Act
        void Action()
        {
            _ = new Flight(identifier, departureAirport, arrivalAirport, aircraft, price, duration);
        }

        // Assert
        _ = Assert.Throws<InvalidFlightCodeException>(Action);
    }

    [Fact]
    public void Set_WhenEntityExists_ShouldSetEntity()
    {
        // Arrange
        var repository = new AircraftRepository();
        var aircraft = new Aircraft("Aircraft 1", "Type 1");

        // Act
        repository.Set(aircraft);

        // Assert
        Assert.Equal(1, repository.Size);
    }

    [Fact]
    public void Size_WhenEntityExists_ShouldReturnSize()
    {
        // Arrange
        var repository = new AircraftRepository();
        var aircraft1 = new Aircraft("Aircraft 1", "Type 1");
        var aircraft2 = new Aircraft("Aircraft 2", "Type 2");
        _ = repository.Add(aircraft1);
        _ = repository.Add(aircraft2);

        // Act
        var result = repository.Size;

        // Assert
        Assert.Equal(2, result);
    }

    [Fact]
    public void CitySize_WhenEntityExists_ShouldReturnCitySize()
    {
        // Arrange
        var repository = new AirportRepository();
        var airport1 = new Airport("Airport 1", "Name 1", "City 1", "Country 1");
        var airport2 = new Airport("Airport 2", "Name 2", "City 2", "Country 2");
        _ = repository.Add(airport1);
        _ = repository.Add(airport2);

        // Act
        var result = repository.CitySize;

        // Assert
        Assert.Equal(2, result);
    }

    [Fact]
    public void CountrySize_WhenEntityExists_ShouldReturnCountrySize()
    {
        // Arrange
        var repository = new AirportRepository();
        var airport1 = new Airport("Airport 1", "Name 1", "City 1", "Country 1");
        var airport2 = new Airport("Airport 2", "Name 2", "City 2", "Country 2");
        _ = repository.Add(airport1);
        _ = repository.Add(airport2);

        // Act
        var result = repository.CountrySize;

        // Assert
        Assert.Equal(2, result);
    }

    [Fact]
    public void Add_WhenEntityDoesntExists_ShouldReturnTrue()
    {
        // Arrange
        var repository = new AirportRepository();
        var airport = new Airport("Airport 1", "Name 1", "City 1", "Country 1");

        // Act
        var result = repository.Add(airport);

        // Assert
        Assert.True(result);
    }

    [Fact]
    public void Add_WhenEntityExists_ShouldReturnFalse()
    {
        // Arrange
        var repository = new AirportRepository();
        var airport = new Airport("Airport 1", "Name 1", "City 1", "Country 1");
        _ = repository.Add(airport);

        // Act
        var result = repository.Add(airport);

        // Assert
        Assert.False(result);
    }

    [Fact]
    public void Add_WhenEntityExists_ShouldNotAddEntity()
    {
        // Arrange
        var repository = new AirportRepository();
        var airport = new Airport("Airport 1", "Name 1", "City 1", "Country 1");
        _ = repository.Add(airport);

        // Act
        _ = repository.Add(airport);

        // Assert
        Assert.Equal(1, repository.Size);
    }

    [Fact]
    public void Add_WhenEntityDoesntExists_ShouldAddEntity()
    {
        // Arrange
        var repository = new AirportRepository();
        var airport = new Airport("Airport 1", "Name 1", "City 1", "Country 1");

        // Act
        _ = repository.Add(airport);

        // Assert
        Assert.Equal(1, repository.Size);
    }

    [Fact]
    public void Delete_WhenEntityExists_ShouldReturnTrue()
    {
        // Arrange
        var repository = new AirportRepository();
        var airport = new Airport("Airport 1", "Name 1", "City 1", "Country 1");
        _ = repository.Add(airport);

        // Act
        var result = repository.Delete(airport.Identifier);

        // Assert
        Assert.True(result);
    }

    [Fact]
    public void Delete_WhenEntityDoesntExists_ShouldReturnFalse()
    {
        // Arrange
        var repository = new AirportRepository();

        // Act
        var result = repository.Delete("Airport 1");

        // Assert
        Assert.False(result);
    }

    [Fact]
    public void Delete_WhenEntityExists_ShouldRemoveEntity()
    {
        // Arrange
        var repository = new AirportRepository();
        var airport = new Airport("Airport 1", "Name 1", "City 1", "Country 1");
        _ = repository.Add(airport);

        // Act
        _ = repository.Delete(airport.Identifier);

        // Assert
        Assert.Equal(0, repository.Size);
    }

    [Fact]
    public void Delete_WhenEntityExists_ShouldRemoveEntityByEntity()
    {
        // Arrange
        var repository = new AirportRepository();
        var airport = new Airport("Airport 1", "Name 1", "City 1", "Country 1");
        _ = repository.Add(airport);

        // Act
        _ = repository.Delete(airport);

        // Assert
        Assert.Equal(0, repository.Size);
    }

    [Fact]
    public void DeleteAll_WhenEntityExists_ShouldRemoveAllEntities()
    {
        // Arrange
        var repository = new AirportRepository();
        var airport1 = new Airport("Airport 1", "Name 1", "City 1", "Country 1");
        var airport2 = new Airport("Airport 2", "Name 2", "City 2", "Country 2");
        _ = repository.Add(airport1);
        _ = repository.Add(airport2);

        // Act
        repository.DeleteAll();

        // Assert
        Assert.Equal(0, repository.Size);
    }

    [Fact]
    public void Exists_WhenEntityExists_ShouldReturnTrue()
    {
        // Arrange
        var repository = new AirportRepository();
        var airport = new Airport("Airport 1", "Name 1", "City 1", "Country 1");
        _ = repository.Add(airport);

        // Act
        var result = repository.Exists(airport.Identifier);

        // Assert
        Assert.True(result);
    }

    [Fact]
    public void Exists_WhenEntityDoesntExists_ShouldReturnFalse()
    {
        // Arrange
        var repository = new AirportRepository();

        // Act
        var result = repository.Exists("Airport 1");

        // Assert
        Assert.False(result);
    }

    [Fact]
    public void Exists_WhenEntityExists_ShouldReturnTrueByEntity()
    {
        // Arrange
        var repository = new AirportRepository();
        var airport = new Airport("Airport 1", "Name 1", "City 1", "Country 1");
        _ = repository.Add(airport);

        // Act
        var result = repository.Exists(airport);

        // Assert
        Assert.True(result);
    }

    [Fact]
    public void Exists_WhenEntityDoesntExists_ShouldReturnFalseByEntity()
    {
        // Arrange
        var repository = new AirportRepository();

        // Act
        var result = repository.Exists(new Airport("Airport 1", "Name 1", "City 1", "Country 1"));

        // Assert
        Assert.False(result);
    }

    [Fact]
    public void GetById_WhenEntityExists_ShouldReturnEntity()
    {
        // Arrange
        var repository = new AirportRepository();
        var airport = new Airport("Airport 1", "Name 1", "City 1", "Country 1");
        _ = repository.Add(airport);

        // Act
        var result = repository.GetById(airport.Identifier);

        // Assert
        Assert.Equal(airport, result);
    }

    [Fact]
    public void GetById_WhenEntityDoesntExists_ShouldReturnNull()
    {
        // Arrange
        var repository = new AirportRepository();

        // Act
        var result = repository.GetById("Airport 1");

        // Assert
        Assert.Null(result);
    }

    [Fact]
    public void GetAll_WhenEntityExists_ShouldReturnAllEntities()
    {
        // Arrange
        var repository = new AirportRepository();
        var airport1 = new Airport("Airport 1", "Name 1", "City 1", "Country 1");
        var airport2 = new Airport("Airport 2", "Name 2", "City 2", "Country 2");
        _ = repository.Add(airport1);
        _ = repository.Add(airport2);

        // Act
        var result = repository.GetAll();

        // Assert
        Assert.Collection(result, airport => Assert.Equal(airport1, airport), airport => Assert.Equal(airport2, airport));
    }

    [Fact]
    public void GetAllByCountry_WhenEntityExists_ShouldReturnAllEntitiesByCountry()
    {
        // Arrange
        var repository = new AirportRepository();
        var airport1 = new Airport("Airport 1", "Name 1", "City 1", "Country 1");
        var airport2 = new Airport("Airport 2", "Name 2", "City 2", "Country 1");
        _ = repository.Add(airport1);
        _ = repository.Add(airport2);

        // Act
        var result = repository.GetAllByCountry("Country 1");

        // Assert
        Assert.Collection(result, airport => Assert.Equal(airport1, airport), airport => Assert.Equal(airport2, airport));
    }

    [Fact]
    public void GetAllByCountry_WhenEntityDoesntExists_ShouldReturnEmpty()
    {
        // Arrange
        var repository = new AirportRepository();

        // Act
        var result = repository.GetAllByCountry("Country 1");

        // Assert
        Assert.Empty(result);
    }

    [Fact]
    public void GetAllByCity_WhenEntityExists_ShouldReturnAllEntitiesByCity()
    {
        // Arrange
        var repository = new AirportRepository();
        var airport1 = new Airport("Airport 1", "Name 1", "City 1", "Country 1");
        var airport2 = new Airport("Airport 2", "Name 2", "City 1", "Country 2");
        _ = repository.Add(airport1);
        _ = repository.Add(airport2);

        // Act
        var result = repository.GetAllByCity("City 1");

        // Assert
        Assert.Collection(result, airport => Assert.Equal(airport1, airport), airport => Assert.Equal(airport2, airport));
    }

    [Fact]
    public void GetAllByCity_WhenEntityDoesntExists_ShouldReturnEmpty()
    {
        // Arrange
        var repository = new AirportRepository();

        // Act
        var result = repository.GetAllByCity("City 1");

        // Assert
        Assert.Empty(result);
    }

    [Fact]
    public void Update_WhenEntityExists_ShouldReturnTrue()
    {
        // Arrange
        var repository = new AirportRepository();
        var airport = new Airport("Airport 1", "Name 1", "City 1", "Country 1");
        _ = repository.Add(airport);

        // Act
        var result = repository.Update(airport);

        // Assert
        Assert.True(result);
    }

    [Fact]
    public void Update_WhenEntityDoesntExists_ShouldReturnFalse()
    {
        // Arrange
        var repository = new AirportRepository();

        // Act
        var result = repository.Update(new Airport("Airport 1", "Name 1", "City 1", "Country 1"));

        // Assert
        Assert.False(result);
    }

    [Fact]
    public void Airport_WhenIdentifierExists_ShouldReturnAirport()
    {
        // Arrange
        var identifier = "Airport 1";
        var name = "Name 1";
        var city = "City 1";
        var country = "Country 1";

        // Act
        var result = new Airport(identifier, name, city, country);

        // Assert
        Assert.Equal(identifier, result.Identifier);
        Assert.Equal(name, result.Name);
        Assert.Equal(city, result.City);
        Assert.Equal(country, result.Country);
    }

    [Fact]
    public void Set_WhenEntityExists_Airport_ShouldSetEntity()
    {
        // Arrange
        var repository = new AirportRepository();
        var airport = new Airport("Airport 1", "Name 1", "City 1", "Country 1");

        // Act
        repository.Set(airport);

        // Assert
        Assert.Equal(1, repository.Size);
    }

    [Fact]
    public void Add_WhenFlightExists_ShouldReturnFalse()
    {
        // Arrange
        var repository = new FlightRepository(new AircraftRepository());
        var flight = new Flight("Flight 1", "Departure 1", "Arrival 1", "Aircraft 1", 100, 1);

        // Act
        var result = repository.Add(flight);

        // Assert
        Assert.False(result);
    }

    [Fact]
    public void Add_WhenFlightDoesntExists_ShouldReturnFalse()
    {
        // Arrange
        var repository = new FlightRepository(new AircraftRepository());
        var flight = new Flight("Flight 1", "Departure 1", "Arrival 1", "Aircraft 1", 100, 1);
        _ = repository.Add(flight);

        // Act
        var result = repository.Add(flight);

        // Assert
        Assert.False(result);
    }

    [Fact]
    public void Add_WhenFlightDoesntExists_ShouldNotAddFlight()
    {
        // Arrange
        var repository = new FlightRepository(new AircraftRepository());
        var flight = new Flight("Flight 1", "Departure 1", "Arrival 1", "Aircraft 1", 100, 1);
        _ = repository.Add(flight);

        // Act
        _ = repository.Add(flight);

        // Assert
        Assert.Equal(0, repository.Size);
    }

    [Fact]
    public void Delete_WhenFlightExists_ShouldReturnFalse()
    {
        // Arrange
        var repository = new FlightRepository(new AircraftRepository());
        var flight = new Flight("Flight 1", "Departure 1", "Arrival 1", "Aircraft 1", 100, 1);
        _ = repository.Add(flight);

        // Act
        var result = repository.Delete(flight.Identifier);

        // Assert
        Assert.False(result);
    }

    [Fact]
    public void Delete_WhenFlightDoesntExists_ShouldReturnFalse()
    {
        // Arrange
        var repository = new FlightRepository(new AircraftRepository());

        // Act
        var result = repository.Delete("Flight 1");

        // Assert
        Assert.False(result);
    }

    [Fact]
    public void Delete_WhenFlightExists_ShouldRemoveFlight()
    {
        // Arrange
        var repository = new FlightRepository(new AircraftRepository());
        var flight = new Flight("Flight 1", "Departure 1", "Arrival 1", "Aircraft 1", 100, 1);
        _ = repository.Add(flight);

        // Act
        _ = repository.Delete(flight.Identifier);

        // Assert
        Assert.Equal(0, repository.Size);
    }

    [Fact]
    public void Delete_WhenFlightExists_ShouldRemoveFlightByEntity()
    {
        // Arrange
        var repository = new FlightRepository(new AircraftRepository());
        var flight = new Flight("Flight 1", "Departure 1", "Arrival 1", "Aircraft 1", 100, 1);
        _ = repository.Add(flight);

        // Act
        _ = repository.Delete(flight);

        // Assert
        Assert.Equal(0, repository.Size);
    }

    [Fact]
    public void DeleteAll_WhenFlightExists_ShouldRemoveAllFlights()
    {
        // Arrange
        var repository = new FlightRepository(new AircraftRepository());
        var flight1 = new Flight("Flight 1", "Departure 1", "Arrival 1", "Aircraft 1", 100, 1);
        var flight2 = new Flight("Flight 2", "Departure 2", "Arrival 2", "Aircraft 2", 200, 2);
        _ = repository.Add(flight1);
        _ = repository.Add(flight2);

        // Act
        repository.DeleteAll();

        // Assert
        Assert.Equal(0, repository.Size);
    }

    [Fact]
    public void Exists_WhenNotFlightExists_ShouldReturnFalse()
    {
        // Arrange
        var repository = new FlightRepository(new AircraftRepository());
        var flight = new Flight("Flight 1", "Departure 1", "Arrival 1", "Aircraft 1", 100, 1);
        _ = repository.Add(flight);

        // Act
        var result = repository.Exists(flight.Identifier);

        // Assert
        Assert.False(result);
    }

    [Fact]
    public void Exists_WhenFlightExists_ShouldReturnTrue()
    {
        // Arrange
        var repository = new FlightRepository(new AircraftRepository());

        // Act
        var result = repository.Exists("Flight 1");

        // Assert
        Assert.False(result);
    }

    [Fact]
    public void Exists_WhenNotFlightExists_ShouldReturnFalseByEntity()
    {
        // Arrange
        var repository = new FlightRepository(new AircraftRepository());
        var flight = new Flight("Flight 1", "Departure 1", "Arrival 1", "Aircraft 1", 100, 1);
        _ = repository.Add(flight);

        // Act
        var result = repository.Exists(flight);

        // Assert
        Assert.False(result);
    }

    [Fact]
    public void Exists_WhenFlightExists_ShouldReturnTrueByEntity()
    {
        // Arrange
        var repository = new FlightRepository(new AircraftRepository());

        // Act
        var result = repository.Exists(new Flight("Flight 1", "Departure 1", "Arrival 1", "Aircraft 1", 100, 1));

        // Assert
        Assert.False(result);
    }

    [Fact]
    public void GetById_WhenFlightExists_ShouldReturnFlight()
    {
        // Arrange
        var repository = new FlightRepository(new AircraftRepository());
        var flight = new Flight("Flight 1", "Departure 1", "Arrival 1", "Aircraft 1", 100, 1);
        _ = repository.Add(flight);

        // Act
        var result = repository.GetById(flight.Identifier);

        // Assert
        Assert.Null(result);
    }

    [Fact]
    public void Update_WhenFlightExists_ShouldReturnFalse()
    {
        // Arrange
        var repository = new FlightRepository(new AircraftRepository());
        var flight = new Flight("Flight 1", "Departure 1", "Arrival 1", "Aircraft 1", 100, 1);
        _ = repository.Add(flight);

        // Act
        var result = repository.Update(flight);

        // Assert
        Assert.False(result);
    }

    [Fact]
    public void Update_WhenFlightDoesntExists_ShouldReturnFalse()
    {
        // Arrange
        var repository = new FlightRepository(new AircraftRepository());

        // Act
        var result = repository.Update(new Flight("Flight 1", "Departure 1", "Arrival 1", "Aircraft 1", 100, 1));

        // Assert
        Assert.False(result);
    }

    [Fact]
    public void Set_WhenFlightExists_ShouldSetFlight()
    {
        // Arrange
        var repository = new FlightRepository(new AircraftRepository());
        var flight = new Flight("Flight 1", "Departure 1", "Arrival 1", "Aircraft 1", 100, 1);

        // Act
        repository.Set(flight);

        // Assert
        Assert.Equal(1, repository.Size);
    }

    [Fact]
    public void Size_WhenFlightExists_ShouldReturnSize()
    {
        // Arrange
        var repository = new FlightRepository(new AircraftRepository());
        var flight1 = new Flight("Flight 1", "Departure 1", "Arrival 1", "Aircraft 1", 100, 1);
        var flight2 = new Flight("Flight 2", "Departure 2", "Arrival 2", "Aircraft 2", 200, 2);
        _ = repository.Add(flight1);
        _ = repository.Add(flight2);

        // Act
        var result = repository.Size;

        // Assert
        Assert.Equal(0, result);
    }

    [Fact]
    public void Add_NewRoute_ShouldReturnTrue()
    {
        // Arrange
        var repository = new RouteRepository();
        var flights = new List<IFlight> { new Flight("21", "313", "JFK", "Aircraft", 231, 21) };
        var route = new Route(flights) { Identifier = "R1" };

        // Act
        var result = repository.Add(route);

        // Assert
        Assert.True(result);
        Assert.Equal(1, repository.Size);
        Assert.Contains(route, repository.GetAll());
    }
    [Fact]
    public void Add_ExistingRoute_ShouldReturnFalse()
    {
        // Arrange
        var repository = new RouteRepository();
        var flights = new List<IFlight> { new Flight("21", "313", "JFK", "Aircraft", 231, 21) };
        var route = new Route(flights) { Identifier = "R1" };
        _ = repository.Add(route);

        // Act
        var result = repository.Add(route);

        // Assert
        Assert.False(result);
        Assert.Equal(1, repository.Size);
        Assert.Contains(route, repository.GetAll());
    }
    [Fact]
    public void Delete_ExistingRoute_ShouldReturnTrue()
    {
        // Arrange
        var repository = new RouteRepository();
        var flights = new List<IFlight> { new Flight("21", "313", "JFK", "Aircraft", 231, 21) };
        var route = new Route(flights) { Identifier = "R1" };
        _ = repository.Add(route);

        // Act
        var result = repository.Delete(route.Identifier);

        // Assert
        Assert.True(result);
        Assert.Equal(0, repository.Size);
        Assert.DoesNotContain(route, repository.GetAll());
    }
    [Fact]
    public void Delete_NonExistingRoute_ShouldReturnFalse()
    {
        // Arrange
        var repository = new RouteRepository();

        // Act
        var result = repository.Delete("R1");

        // Assert
        Assert.False(result);
        Assert.Equal(0, repository.Size);
    }
    [Fact]
    public void DeleteAll_ExistingRoutes_ShouldRemoveAllRoutes()
    {
        // Arrange
        var repository = new RouteRepository();
        var flights1 = new List<IFlight> { new Flight("21", "313", "JFK", "Aircraft", 231, 21) };
        var flights2 = new List<IFlight> { new Flight("22", "314", "JFK", "Aircraft", 232, 22) };
        var route1 = new Route(flights1) { Identifier = "R1" };
        var route2 = new Route(flights2) { Identifier = "R2" };
        _ = repository.Add(route1);
        _ = repository.Add(route2);

        // Act
        repository.DeleteAll();

        // Assert
        Assert.Equal(0, repository.Size);
        Assert.DoesNotContain(route1, repository.GetAll());
        Assert.DoesNotContain(route2, repository.GetAll());
    }

    [Fact]
    public void Exists_ExistingRoute_ShouldReturnTrue()
    {
        // Arrange
        var repository = new RouteRepository();
        var flights = new List<IFlight> { new Flight("21", "313", "JFK", "Aircraft", 231, 21) };
        var route = new Route(flights) { Identifier = "R1" };
        _ = repository.Add(route);

        // Act
        var result = repository.Exists(route.Identifier);

        // Assert
        Assert.True(result);
    }

    [Fact]
    public void Exists_NonExistingRoute_ShouldReturnFalse()
    {
        // Arrange
        var repository = new RouteRepository();

        // Act
        var result = repository.Exists("R1");

        // Assert
        Assert.False(result);
    }

    [Fact]
    public void Exists_ExistingRoute_ShouldReturnTrueByEntity()
    {
        // Arrange
        var repository = new RouteRepository();
        var flights = new List<IFlight> { new Flight("21", "313", "JFK", "Aircraft", 231, 21) };
        var route = new Route(flights) { Identifier = "R1" };
        _ = repository.Add(route);

        // Act
        var result = repository.Exists(route);

        // Assert
        Assert.True(result);
    }

    [Fact]
    public void RouteRepository_GetById_ExistingRoute_ShouldReturnRoute()
    {
        // Arrange
        var repository = new RouteRepository();
        var flights = new List<IFlight> { new Flight("21", "313", "JFK", "Aircraft", 231, 21) };
        var route = new Route(flights) { Identifier = "R1" };
        _ = repository.Add(route);

        // Act
        var result = repository.GetById(route.Identifier);

        // Assert
        Assert.Equal(route, result);
    }

    [Fact]
    public void RouteRepository_GetById_NonExistingRoute_ShouldReturnNull()
    {
        // Arrange
        var repository = new RouteRepository();

        // Act
        var result = repository.GetById("R1");

        // Assert
        Assert.Null(result);
    }

    [Fact]
    public void RouteRepository_GetAll_ExistingRoutes_ShouldReturnAllRoutes()
    {
        // Arrange
        var repository = new RouteRepository();
        var flights1 = new List<IFlight> { new Flight("21", "313", "JFK", "Aircraft", 231, 21) };
        var flights2 = new List<IFlight> { new Flight("22", "314", "JFK", "Aircraft", 232, 22) };
        var route1 = new Route(flights1) { Identifier = "R1" };
        var route2 = new Route(flights2) { Identifier = "R2" };
        _ = repository.Add(route1);
        _ = repository.Add(route2);

        // Act
        var result = repository.GetAll();

        // Assert
        Assert.Collection(result, route => Assert.Equal(route1, route), route => Assert.Equal(route2, route));
    }

    [Fact]
    public void RouteRepository_Update_ExistingRoute_ShouldReturnTrue()
    {
        // Arrange
        var repository = new RouteRepository();
        var flights = new List<IFlight> { new Flight("21", "313", "JFK", "Aircraft", 231, 21) };
        var route = new Route(flights) { Identifier = "R1" };
        _ = repository.Add(route);

        // Act
        var result = repository.Update(route);

        // Assert
        Assert.True(result);
    }
    // test it:     public IEnumerable<Flight> GetAll() => _flights.Values;
    [Fact]
    public void FlightRepository_GetAll_ExistingFlights_ShouldReturnAllFlights()
    {
        // Arrange
        var repository = new FlightRepository(new AircraftRepository());
        var flight1 = new Flight("21", "313", "JFK", "Aircraft", 231, 21);
        var flight2 = new Flight("22", "314", "JFK", "Aircraft", 232, 22);
        _ = repository.Add(flight1);
        _ = repository.Add(flight2);

        // Act
        _ = repository.GetAll();
    }
}