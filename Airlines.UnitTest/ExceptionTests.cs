﻿using Airlines.Persistence.InMemory;
using Airlines.Persistence.InMemory.Exceptions;

namespace Airlines.UnitTest;
public class ExceptionTests
{
    [Fact]
    public void ExecutionBatchExceptionTest()
    {
        // Arrange
        var message = "Test message";
        var innerException = new Exception("Inner exception");

        // Act
        var exception = new ExecutionBatchException(message, innerException);

        // Assert
        Assert.Equal(message, exception.Message);
        Assert.Equal(innerException, exception.InnerException);
    }

    [Fact]
    public void ExecutionReservationExceptionTest()
    {
        // Arrange
        var message = "Test message";
        var innerException = new Exception("Inner exception");

        // Act
        var exception = new ExecutionReservationException(message, innerException);

        // Assert
        Assert.Equal(message, exception.Message);
        Assert.Equal(innerException, exception.InnerException);
    }

    [Fact]
    public void ExecutionRouteExceptionTest()
    {
        // Arrange
        var message = "Test message";
        var innerException = new Exception("Inner exception");

        // Act
        var exception = new ExecutionRouteException(message, innerException);

        // Assert
        Assert.Equal(message, exception.Message);
        Assert.Equal(innerException, exception.InnerException);
    }

    [Fact]
    public void ExecutionSearchExceptionTest()
    {
        // Arrange
        var message = "Test message";
        var innerException = new Exception("Inner exception");

        // Act
        var exception = new ExecutionSearchException(message, innerException);

        // Assert
        Assert.Equal(message, exception.Message);
        Assert.Equal(innerException, exception.InnerException);
    }

    [Fact]
    public void ExecutionSortExceptionTest()
    {
        // Arrange
        var message = "Test message";
        var innerException = new Exception("Inner exception");

        // Act
        var exception = new ExecutionSortException(message, innerException);

        // Assert
        Assert.Equal(message, exception.Message);
        Assert.Equal(innerException, exception.InnerException);
    }

    [Fact]
    public void InvalidAirlineListExceptionTest()
    {
        // Arrange
        var message = "Test message";
        var innerException = new Exception("Inner exception");

        // Act
        var exception = new InvalidAirlineListException(message, innerException);

        // Assert
        Assert.Equal(message, exception.Message);
        Assert.Equal(innerException, exception.InnerException);
    }

    [Fact]
    public void InvalidAirlineNameExceptionTest()
    {
        // Arrange
        var message = "Test message";
        // Act
        var exception = new InvalidAirlineNameException(message);

        // Assert
        Assert.Equal(message, exception.Message);
    }

    [Fact]
    public void InvalidCommandExceptionTest()
    {
        // Arrange
        var message = "Test message";
        // Act
        var exception = new InvalidCommandException(message);

        // Assert
        Assert.Equal(message, exception.Message);
    }

    [Fact]
    public void InvalidFlightCodeExceptionTest()
    {
        // Arrange
        var message = "Test message";
        // Act
        var exception = new InvalidFlightCodeException(message);

        // Assert
        Assert.Equal(message, exception.Message);
    }

    [Fact]
    public void InvalidInputExceptionTest()
    {
        // Arrange
        var message = "Test message";
        // Act
        var exception = new InvalidInputException(message);

        // Assert
        Assert.Equal(message, exception.Message);
    }

    [Fact]
    public void InvalidRouteExceptionTest()
    {
        // Arrange
        var message = "Test message";
        // Act
        var exception = new InvalidRouteException(message);

        // Assert
        Assert.Equal(message, exception.Message);
    }

    [Fact]
    public void InvalidRouteException_DefaultConstructorTest()
    {
        // Arrange

        // Act
        var exception = new InvalidRouteException();

        // Assert
        Assert.NotNull(exception);
    }
    [Fact]
    public void InvalidRouteException_InnerExceptionTest()
    {
        // Arrange
        var message = "Test message";
        var innerException = new Exception("Inner exception");

        // Act
        var exception = new InvalidRouteException(message, innerException);

        // Assert
        Assert.Equal(message, exception.Message);
        Assert.Equal(innerException, exception.InnerException);
    }

    [Fact]
    public void ExecutionBatchException_Constructor()
    {
        // Arrange
        var message = "Test message";

        // Act
        var exception = new ExecutionBatchException(message);

        // Assert
        Assert.Equal(message, exception.Message);
    }

    [Fact]
    public void ExecutionBatchException_DefaultConstructorTest()
    {
        // Arrange

        // Act
        var exception = new ExecutionBatchException();

        // Assert
        Assert.NotNull(exception);
    }

    [Fact]
    public void ExecutionReservationException_Constructor()
    {
        // Arrange
        var message = "Test message";

        // Act
        var exception = new ExecutionReservationException(message);

        // Assert
        Assert.Equal(message, exception.Message);
    }

    [Fact]
    public void ExecutionReservationException_DefaultConstructorTest()
    {
        // Arrange

        // Act
        var exception = new ExecutionReservationException();

        // Assert
        Assert.NotNull(exception);
    }

    [Fact]
    public void ExecutionRouteException_Constructor()
    {
        // Arrange
        var message = "Test message";

        // Act
        var exception = new ExecutionRouteException(message);

        // Assert
        Assert.Equal(message, exception.Message);
    }

    [Fact]
    public void ExecutionRouteException_DefaultConstructorTest()
    {
        // Arrange

        // Act
        var exception = new ExecutionRouteException();

        // Assert
        Assert.NotNull(exception);
    }

    [Fact]
    public void ExecutionSearchException_Constructor()
    {
        // Arrange
        var message = "Test message";

        // Act
        var exception = new ExecutionSearchException(message);

        // Assert
        Assert.Equal(message, exception.Message);
    }

    [Fact]
    public void ExecutionSearchException_DefaultConstructorTest()
    {
        // Arrange

        // Act
        var exception = new ExecutionSearchException();

        // Assert
        Assert.NotNull(exception);
    }

    [Fact]
    public void ExecutionSortException_Constructor()
    {
        // Arrange
        var message = "Test message";

        // Act
        var exception = new ExecutionSortException(message);

        // Assert
        Assert.Equal(message, exception.Message);
    }
    // test it: public class InvalidAirlineListException(string message, Exception innerException) : Exception(message, innerException)
    [Fact]
    public void InvalidAirlineListException_Constructor()
    {
        // Arrange
        var message = "Test message";
        var innerException = new Exception("Inner exception");

        // Act
        var exception = new InvalidAirlineListException(message, innerException);

        // Assert
        Assert.Equal(message, exception.Message);
        Assert.Equal(innerException, exception.InnerException);
    }

    [Fact]
    public void InvalidCommandException_Constructor()
    {
        // Arrange
        string expectedMessage = "Test message";

        // Act
        var exception = new InvalidCommandException(expectedMessage);

        // Assert
        Assert.Equal(expectedMessage, exception.Message);
    }
    [Fact]
    public void InvalidCommandException_ConstructorWithInnerException()
    {
        // Arrange
        string expectedMessage = "Test message";
        var innerException = new Exception("Inner exception");

        // Act
        var exception = new InvalidCommandException(expectedMessage, innerException);

        // Assert
        Assert.Equal(expectedMessage, exception.Message);
        Assert.Equal(innerException, exception.InnerException);
    }
}
