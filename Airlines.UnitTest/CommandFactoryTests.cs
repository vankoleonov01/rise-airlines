﻿using Airlines.Business.Commands.Exist;
using Airlines.Business.Commands.List;
using Airlines.Business.Commands.Search;
using Airlines.Business.Commands.Sort;
using Airlines.Business.Commands;
using Airlines.Persistence.InMemory.Management;
using Airlines.Persistence.InMemory.Models;
using Airlines.Persistence.InMemory.Models.Interfaces;

namespace Airlines.UnitTest;
public class CommandFactoryTests()
{
    [Fact]
    public void CreateCommand_SearchCommand_ReturnsSearchCommand()
    {
        // Arrange
        var tokens = new[] { "search", "searchTerm" };
        var airports = new List<IAirport>();
        var flights = new List<IFlight>();
        var airlines = new List<IAirline>();
        var cargoAircrafts = new List<CargoAircraft>();
        var passengerAircrafts = new List<PassengerAircraft>();
        var route = new LinkedList<IFlight>();
        var dataManagement = new DataManagement(airports, airlines, flights, route, cargoAircrafts, passengerAircrafts);

        // Act
        var command = CommandFactory.CreateCommand(tokens, dataManagement, airports, flights, airlines, cargoAircrafts, passengerAircrafts, route);

        // Assert
        _ = Assert.IsType<SearchCommand>(command);
    }
    [Fact]
    public void CreateCommand_SortCommand_ReturnsSortCommand()
    {
        // Arrange
        var tokens = new[] { "sort", "dataType", "ascending" };
        var airports = new List<IAirport>();
        var flights = new List<IFlight>();
        var airlines = new List<IAirline>();
        var cargoAircrafts = new List<CargoAircraft>();
        var passengerAircrafts = new List<PassengerAircraft>();
        var route = new LinkedList<IFlight>();
        var dataManagement = new DataManagement(airports, airlines, flights, route, cargoAircrafts, passengerAircrafts);

        // Act
        var command = CommandFactory.CreateCommand(tokens, dataManagement, airports, flights, airlines, cargoAircrafts, passengerAircrafts, route);

        // Assert
        _ = Assert.IsType<SortCommand>(command);
    }
    [Fact]
    public void CreateCommand_ExistCommand_ReturnsExistCommand()
    {
        // Arrange
        var tokens = new[] { "exist", "airlineName" };
        var airports = new List<IAirport>();
        var flights = new List<IFlight>();
        var airlines = new List<IAirline>();
        var cargoAircrafts = new List<CargoAircraft>();
        var passengerAircrafts = new List<PassengerAircraft>();
        var route = new LinkedList<IFlight>();
        var dataManagement = new DataManagement(airports, airlines, flights, route, cargoAircrafts, passengerAircrafts);

        // Act
        var command = CommandFactory.CreateCommand(tokens, dataManagement, airports, flights, airlines, cargoAircrafts, passengerAircrafts, route);

        // Assert
        _ = Assert.IsType<ExistCommand>(command);
    }
    [Fact]
    public void CreateCommand_ListCommand_ReturnsListCommand()
    {
        // Arrange
        var tokens = new[] { "list", "inputData", "from" };
        var airports = new List<IAirport>();
        var flights = new List<IFlight>();
        var airlines = new List<IAirline>();
        var cargoAircrafts = new List<CargoAircraft>();
        var passengerAircrafts = new List<PassengerAircraft>();
        var route = new LinkedList<IFlight>();
        var dataManagement = new DataManagement(airports, airlines, flights, route, cargoAircrafts, passengerAircrafts);

        // Act
        var command = CommandFactory.CreateCommand(tokens, dataManagement, airports, flights, airlines, cargoAircrafts, passengerAircrafts, route);

        // Assert
        _ = Assert.IsType<ListCommand>(command);
    }

    [Fact]
    public void CreateSingleCommand_NullTokens_ThrowsArgumentNullException()
    {
        // Arrange
        string[] tokens = null!;
        var airports = new List<IAirport>();
        var flights = new List<IFlight>();
        var airlines = new List<IAirline>();

        // Act & Assert
        _ = Assert.Throws<ArgumentNullException>(() =>
        {
            _ = CommandFactory.CreateSingleCommand(tokens!, airports, flights, airlines);
        });
    }
    [Fact]
    public void CreateSingleCommand_NullAirports_ThrowsArgumentNullException()
    {
        // Arrange
        var tokens = Array.Empty<string>();
        List<IAirport>? airports = null;
        var flights = new List<IFlight>();
        var airlines = new List<IAirline>();

        // Act & Assert
        _ = Assert.Throws<ArgumentNullException>(() =>
        {
            _ = CommandFactory.CreateSingleCommand(tokens, airports, flights, airlines);
        });
    }

    [Fact]
    public void CreateSingleCommand_NullFlights_ThrowsArgumentNullException()
    {
        // Arrange
        var tokens = Array.Empty<string>();
        var airports = new List<IAirport>();
        List<IFlight> flights = null!;
        var airlines = new List<IAirline>();

        // Act & Assert
        _ = Assert.Throws<ArgumentNullException>(() =>
        {
            _ = CommandFactory.CreateSingleCommand(tokens, airports, flights, airlines);
        });
    }

    [Fact]
    public void CreateSingleCommand_NullAirlines_ThrowsArgumentNullException()
    {
        // Arrange
        var tokens = Array.Empty<string>();
        var airports = new List<IAirport>();
        var flights = new List<IFlight>();
        List<IAirline> airlines = null!;

        // Act & Assert
        _ = Assert.Throws<ArgumentNullException>(() =>
        {
            _ = CommandFactory.CreateSingleCommand(tokens, airports, flights, airlines);
        });
    }

    [Fact]
    public void CreateSingleCommand_ValidParameters_ReturnsNull()
    {
        // Arrange
        var tokens = Array.Empty<string>();
        var airports = new List<IAirport>();
        var flights = new List<IFlight>();
        var airlines = new List<IAirline>();

        // Act
        var result = CommandFactory.CreateSingleCommand(tokens, airports, flights, airlines);

        // Assert
        Assert.Null(result);
    }
}