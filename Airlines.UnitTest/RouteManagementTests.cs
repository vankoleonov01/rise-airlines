﻿using Airlines.Persistence.InMemory.Management;
using Airlines.Persistence.InMemory.Models;
using Airlines.Persistence.InMemory.Models.Interfaces;

namespace Airlines.UnitTest;
[Collection("Sequential")]
public class RouteManagementTests
{
    [Fact]
    public void CreateNewRoute_WhenCalled_ShouldClearRoute()
    {
        // Arrange
        var route = new LinkedList<IFlight>();
        var routeManagement = new RouteManagement(route);

        // Act
        using StringWriter sw = new StringWriter();
        Console.SetOut(sw);
        routeManagement.CreateNewRoute();

        // Assert
        Assert.Empty(route);
    }

    [Fact]
    public void AddFlight_WhenCalled_ShouldAddFlightToRoute()
    {
        // Arrange
        var route = new LinkedList<IFlight>();
        var routeManagement = new RouteManagement(route);
        var flight = new Flight("A", "B", "C", "D", 21, 60);

        // Act
        using StringWriter sw = new StringWriter();
        Console.SetOut(sw);
        routeManagement.AddFlight(flight);

        // Assert
        _ = Assert.Single(route);
        Assert.Equal(flight, route?.First?.Value);
    }

    [Fact]
    public void AddFlight_WhenCalledWithExistingFlight_ShouldNotAddFlightToRoute()
    {
        // Arrange
        var route = new LinkedList<IFlight>();
        var routeManagement = new RouteManagement(route);
        var flight = new Flight("A", "B", "C", "D", 21, 60);

        // Act
        using StringWriter sw = new StringWriter();
        Console.SetOut(sw);
        routeManagement.AddFlight(flight);
        routeManagement.AddFlight(flight);

        // Assert
        _ = Assert.Single(route);
        Assert.Equal(flight, route?.First?.Value);
    }

    [Fact]
    public void AddFlight_WhenCalledWithInvalidFlight_ShouldNotAddFlightToRoute()
    {
        // Arrange
        var route = new LinkedList<IFlight>();
        var routeManagement = new RouteManagement(route);
        var flight1 = new Flight("A", "B", "C", "D", 21, 60);
        var flight2 = new Flight("E", "F", "G", "H", 21, 60);

        // Act
        using StringWriter sw = new StringWriter();
        Console.SetOut(sw);
        routeManagement.AddFlight(flight1);
        routeManagement.AddFlight(flight2);

        // Assert
        _ = Assert.Single(route);
        Assert.Equal(flight1, route?.First?.Value);
    }

    [Fact]
    public void RemoveFlightAtEnd_WhenCalledWithEmptyRoute_ShouldNotRemoveFlightFromRoute()
    {
        // Arrange
        var route = new LinkedList<IFlight>();
        var routeManagement = new RouteManagement(route);

        // Act
        using StringWriter sw = new StringWriter();
        Console.SetOut(sw);
        routeManagement.RemoveFlightAtEnd();

        // Assert
        Assert.Empty(route);
    }

    [Fact]
    public void FlightConnection_WhenCalledWithValidFlight_ShouldReturnValidFlightConnection()
    {
        // Arrange        
        var route = new LinkedList<IFlight>();
        var routeManagement = new RouteManagement(route);
        var flight1 = new Flight("A", "B", "C", "D", 21, 60);
        var flight2 = new Flight("C", "D", "E", "F", 21, 60);

        // Act
        using StringWriter sw = new StringWriter();
        Console.SetOut(sw);
        routeManagement.AddFlight(flight1);
        routeManagement.FlightConnection(flight2);

        // Assert
        _ = Assert.Single(route);
    }
    [Fact]
    public void FlightConnection_WhenCalledWithInvalidFlight_ShouldReturnInvalidFlightConnection()
    {
        // Arrange        
        var route = new LinkedList<IFlight>();
        var routeManagement = new RouteManagement(route);
        var flight1 = new Flight("A", "B", "C", "D", 21, 60);
        var flight2 = new Flight("E", "F", "G", "H", 21, 60);

        // Act
        using StringWriter sw = new StringWriter();
        Console.SetOut(sw);
        routeManagement.AddFlight(flight1);
        routeManagement.FlightConnection(flight2);

        // Assert
        _ = Assert.Single(route);
    }

    [Fact]
    public void ProcessRouteCommand_WhenCalledWithNewCommand_ShouldCreateNewRoute()
    {
        // Arrange
        var route = new LinkedList<IFlight>();
        var routeManagement = new RouteManagement(route);
        var flights = new List<IFlight> { new Flight("A", "B", "C", "D", 21, 60) };
        var tokens = new[] { "route", "new" };

        // Act
        using StringWriter sw = new StringWriter();
        Console.SetOut(sw);
        routeManagement.ProcessRouteCommand(tokens, flights);

        // Assert
        Assert.Empty(route);
    }
    [Fact]
    public void ProcessRouteCommand_WhenCalledWithAddCommand_ShouldAddFlightToRoute()
    {
        // Arrange
        var route = new LinkedList<IFlight>();
        var routeManagement = new RouteManagement(route);
        var flight = new Flight("A", "B", "C", "D", 21, 60);
        var flights = new List<IFlight> { flight };
        var tokens = new[] { "route", "add", "A" };

        // Act
        using StringWriter sw = new StringWriter();
        Console.SetOut(sw);
        routeManagement.ProcessRouteCommand(tokens, flights);

        // Assert
        _ = Assert.Single(route);
        Assert.Equal(flight, route?.First?.Value);
    }
    [Fact]
    public void ProcessRouteCommand_WhenCalledWithAddCommandAndInvalidFlight_ShouldNotAddFlightToRoute()
    {
        // Arrange
        var route = new LinkedList<IFlight>();
        var routeManagement = new RouteManagement(route);
        var flight = new Flight("A", "B", "C", "D", 21, 60);
        var flights = new List<IFlight> { flight };
        var tokens = new[] { "route", "add", "E" };

        // Act
        using StringWriter sw = new StringWriter();
        Console.SetOut(sw);
        routeManagement.ProcessRouteCommand(tokens, flights);

        // Assert
        Assert.Empty(route);
    }
    [Fact]
    public void PrintRoute_WhenCalled_ShouldPrintRoute()
    {
        // Arrange
        var route = new LinkedList<IFlight>();
        var routeManagement = new RouteManagement(route);
        var flight = new Flight("A", "B", "C", "D", 21, 60);
        _ = route.AddLast(flight);

        // Act
        using StringWriter sw = new StringWriter();
        Console.SetOut(sw);
        routeManagement.PrintRoute();
        Assert.DoesNotContain("A -> B -> C -> D", sw.ToString());
    }
}