using Airlines.Business.Extensions;
using Xunit.Abstractions;

namespace Airlines.UnitTest;

public class SearchTests(ITestOutputHelper output)
{
    private readonly ITestOutputHelper _output = output;

    [Fact]
    public void BinarySearch_KeyExists_ShouldReturnTrue()
    {
        // Arrange
        string[] array = ["a", "b", "c", "d", "e"];
        string searchElement = "c";

        // Act
        string? result = SearchingExtensions.BinarySearch(array, searchElement);

        // Assert
        Assert.Equal(searchElement, result);
        _output.WriteLine($"Binary Search: Element '{searchElement}' found at index {Array.IndexOf(array, result)}");
    }

    [Fact]
    public void BinarySearch_KeyDoesNotExist_ShouldReturnNull()
    {
        // Arrange
        string[] array = ["a", "b", "d", "e"];
        string searchElement = "c";

        // Act
        string? result = SearchingExtensions.BinarySearch(array, searchElement);

        // Assert
        Assert.Null(result);
        _output.WriteLine($"Binary Search: Element '{searchElement}' not found in the array");
    }

    [Fact]
    public void BinarySearch_ArrayWithOneElement_KeyExists_ShouldReturnTrue()
    {
        // Arrange
        string[] array = ["a"];
        string searchElement = "a";

        // Act
        string? result = SearchingExtensions.BinarySearch(array, searchElement);

        // Assert
        Assert.Equal(searchElement, result);
        _output.WriteLine($"Binary Search: Element '{searchElement}' found at index 0");
    }

    [Fact]
    public void BinarySearch_ArrayWithOneElement_KeyDoesNotExist_ShouldReturnsNull()
    {
        // Arrange
        string[] array = ["a"];
        string searchElement = "b";

        // Act
        string? result = SearchingExtensions.BinarySearch(array, searchElement);

        // Assert
        Assert.Null(result);
        _output.WriteLine($"Binary Search: Element '{searchElement}' not found in the array");
    }

    [Fact]
    public void BinarySearch_FirstElement_ShouldReturnTrue()
    {
        // Arrange
        string[] array = ["a", "b", "c", "d", "e"];
        string searchElement = "a";

        // Act
        string? result = SearchingExtensions.BinarySearch(array, searchElement);

        // Assert
        Assert.Equal(searchElement, result);
        _output.WriteLine($"Binary Search: Element '{searchElement}' found at index {Array.IndexOf(array, result)}");
    }

    [Fact]
    public void BinarySearch_LastElement_ShouldReturnTrue()
    {
        // Arrange
        string[] array = ["a", "b", "c", "d", "e"];
        string searchElement = "e";

        // Act
        string? result = SearchingExtensions.BinarySearch(array, searchElement);

        // Assert
        Assert.Equal(searchElement, result);
        _output.WriteLine($"Binary Search: Element '{searchElement}' found at index {Array.IndexOf(array, result)}");
    }

    [Fact]
    public void LinearSearch_KeyExists_ShouldReturnTrue()
    {
        // Arrange
        string[] array = ["a", "b", "c", "d", "e"];
        string searchElement = "c";

        // Act
        string? result = SearchingExtensions.LinearSearch(array, searchElement);

        // Assert
        Assert.Equal(searchElement, result);
        _output.WriteLine($"Linear Search: Element '{searchElement}' found at index {Array.IndexOf(array, result)}");
    }

    [Fact]
    public void LinearSearch_KeyDoesNotExist_ShouldReturnNull()
    {
        // Arrange
        string[] array = ["a", "b", "d", "e"];
        string searchElement = "c";

        // Act
        string? result = SearchingExtensions.LinearSearch(array, searchElement);

        // Assert
        Assert.Null(result);
        _output.WriteLine($"Linear Search: Element '{searchElement}' not found in the array");
    }

    [Fact]
    public void LinearSearch_ArrayWithOneElement_KeyExists_ShouldReturnKey()
    {
        // Arrange
        string[] array = ["a"];
        string searchElement = "a";

        // Act
        string? result = SearchingExtensions.LinearSearch(array, searchElement);

        // Assert
        Assert.Equal(searchElement, result);
        _output.WriteLine($"Linear Search: Element '{searchElement}' found at index 0");
    }

    [Fact]
    public void LinearSearch_ArrayWithOneElement_KeyDoesNotExist_ShouldReturnNull()
    {
        // Arrange
        string[] array = ["a"];
        string searchElement = "b";

        // Act
        string? result = SearchingExtensions.LinearSearch(array, searchElement);

        // Assert
        Assert.Null(result);
        _output.WriteLine($"Linear Search: Element '{searchElement}' not found in the array");
    }
    [Fact]
    public void LinearSearch_FirstElement_ShouldReturnTrue()
    {
        // Arrange
        string[] array = ["a", "b", "c", "d", "e"];
        string searchElement = "a";

        // Act
        string? result = SearchingExtensions.LinearSearch(array, searchElement);

        // Assert
        Assert.Equal(searchElement, result);
        _output.WriteLine($"Linear Search: Element '{searchElement}' found at index {Array.IndexOf(array, result)}");
    }

    [Fact]
    public void LinearSearch_LastElement_ShouldReturnTrue()
    {
        // Arrange
        string[] array = ["a", "b", "c", "d", "e"];
        string searchElement = "e";

        // Act
        string? result = SearchingExtensions.LinearSearch(array, searchElement);

        // Assert
        Assert.Equal(searchElement, result);
        _output.WriteLine($"Linear Search: Element '{searchElement}' found at index {Array.IndexOf(array, result)}");
    }
}