using Airlines.Business.Commands.List;
using Airlines.Persistence.InMemory.Models;
using Airlines.Persistence.InMemory.Models.Interfaces;
using Xunit.Abstractions;

namespace Airlines.UnitTest;

public class ListCommandTests(ITestOutputHelper output)
{
    private readonly ITestOutputHelper _output = output;

    [Fact]
    public void ListCommand_Execute_ListsAirportsByCity()
    {
        // Arrange
        var inputData = "City 1";
        var from = "City";
        var airports = new List<IAirport>
        {
            new Airport("A1", "Airport 1", "City 1", "Country 1"),
            new Airport("A2", "Airport 2", "City 2", "Country 2"),
            new Airport("A3", "Airport 3", "City 1", "Country 3")
        };
        _ = new ListCommand(inputData, from, airports);
        _output.WriteLine("Something");


    }
    [Fact]
    public void ListCommand_Execute_ListsAirportsByCountry()
    {
        // Arrange
        var inputData = "Country 1";
        var from = "Country";
        var airports = new List<IAirport>
        {
            new Airport("A1", "Airport 1", "City 1", "Country 1"),
            new Airport("A2", "Airport 2", "City 2", "Country 2"),
            new Airport("A3", "Airport 3", "City 1", "Country 1")
        };
        _ = new ListCommand(inputData, from, airports);

        _output.WriteLine("Something New");
    }
    [Fact]
    public void ListCommand_Execute_NoAirportsAvailable()
    {
        // Arrange
        var inputData = "City 1";
        var from = "City";
        var airports = new List<IAirport>();
        var listCommand = new ListCommand(inputData, from, airports);

        // Act
        using var sw = new StringWriter();
        Console.SetOut(sw);
        listCommand.Execute(null!, null!, null!);
        var actualOutput = sw.ToString();
        sw.Close();
        // Assert
        var expectedOutput = "No airports available." + Environment.NewLine;
        _output.WriteLine(actualOutput);
    }
}