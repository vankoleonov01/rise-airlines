using Airlines.Business.Extensions;
using Xunit.Abstractions;

namespace Airlines.UnitTest;

public class SortingTests(ITestOutputHelper output)
{
    private readonly ITestOutputHelper _output = output;
    [Fact]
    public void BubbleSort_UnsortedArray_ShouldReturnTrue()
    {
        // Arrange
        string[] input = ["DEF", "ABC", "JKL", "MNO", "GHI"];
        string[] expected = ["ABC", "DEF", "GHI", "JKL", "MNO"];

        // Act
        string[] result = SortingExtensions.BubbleSort(input);

        // Assert
        Assert.Equal(expected, result);
        _output.WriteLine("BubbleSort_UnsortedArray: Test Passed");
    }

    [Fact]
    public void BubbleSort_ReverseSortedArray_ShouldReturnTrue()
    {
        // Arrange
        string[] input = ["MNO", "JKL", "GHI", "DEF", "ABC"];
        string[] expected = ["ABC", "DEF", "GHI", "JKL", "MNO"];

        // Act
        string[] result = SortingExtensions.BubbleSort(input);

        // Assert
        Assert.Equal(expected, result);
        _output.WriteLine("BubbleSort_ReverseSortedArray: Test Passed");
    }

    [Fact]
    public void BubbleSort_SortedArray_ShouldReturnTrue()
    {
        // Arrange
        string[] input = ["ABC", "DEF", "GHI", "JKL", "MNO"];

        // Act
        string[] result = SortingExtensions.BubbleSort(input);

        // Assert
        Assert.Equal(input, result);
        _output.WriteLine("BubbleSort_SortedArray: Test Passed");
    }

    [Fact]
    public void BubbleSort_EmptyArray_ShouldReturnTrue()
    {
        // Arrange
        string[] input = [];
        string[] expected = [];

        // Act
        string[] result = SortingExtensions.BubbleSort(input);

        // Assert
        Assert.Equal(expected, result);
        _output.WriteLine("BubbleSort_EmptyArray: Test Passed");
    }

    [Fact]
    public void BubbleSort_SortedArrayWithDuplicates_ShouldReturnTrue()
    {
        // Arrange
        string[] input = ["ABC", "DEF", "DEF", "GHI", "JKL", "MNO"];
        string[] expected = ["ABC", "DEF", "DEF", "GHI", "JKL", "MNO"];

        // Act
        string[] result = SortingExtensions.BubbleSort(input);

        // Assert
        Assert.Equal(expected, result);
        _output.WriteLine("BubbleSort_SortedArrayWithDuplicates: Test Passed");
    }

    [Fact]
    public void SelectionSort_UnsortedArray_ShouldReturnTrue()
    {
        // Arrange
        string[] input = ["DEF", "ABC", "JKL", "MNO", "GHI"];
        string[] expected = ["ABC", "DEF", "GHI", "JKL", "MNO"];

        // Act
        string[] result = SortingExtensions.SelectionSort(input);

        // Assert
        Assert.Equal(expected, result);
        _output.WriteLine("SelectionSort_UnsortedArray: Test Passed");
    }

    [Fact]
    public void SelectionSort_SortedArray_ShouldReturnTrue()
    {
        // Arrange
        string[] input = ["ABC", "DEF", "GHI", "JKL", "MNO"];

        // Act
        string[] result = SortingExtensions.SelectionSort(input);

        // Assert
        Assert.Equal(input, result);
        _output.WriteLine("SelectionSort_SortedArray: Test Passed");
    }

    [Fact]
    public void SelectionSort_SortedArrayWithDuplicates_ShouldReturnTrue()
    {
        // Arrange
        string[] input = ["ABC", "DEF", "DEF", "GHI", "JKL", "MNO"];
        string[] expected = ["ABC", "DEF", "DEF", "GHI", "JKL", "MNO"];

        // Act
        string[] result = SortingExtensions.SelectionSort(input);

        // Assert
        Assert.Equal(expected, result);
        _output.WriteLine("SelectionSort_SortedArrayWithDuplicates: Test Passed");
    }

    [Fact]
    public void SelectionSort_EmptyArray_ShouldReturnTrue()
    {
        // Arrange
        string[] input = [];
        string[] expected = [];

        // Act
        string[] result = SortingExtensions.SelectionSort(input);

        // Assert
        Assert.Equal(expected, result);
        _output.WriteLine("SelectionSort_EmptyArray: Test Passed");
    }

    [Fact]
    public void SelectionSort_ReverseSortedArray_ShouldReturnTrue()
    {
        // Arrange
        string[] input = ["MNO", "JKL", "GHI", "DEF", "ABC"];
        string[] expected = ["ABC", "DEF", "GHI", "JKL", "MNO"];

        // Act
        string[] result = SortingExtensions.SelectionSort(input);

        // Assert
        Assert.Equal(expected, result);
        _output.WriteLine("SelectionSort_ReverseSortedArray: Test Passed");
    }
}
