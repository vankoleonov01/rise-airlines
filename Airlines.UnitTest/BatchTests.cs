﻿using Airlines.Business.Commands.Batch;

namespace Airlines.UnitTest;
public class BatchTests
{
    [Fact]
    public void CancelBatchCommandTest()
    {
        // Arrange
        var batchModel = new BatchModel();
        var cancelBatchCommand = new CancelBatchCommand(batchModel);

        // Act
        cancelBatchCommand.Execute([], [], []);

        // Assert
        Assert.False(batchModel.IsBatchModeActive);
    }

    [Fact]
    public void CancelBatchCommandOutputTest()
    {
        // Arrange
        var batchModel = new BatchModel();
        var cancelBatchCommand = new CancelBatchCommand(batchModel);

        // Act
        cancelBatchCommand.Execute([], [], []);

        // Assert
        Assert.Equal(string.Empty, CancelBatchCommand.Output);
    }
    [Fact]
    public void CancelBatchCommandContinueProgramAfterExecutionTest()
    {
        // Arrange
        var batchModel = new BatchModel();
        var cancelBatchCommand = new CancelBatchCommand(batchModel);

        // Act
        cancelBatchCommand.Execute([], [], []);

        // Assert
        Assert.True(CancelBatchCommand.ContinueProgramAfterExecution);
    }
    [Fact]
    public void StartBatchCommandTest()
    {
        // Arrange
        var batchModel = new BatchModel();
        var startBatchCommand = new StartBatchCommand(batchModel);

        // Act
        startBatchCommand.Execute([], [], []);

        // Assert
        Assert.True(batchModel.IsBatchModeActive);
    }

    [Fact]
    public void StartBatchCommandExecuteBatchCommandsTest()
    {
        // Arrange
        var batchModel = new BatchModel();
        var startBatchCommand = new StartBatchCommand(batchModel);

        // Act
        startBatchCommand.Execute([], [], []);

        // Assert
        Assert.True(batchModel.IsBatchModeActive);
    }

    [Fact]
    public void StartBatchCommandAddToBatchTest()
    {
        // Arrange
        var batchModel = new BatchModel();
        var startBatchCommand = new StartBatchCommand(batchModel);

        // Act
        startBatchCommand.Execute([], [], []);

        // Assert
        Assert.NotNull(batchModel.Commands);
    }

    [Fact]
    public void StartBatchCommandOutputTest()
    {
        // Arrange
        var batchModel = new BatchModel();
        var startBatchCommand = new StartBatchCommand(batchModel);

        // Act
        startBatchCommand.Execute([], [], []);

        // Assert
        Assert.Equal(string.Empty, StartBatchCommand.Output);
    }

    [Fact]
    public void StartBatchCommandContinueProgramAfterExecutionTest()
    {
        // Arrange
        var batchModel = new BatchModel();
        var startBatchCommand = new StartBatchCommand(batchModel);

        // Act
        startBatchCommand.Execute([], [], []);

        // Assert
        Assert.True(StartBatchCommand.ContinueProgramAfterExecution);
    }

    [Fact]
    public void RunBatchCommandTest()
    {
        // Arrange
        var batchModel = new BatchModel();
        var runBatchCommand = new RunBatchCommand(batchModel);

        // Act
        runBatchCommand.Execute([], [], []);

        // Assert
        Assert.False(batchModel.IsBatchModeActive);
    }

    [Fact]
    public void RunBatchCommandOutputTest()
    {
        // Arrange
        var batchModel = new BatchModel();
        var runBatchCommand = new RunBatchCommand(batchModel);

        // Act
        runBatchCommand.Execute([], [], []);

        // Assert
        Assert.Equal(string.Empty, RunBatchCommand.Output);
    }

    [Fact]
    public void RunBatchCommandContinueProgramAfterExecutionTest()
    {
        // Arrange
        var batchModel = new BatchModel();
        var runBatchCommand = new RunBatchCommand(batchModel);

        // Act
        runBatchCommand.Execute([], [], []);

        // Assert
        Assert.True(RunBatchCommand.ContinueProgramAfterExecution);
    }

    [Fact]
    public void BatchModel_ConstructorTest()
    {
        // Arrange
        var batchModel = new BatchModel();

        // Act
        var isBatchModeActive = batchModel.IsBatchModeActive;
        var commands = batchModel.Commands;

        // Assert
        Assert.False(isBatchModeActive);
        Assert.NotNull(commands);
    }

    [Fact]
    public void BatchModel_AddCommandTest()
    {
        // Arrange
        var batchModel = new BatchModel();
        var command = new Action(() => { });

        // Act
        batchModel.AddCommand(command);

        // Assert
        Assert.NotNull(batchModel.Commands);
    }

    [Fact]
    public void BatchModel_CleanCommandsTest()
    {
        // Arrange
        var batchModel = new BatchModel();
        var command = new Action(() => { });
        batchModel.AddCommand(command);

        // Act
        batchModel.CleanCommands();

        // Assert
        Assert.NotNull(batchModel.Commands);
    }

    [Fact]
    public void BatchModel_DoNothingFuncTest()
    {
        // Arrange
        var batchModel = new BatchModel();

        // Act
        batchModel.CleanCommands();

        // Assert
        Assert.NotNull(batchModel.Commands);
    }

    [Fact]
    public void BatchModel_IsBatchModeActiveTest()
    {
        // Arrange
        var batchModel = new BatchModel
        {
            // Act
            IsBatchModeActive = true
        };

        // Assert
        Assert.True(batchModel.IsBatchModeActive);
    }

    [Fact]
    public void BatchModel_CommandsTest()
    {
        // Arrange
        var batchModel = new BatchModel();
        var command = new Action(() => { });

        // Act
        batchModel.Commands += command;

        // Assert
        Assert.NotNull(batchModel.Commands);
    }
}
