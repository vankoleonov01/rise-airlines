﻿using Airlines.Business.Commands.Batch;
using Airlines.Business.Commands.Sort;
using Airlines.Persistence.InMemory.Exceptions;

namespace Airlines.UnitTest;
public class SortCommandTests
{
    [Fact]
    public void Execute_WhenBatchModeIsActive_ShouldCallCommands()
    {
        // Arrange
        var batchModel = new BatchModel();
        var runBatchCommand = new RunBatchCommand(batchModel);

        // Act
        runBatchCommand.Execute([], [], []);

        // Assert
        Assert.False(batchModel.IsBatchModeActive);
    }
    [Fact]
    public void Execute_WhenBatchModeIsNotActive_ShouldNotCallCommands()
    {
        // Arrange
        var batchModel = new BatchModel();
        var runBatchCommand = new RunBatchCommand(batchModel);

        // Act
        runBatchCommand.Execute([], [], []);

        // Assert
        Assert.False(batchModel.IsBatchModeActive);
    }
    [Fact]
    public void SortCommand_Constructor_Throws_Exception_On_Null_DataType() => _ = Assert.Throws<ArgumentException>(() => new SortCommand(null, "asc"));

    [Fact]
    public void SortCommand_Constructor_Throws_Exception_On_Null_SortOrder() => _ = Assert.Throws<ArgumentException>(() => new SortCommand("airlines", null));

    [Fact]
    public void Execute_Throws_Exception_When_DataType_Is_Invalid()
    {
        // Arrange
        var command = new SortCommand("invalidType", "ascending");

        // Act & Assert
        var exception = Assert.Throws<ExecutionSortException>(() => command.Execute(null, null, null));
        Assert.Equal("Error occurred while executing sort command.", exception.Message);
    }
}
