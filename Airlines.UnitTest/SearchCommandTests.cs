﻿using Airlines.Business.Commands.Search;
using Airlines.Persistence.InMemory.Exceptions;
using Airlines.Persistence.InMemory.Management;
using Moq;

namespace Airlines.UnitTest;
public class SearchCommandTests
{

    [Fact]
    public void SearchCommand_Constructor_Throws_Exception_On_Null_DataManagement() => _ = Assert.Throws<ArgumentNullException>(() => new SearchCommand(null, "searchTerm"));

    [Fact]
    public void Constructor_Throws_ArgumentException_SearchTerm() => Assert.Throws<ArgumentException>(() => new SearchCommand(Mock.Of<DataManagement>(), null));

    [Fact]
    public void Execute_Throws_Exception_When_SearchData_Throws_Exception()
    {
        // Arrange
        var dataManagement = new DataManagement(null, null, null, null, null, null);
        var searchTerm = "searchTerm";
        var command = new SearchCommand(dataManagement, searchTerm);

        // Act & Assert
        var exception = Assert.Throws<ExecutionSearchException>(() => command.Execute(null, null, null));
        Assert.Equal("Error occurred while executing search command.", exception.Message);
    }
}