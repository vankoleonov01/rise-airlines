﻿using Airlines.Persistence.InMemory.Models;
using Airlines.Persistence.InMemory.Models.Interfaces;

namespace Airlines.UnitTest;
public class NodeAndTreeTests
{
    [Fact]
    public void NodeConstructor_WhenCalled_SetDataAndChildren()
    {
        // Arrange
        var item = "Item";

        // Act
        var node = new Node(item);

        // Assert
        Assert.Equal(item, node.Data);
        Assert.Empty(node.Children);
    }

    [Fact]
    public void Insert_WhenCalled_InsertData()
    {
        // Arrange
        var tree = new Tree();
        var data = "Data";

        // Act
        tree.Insert(data);

        // Assert
        Assert.Equal(data, tree.Root?.Data);
    }

    [Fact]
    public void InsertRecursive_AddDataToRoot_Success()
    {
        // Arrange
        var tree = new Tree();
        var data = "FL001";

        // Act
        var root = tree.InsertRecursive(null, data);

        // Assert
        Assert.NotNull(root);
        Assert.Equal(data, root.Data);
    }

    [Fact]
    public void InsertRecursive_AddDataToChildren_Success()
    {
        // Arrange
        var tree = new Tree();
        var data = "FL001";
        var childData = "FL002";
        tree.Insert(data);

        // Act
        var root = tree.InsertRecursive(tree.Root, childData);

        // Assert
        Assert.NotNull(root);
        Assert.Equal(data, root.Data);
        _ = Assert.Single(root.Children);
        Assert.Equal(childData, root.Children[0].Data);
    }

    [Fact]
    public void AlreadyInserted_FlightExists_ReturnsFalse()
    {
        // Arrange
        var tree = new Tree();
        var flightID = "FL100";
        tree.Insert(flightID);

        // Act
        var result = tree.AlreadyInserted(flightID);

        // Assert
        Assert.False(result);
    }

    [Fact]
    public void AlreadyInserted_FlightDoesNotExist_ReturnsFalse()
    {
        // Arrange
        var tree = new Tree();
        var flightID = "FL100";
        tree.Insert("FL101");

        // Act
        var result = tree.AlreadyInserted(flightID);

        // Assert
        Assert.False(result);
    }

    [Fact]
    public void DoesFlightExist_FlightExists_ReturnsTrue()
    {
        // Arrange
        var flights = new List<IFlight>
        {
            new Flight("FL100", "LAX", "JFK", "Boeing 747", 21, 60),
            new Flight("FL101", "JFK", "LAX", "Boeing 747", 21, 60)
        };
        var flightID = "FL100";

        // Act
        var result = Tree.DoesFlightExist(flights, flightID);

        // Assert
        Assert.True(result);
    }

    [Fact]
    public void DoesFlightExist_FlightDoesNotExist_ReturnsFalse()
    {
        // Arrange
        var flights = new List<IFlight>
        {
            new Flight("FL100", "LAX", "JFK", "Boeing 747", 50, 120),
            new Flight("FL101", "JFK", "LAX", "Boeing 747", 21, 60)
        };
        var flightID = "FL102";

        // Act
        var result = Tree.DoesFlightExist(flights, flightID);

        // Assert
        Assert.False(result);
    }

    [Fact]
    public void GetArrivalAirport_FlightExists_ReturnsDepartureAirport()
    {
        // Arrange
        var flights = new List<IFlight>
        {
            new Flight("FL100", "LAX", "JFK", "Boeing 747", 50, 120),
            new Flight("FL101", "JFK", "LAX", "Boeing 747", 21, 60)
        };
        var flightID = "FL100";

        // Act
        var result = Tree.GetArrivalAirport(flights, flightID);

        // Assert
        Assert.Equal("LAX", result);
    }

    [Fact]
    public void GetArrivalAirport_FlightDoesNotExist_ReturnsEmpty()
    {
        // Arrange
        var flights = new List<IFlight>
        {
            new Flight("FL100", "LAX", "JFK", "Boeing 747", 21, 60),
            new Flight("FL101", "JFK", "LAX", "Boeing 747", 50, 120)
        };
        var flightID = "FL102";

        // Act
        var result = Tree.GetArrivalAirport(flights, flightID);

        // Assert
        Assert.Empty(result);
    }

    [Fact]
    public void GetDepartureAirport_FlightExists_ReturnsDepartureAirport()
    {
        // Arrange
        var flights = new List<IFlight>
        {
            new Flight("FL100", "LAX", "JFK", "Boeing 747", 21, 60),
            new Flight("FL101", "JFK", "LAX", "Boeing 747", 21, 60)
        };
        var flightID = "FL100";

        // Act
        var result = Tree.GetDepartureAirport(flights, flightID);

        // Assert
        Assert.Equal("JFK", result);
    }

    [Fact]
    public void GetNode_FlightDoesNotExist_ReturnsNull()
    {
        // Arrange
        var tree = new Tree();
        tree.Insert("FL100");

        // Act
        var result = tree.GetNode("FL101");

        // Assert
        Assert.Null(result);
    }
}
