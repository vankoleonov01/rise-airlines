﻿using Airlines.Persistence.InMemory.Models;

namespace Airlines.UnitTest;
public class EdgeTests
{
    // test it: namespace Airlines.Persistence.InMemory.Models;
    // public class Edge(Vertex destination, double time, double price)
    // {
    //     public Vertex Destination { get; set; } = destination;
    //     public double Time { get; set; } = time;
    //     public double Price { get; set; } = price;
    // }
    [Fact]
    public void EdgeConstructorTest()
    {
        // Arrange
        var destination = new Vertex("destination");
        var time = 10.0;
        var price = 100.0;

        // Act
        var edge = new Edge(destination, time, price);

        // Assert
        Assert.Equal(destination, edge.Destination);
        Assert.Equal(time, edge.Time);
        Assert.Equal(price, edge.Price);
    }
    [Fact]
    public void EdgeDestinationTest()
    {
        // Arrange
        var destination = new Vertex("destination");
        var time = 10.0;
        var price = 100.0;
        var edge = new Edge(destination, time, price);

        // Act
        var result = edge.Destination;

        // Assert
        Assert.Equal(destination, result);
    }
    [Fact]
    public void EdgeTimeTest()
    {
        // Arrange
        var destination = new Vertex("destination");
        var time = 10.0;
        var price = 100.0;
        var edge = new Edge(destination, time, price);

        // Act
        var result = edge.Time;

        // Assert
        Assert.Equal(time, result);
    }
    [Fact]
    public void EdgePriceTest()
    {
        // Arrange
        var destination = new Vertex("destination");
        var time = 10.0;
        var price = 100.0;
        var edge = new Edge(destination, time, price);

        // Act
        var result = edge.Price;

        // Assert
        Assert.Equal(price, result);
    }
    [Fact]
    public void EdgePriceSetTest()
    {
        // Arrange
        var destination = new Vertex("destination");
        var time = 10.0;
        var price = 100.0;
        var edge = new Edge(destination, time, price)
        {
            // Act
            Price = 200.0
        };

        var result = edge.Price;

        // Assert
        Assert.Equal(200.0, result);
    }
    [Fact]
    public void EdgeTimeSetTest()
    {
        // Arrange
        var destination = new Vertex("destination");
        var time = 10.0;
        var price = 100.0;
        var edge = new Edge(destination, time, price)
        {
            // Act
            Time = 20.0
        };

        var result = edge.Time;

        // Assert
        Assert.Equal(20.0, result);
    }
    [Fact]
    public void EdgeDestinationSetTest()
    {
        // Arrange
        var destination = new Vertex("destination");
        var time = 10.0;
        var price = 100.0;
        var edge = new Edge(destination, time, price);
        var newDestination = new Vertex("newDestination");

        // Act
        edge.Destination = newDestination;
        var result = edge.Destination;

        // Assert
        Assert.Equal(newDestination, result);
    }
    [Fact]
    public void Graph_AddEdgeTest()
    {
        // Arrange
        var graph = new Graph();
        var departure = "departure";
        var destination = "destination";
        var price = 100.0;
        var time = 10.0;

        // Act
        graph.AddEdge(departure, destination, price, time);

        // Assert
        Assert.NotNull(graph.Vertices);
        Assert.NotEmpty(graph.Vertices);
        _ = Assert.Single(graph.Vertices);
        Assert.Equal(departure, graph.Vertices[0].Data);
        Assert.NotEmpty(graph.Vertices[0].Children);
        _ = Assert.Single(graph.Vertices[0].Children);
        Assert.Equal(destination, graph.Vertices[0].Children[0].Destination.Data);
        Assert.Equal(price, graph.Vertices[0].Children[0].Price);
        Assert.Equal(time, graph.Vertices[0].Children[0].Time);
    }
    [Fact]
    public void Graph_AddEdgeExistingVertexTest()
    {
        // Arrange
        var graph = new Graph();
        var departure = "departure";
        var destination = "destination";
        var price = 100.0;
        var time = 10.0;
        graph.Vertices.Add(new Vertex(departure));

        // Act
        graph.AddEdge(departure, destination, price, time);

        // Assert
        Assert.NotNull(graph.Vertices);
        Assert.NotEmpty(graph.Vertices);
        _ = Assert.Single(graph.Vertices);
        Assert.Equal(departure, graph.Vertices[0].Data);
        Assert.NotEmpty(graph.Vertices[0].Children);
        _ = Assert.Single(graph.Vertices[0].Children);
        Assert.Equal(destination, graph.Vertices[0].Children[0].Destination.Data);
        Assert.Equal(price, graph.Vertices[0].Children[0].Price);
        Assert.Equal(time, graph.Vertices[0].Children[0].Time);
    }
    [Fact]
    public void Graph_AddEdgeExistingEdgeTest()
    {
        // Arrange
        var graph = new Graph();
        var departure = "departure";
        var destination = "destination";
        var price = 100.0;
        var time = 10.0;
        graph.Vertices.Add(new Vertex(departure));
        graph.Vertices[0].Children.Add(new Edge(new Vertex(destination), 20.0, 200.0));

        // Act
        graph.AddEdge(departure, destination, price, time);

        // Assert
        Assert.NotNull(graph.Vertices);
        Assert.NotEmpty(graph.Vertices);
        _ = Assert.Single(graph.Vertices);
        Assert.Equal(departure, graph.Vertices[0].Data);
        Assert.NotEmpty(graph.Vertices[0].Children);
        _ = Assert.Single(graph.Vertices[0].Children);
        Assert.Equal(destination, graph.Vertices[0].Children[0].Destination.Data);
        Assert.Equal(price, graph.Vertices[0].Children[0].Price);
        Assert.Equal(time, graph.Vertices[0].Children[0].Time);
    }
    [Fact]
    public void Graph_AddEdgeNewVertexTest()
    {
        // Arrange
        var graph = new Graph();
        var departure = "departure";
        var destination = "destination";
        var price = 100.0;
        var time = 10.0;

        // Act
        graph.AddEdge(departure, destination, price, time);

        // Assert
        Assert.NotNull(graph.Vertices);
        Assert.NotEmpty(graph.Vertices);
        _ = Assert.Single(graph.Vertices);
        Assert.Equal(departure, graph.Vertices[0].Data);
        Assert.NotEmpty(graph.Vertices[0].Children);
        _ = Assert.Single(graph.Vertices[0].Children);
        Assert.Equal(destination, graph.Vertices[0].Children[0].Destination.Data);
        Assert.Equal(price, graph.Vertices[0].Children[0].Price);
        Assert.Equal(time, graph.Vertices[0].Children[0].Time);
    }
}