﻿namespace Airlines.API.Utils;

public static class Constants
{
    public const int DefaultPage = 1;
    public const int PageSize = 10;
}