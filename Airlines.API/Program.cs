using Airlines.Business.Mapping;
using Airlines.Business.Services.Interfaces;
using Airlines.Business.Services;
using Airlines.Persistence.InMemory.Entities;
using Airlines.Persistence.InMemory.EntitiesRepository;
using Airlines.Persistence.InMemory.EntitiesRepository.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Airlines.API;

public static class Program
{
    public static void Main(string[] args)
    {
        var builder = WebApplication.CreateBuilder(args);
        ConfigureServices(builder.Services);
        _ = builder.Services.AddCors(options =>
        {
            options.AddPolicy(name: "_myAllowSpecificOrigins",
                policy =>
                {
                    _ = policy.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod();
                });
        });

        _ = builder.Services.AddControllers();
        _ = builder.Services.AddEndpointsApiExplorer();
        _ = builder.Services.AddSwaggerGen();

        var app = builder.Build();
        if (app.Environment.IsDevelopment())
        {
            _ = app.UseSwagger();
            _ = app.UseSwaggerUI();
        }

        _ = app.UseHttpsRedirection();

        _ = app.UseCors("_myAllowSpecificOrigins");

        _ = app.UseAuthorization();

        _ = app.MapControllers();

        app.Run();
    }

    private static void ConfigureServices(IServiceCollection services)
    {
        _ = services.AddControllersWithViews();

        _ = services.AddScoped<IAirportRepository, AirportRepository>();
        _ = services.AddScoped<IAirportService, AirportService>();
        _ = services.AddAutoMapper(typeof(AirportMapper));

        _ = services.AddDbContext<RiseAirlinesDBContext>(options =>
            options.UseSqlServer(Persistence.InMemory.Configuration.ConfigurationManager.GetConnectionString("DefaultConnection")));

        _ = services.AddScoped<IAirlineRepository, AirlineRepository>();
        _ = services.AddScoped<IAirlineService, AirlineService>();
        _ = services.AddAutoMapper(typeof(AirlineMapper));

        _ = services.AddScoped<IFlightRepository, FlightRepository>();
        _ = services.AddScoped<IFlightService, FlightService>();
        _ = services.AddAutoMapper(typeof(FlightMapper));
    }
}