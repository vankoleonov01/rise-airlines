﻿using Airlines.Business.Dto;
using Airlines.Business.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using static Airlines.API.Utils.Constants;
namespace Airlines.API.Controllers;

[ApiController]
[Route("api/[controller]")]
public class FlightController(IFlightService flightService, IAirportService airportService) : ControllerBase
{

    [HttpGet]
    public async Task<IActionResult> GetAll()
    {
        var flights = await flightService.GetFlights(DefaultPage, PageSize);

        if (flights == null || flights.Count == 0)
        {
            return StatusCode(404, new { NumberOfCode = 404, Message = "No flights founded" });
        }

        foreach (var flight in flights)
        {
            var fromAirport = await airportService.GetAirportByIdAsync(flight.FromAirportId);
            var toAirport = await airportService.GetAirportByIdAsync(flight.ToAirportId);

            if (fromAirport != null)
            {
                flight.FromAirport = fromAirport;
            }

            if (toAirport != null)
            {
                flight.ToAirport = toAirport;
            }
        }

        return Ok(flights);
    }

    [HttpGet("{flightId}")]
    public async Task<IActionResult> GetOne(int flightId)
    {
        var flight = await flightService.GetFlightByIdAsync(flightId);
        if (flight == null)
        {
            return StatusCode(404, new { NumberOfCode = 404, Message = "Flight not found" });
        }
        return Ok(flight);
    }


    [HttpPost]
    public async Task<IActionResult> Create(FlightDto flightDto)
    {
        if (!ModelState.IsValid)
        {
            return BadRequest(ModelState);
        }
        var isSuccess = await flightService.AddFlight(flightDto);

        if (!isSuccess)
        {
            return StatusCode(500, new { message = "An error occurred while creating the flight" });
        }

        return StatusCode(201, new { message = "The flight was successfully created" });
    }

    [HttpPut("{flightId}")]
    public async Task<IActionResult> Edit(int flightId, FlightDto flightDto)
    {
        if (!ModelState.IsValid)
        {
            return BadRequest(ModelState);
        }

        var existingFlight = await flightService.GetFlightByIdAsync(flightId);
        if (existingFlight == null)
        {
            return StatusCode(404, new { NumberOfCode = 404, Message = "Flight not found" });
        }

        await flightService.UpdateFlight(flightDto);
        return StatusCode(204, new { NumberOfCode = 204, Message = "The flight was successfully updated" });
    }

    [HttpDelete("{flightId}")]
    public async Task<IActionResult> Delete(int flightId)
    {
        var existingFlight = await flightService.GetFlightByIdAsync(flightId);
        if (existingFlight == null)
        {
            return StatusCode(404, new { NumberOfCode = 404, Message = "Flight not found" });
        }

        await flightService.DeleteFlight(flightId);
        return StatusCode(204, new { NumberOfCode = 204, Message = "The flight was successfully deleted" });
    }

    [HttpGet("counts")]
    public async Task<IActionResult> GetTotalFlightsCount()
    {
        var totalFlightsCount = await flightService.GetTotalFlightsCount();
        return Ok(totalFlightsCount);
    }
}