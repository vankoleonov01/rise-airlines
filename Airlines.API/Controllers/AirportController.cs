﻿using Airlines.Business.Dto;
using Airlines.Business.Services;
using Airlines.Business.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using static Airlines.API.Utils.Constants;
namespace Airlines.API.Controllers;

[ApiController]
[Route("api/[controller]")]
public class AirportController(IAirportService airportService) : ControllerBase
{
    [HttpGet]
    public async Task<IActionResult> GetAll()
    {
        var airports = await airportService.GetAirports(DefaultPage, PageSize);
        if (airports == null || airports.Count == 0)
        {
            return StatusCode(404, new { NumberOfCode = 404, Message = "No airports founded" });
        }
        return Ok(airports);
    }

    [HttpGet("{airportId}")]
    public async Task<IActionResult> GetOne(int airportId)
    {
        var airport = await airportService.GetAirportByIdAsync(airportId);
        if (airport == null)
        {
            return StatusCode(404, new { NumberOfCode = 404, Message = "Airport not found" });
        }
        return Ok(airport);
    }

    [HttpPost]
    public async Task<IActionResult> Create(AirportDto airportDto)
    {

        if (!ModelState.IsValid)
        {
            return BadRequest(ModelState);
        }

        var isSuccess = await airportService.AddAirport(airportDto);

        if (!isSuccess)
        {
            return StatusCode(500, new { message = "An error occurred while creating the airport" });
        }

        return StatusCode(201, new { message = "The airport was successfully created" });
    }

    [HttpPut("{airportId}")]
    public async Task<IActionResult> Edit(int airportId, AirportDto airportDto)
    {
        if (!ModelState.IsValid)
        {
            return BadRequest(ModelState);
        }

        var existingAirport = await airportService.GetAirportByIdAsync(airportId);
        if (existingAirport == null)
        {
            return StatusCode(404, new { NumberOfCode = 404, Message = "Airport not found" });
        }

        await airportService.UpdateAirport(airportDto);

        return StatusCode(204, new { NumberOfCode = 204, Message = "The airport was successfully updated" });
    }

    [HttpDelete("{airportId}")]
    public async Task<IActionResult> Delete(int airportId)
    {
        var existingAirport = await airportService.GetAirportByIdAsync(airportId);
        if (existingAirport == null)
        {
            return StatusCode(404, new { NumberOfCode = 404, Message = "Airport not found" });
        }

        await airportService.DeleteAirport(airportId);
        return StatusCode(204, new { NumberOfCode = 204, Message = "The airport was successfully deleted" });
    }

    [HttpGet("counts")]
    public async Task<IActionResult> GetTotalAirportsCount()
    {
        var totalAirportsCount = await airportService.GetTotalAirportsCount();
        return Ok(totalAirportsCount);
    }
}