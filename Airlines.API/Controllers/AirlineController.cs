﻿using Airlines.Business.Dto;
using Airlines.Business.Services;
using Airlines.Business.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using static Airlines.API.Utils.Constants;

namespace Airlines.API.Controllers;

[ApiController]
[Route("api/[controller]")]
public class AirlineController(IAirlineService airlineService) : ControllerBase
{
    [HttpGet]
    public async Task<IActionResult> GetAll()
    {
        var airlines = await airlineService.GetAirlines(DefaultPage, PageSize);
        if (airlines == null || airlines.Count == 0)
        {
            return StatusCode(404, new { NumberOfCode = 404, Message = "Airlines not found" });
        }
        return Ok(airlines);
    }

    [HttpGet("{airlineId}")]
    public async Task<IActionResult> GetOne(int airlineId)
    {
        var airline = await airlineService.GetAirlineByIdAsync(airlineId);

        if (airline == null)
        {
            return StatusCode(404, new { NumberOfCode = 404, Message = "Airline not found" });
        }

        return Ok(airline);
    }

    [HttpPost]
    public async Task<IActionResult> Create(AirlineDto airlineDto)
    {
        if (!ModelState.IsValid)
        {
            return BadRequest(ModelState);
        }

        var isSuccess = await airlineService.AddAirline(airlineDto);

        if (!isSuccess)
        {
            return StatusCode(500, new { message = "An error occurred while creating the airline" });
        }

        return StatusCode(201, new { message = "The airline was successfully created" });
    }

    [HttpPut("{airlineId}")]
    public async Task<IActionResult> Edit(int airlineId, AirlineDto airlineDto)
    {
        if (!ModelState.IsValid)
        {
            return BadRequest(ModelState);
        }

        var existingAirline = await airlineService.GetAirlineByIdAsync(airlineId);
        if (existingAirline == null)
        {
            return StatusCode(404, new { NumberOfCode = 404, Message = "Airline not found" });
        }

        await airlineService.UpdateAirline(airlineDto);
        return StatusCode(204, new { NumberOfCode = 204, Message = "The airline was successfully updated" });
    }


    [HttpDelete("{airlineId}")]
    public async Task<IActionResult> Delete(int airlineId)
    {
        var existingAirline = await airlineService.GetAirlineByIdAsync(airlineId);
        if (existingAirline == null)
        {
            return StatusCode(404, new { NumberOfCode = 404, Message = "Airline not found" });
        }

        await airlineService.DeleteAirline(airlineId);
        return StatusCode(204, new { NumberOfCode = 204, Message = "The airline was successfully deleted" });
    }

    [HttpGet("counts")]
    public async Task<IActionResult> GetTotalAirlinesCount()
    {
        var totalAirlinesCount = await airlineService.GetTotalAirlinesCount();
        return Ok(totalAirlinesCount);
    }
}