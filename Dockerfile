FROM mcr.microsoft.com/dotnet/aspnet:8.0 AS base
USER app
WORKDIR /app
EXPOSE 8080

FROM mcr.microsoft.com/dotnet/sdk:8.0 AS build
ARG BUILD_CONFIGURATION=Release
WORKDIR /src
COPY ["Airlines.Web/Airlines.Web.csproj", "Airlines.Web/"]
COPY ["Airlines.Business/Airlines.Business.csproj", "Airlines.Business/"]
COPY ["Airlines.Persistence.InMemory/Airlines.Persistence.InMemory.csproj", "Airlines.Persistence.InMemory/"]
RUN dotnet restore "./Airlines.Web/Airlines.Web.csproj"
COPY . .
WORKDIR "/src/Airlines.Web"
RUN dotnet build "./Airlines.Web.csproj" -c $BUILD_CONFIGURATION -o /app/build

FROM build AS publish
ARG BUILD_CONFIGURATION=Release
RUN dotnet publish "./Airlines.Web.csproj" -c $BUILD_CONFIGURATION -o /app/publish /p:UseAppHost=false

FROM mcr.microsoft.com/mssql/server AS mssql
ENV SA_PASSWORD=adragnev
ENV ACCEPT_EULA=Y

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "Airlines.Web.dll"]
