DROP TABLE IF EXISTS dbo.Airports;
CREATE TABLE dbo.Airports
(
    AirportId INT IDENTITY(1, 1) PRIMARY KEY,
    Name NVARCHAR(255) NOT NULL,
    Country NVARCHAR(100) NOT NULL,
    City NVARCHAR(255) NOT NULL,
    Code CHAR(3) UNIQUE NOT NULL,
    RunwaysCount INT,
    Founded DATE,
    CreatedDate DATETIME2 DEFAULT GETDATE(),
    ModifiedDate DATETIME2 DEFAULT GETDATE()
);

DROP TABLE IF EXISTS dbo.Airlines;
CREATE TABLE dbo.Airlines
(
    AirlineID INT IDENTITY(1, 1) PRIMARY KEY,
    Name NVARCHAR(255) NOT NULL,
    Founded DATE NOT NULL,
    FleetSize INT NOT NULL,
    Description NVARCHAR(MAX),
    CreatedDate DATETIME2 DEFAULT GETDATE(),
    ModifiedDate DATETIME2 DEFAULT GETDATE()
);

DROP TABLE IF EXISTS dbo.Flights;
CREATE TABLE dbo.Flights
(
    FlightID INT IDENTITY(1, 1) PRIMARY KEY,
    FlightNumber NVARCHAR(255) NOT NULL,
    FromAirportID INT NOT NULL,
    ToAirportID INT NOT NULL,
    DepartureDateTime DATETIME2 NOT NULL,
    ArrivalDateTime DATETIME2 NOT NULL,
    RealDepartureDateTime DATETIME2,
    RealArrivalDateTime DATETIME2,
    Cancelled BIT DEFAULT 0,
    CancelationReason NVARCHAR(MAX),
    CreatedDate DATETIME2 DEFAULT GETDATE(),
    ModifiedDate DATETIME2 DEFAULT GETDATE(),
);

ALTER TABLE dbo.Flights
ADD FOREIGN KEY (FromAirportID) REFERENCES dbo.Airports (AirportID);

ALTER TABLE dbo.Flights
ADD FOREIGN KEY (ToAirportID) REFERENCES dbo.Airports (AirportID);

ALTER TABLE dbo.Flights
ADD CONSTRAINT CHK_Flights_DepartureDateTime CHECK (DepartureDateTime > GETDATE());

ALTER TABLE dbo.Flights
ADD CONSTRAINT CHK_Flights_ArrivalDateTime CHECK (ArrivalDateTime > DepartureDateTime
                                                  AND ArrivalDateTime > GETDATE()
                                                 );

ALTER TABLE dbo.Flights
ADD AirlineID INT;