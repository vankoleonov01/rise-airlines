INSERT INTO dbo.Airports
(
    Name,
    Country,
    City,
    Code,
    RunwaysCount,
    Founded
)
VALUES
('JFK', 'United States', 'New York', 'USA', 2, '1937-12-16'),
('PEK', 'China', 'Beijing', 'PEK', 4, '1961-01-15');

INSERT INTO dbo.Flights
(
    FlightNumber,
    FromAirportID,
    ToAirportID,
    DepartureDateTime,
    ArrivalDateTime
)
VALUES
('FL123', 1, 2, '2024-04-24 15:30:00', '2024-04-24 17:15:00'),
('FL456', 2, 1, '2024-04-27 11:11:00', '2024-04-27 12:35:00');

SELECT FlightID,
       FlightNumber,
       FromAirportID,
       ToAirportID,
       DepartureDateTime,
       ArrivalDateTime
FROM dbo.Flights
WHERE FlightNumber = 'FL123';

UPDATE dbo.Flights
SET ToAirportID = 4
WHERE FlightID = 2;

DELETE FROM dbo.Flights
WHERE FlightID = 1;

-- Query to show all the flights scheduled for tomorrow
SELECT f.FlightID,
       f.FlightNumber,
       fa.Name AS FromAirport,
       ta.Name AS ToAirport,
       fa.City AS FromCity,
       ta.City AS ToCity,
       fa.Country AS FromCountry,
       ta.Country AS ToCountry,
       a.Name AS Airline,
       a.Founded AS AirlineFounded,
       a.FleetSize AS AirlineFleetSize,
       a.Description AS AirlineDescription,
       f.DepartureDateTime,
       f.ArrivalDateTime
FROM dbo.Flights AS f
    JOIN dbo.Airports AS fa
        ON f.FromAirportID = fa.AirportID
    JOIN dbo.Airports AS ta
        ON f.ToAirportID = ta.AirportID
    JOIN dbo.Airlines AS a
        ON f.AirlineID = a.AirlineID
WHERE CONVERT(DATE, f.DepartureDateTime) = DATEADD(DAY, 1, CONVERT(DATE, GETDATE()));

-- Query to show the count of all departed flights
SELECT COUNT(*) AS DepartedFlightsCount
FROM dbo.Flights
WHERE DepartureDateTime < GETDATE();

-- Query to show the most farther flight
SELECT f.FlightID,
       f.FlightNumber,
       fa.Name AS FromAirport,
       ta.Name AS ToAirport,
       fa.City AS FromCity,
       ta.City AS ToCity,
       fa.Country AS FromCountry,
       ta.Country AS ToCountry,
       f.DepartureDateTime,
       f.ArrivalDateTime,
       DATEDIFF(MINUTE, f.DepartureDateTime, f.ArrivalDateTime) AS DurationMinutes
FROM dbo.Flights AS f
    JOIN dbo.Airports AS fa
        ON f.FromAirportID = fa.AirportID
    JOIN dbo.Airports AS ta
        ON f.ToAirportID = ta.AirportID
WHERE DATEDIFF(MINUTE, f.DepartureDateTime, f.ArrivalDateTime) =
(
    SELECT MAX(DATEDIFF(MINUTE, DepartureDateTime, ArrivalDateTime))
    FROM dbo.Flights
);