using Microsoft.Extensions.Configuration;

namespace Airlines.Persistence.InMemory.Configuration;
public abstract class ConfigurationManager
{
    private static readonly IConfigurationRoot _configuration;

    static ConfigurationManager()
    {
        var basePath = Path.Combine(AppContext.BaseDirectory, "..", "..", "..", "..", "Airlines.Persistence.InMemory");

        // Build configuration once
        _configuration = new ConfigurationBuilder()
            .SetBasePath(basePath)
            .AddJsonFile("appsettings.json")
            .Build();
    }

    public static string? GetConnectionString(string name) => _configuration.GetConnectionString(name);
}