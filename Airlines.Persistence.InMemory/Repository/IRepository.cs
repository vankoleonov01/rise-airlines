﻿namespace Airlines.Persistence.InMemory.Repository;
public interface IRepository<T> where T : class
{
    T? GetById(string id);
    IEnumerable<T> GetAll();
    bool Add(T entity);
    bool Update(T entity);
    bool Delete(string id);
    bool Delete(T entity);
    bool Exists(string id);
    bool Exists(T entity);
    void DeleteAll();
    int Size { get; }
}