﻿using Airlines.Persistence.InMemory.Models;

namespace Airlines.Persistence.InMemory.Repository;
public class AirlineRepository : IRepository<Airline>
{
    private readonly Dictionary<string, Airline> _airlines = [];

    public bool Add(Airline entity)
    {
        if (Exists(entity))
            return false;

        _airlines[entity.Name.ToUpper()] = entity;
        return true;
    }

    public bool Delete(string id) => _airlines.Remove(id.ToUpper());

    public bool Delete(Airline entity) => Delete(entity.Name);

    public void DeleteAll() => _airlines.Clear();

    public bool Exists(string id) => _airlines.ContainsKey(id.ToUpper());

    public bool Exists(Airline entity) => Exists(entity.Name);

    public Airline? GetById(string id) => _airlines.GetValueOrDefault(id.ToUpper());

    public IEnumerable<Airline> GetAll() => _airlines.Values;

    public bool Update(Airline entity)
    {
        if (!Exists(entity))
            return false;

        _airlines[entity.Name.ToUpper()] = entity;
        return true;
    }

    public int Size => _airlines.Count;
}