﻿using Airlines.Persistence.InMemory.Models;

namespace Airlines.Persistence.InMemory.Repository;

public class FlightRepository(AircraftRepository aircraftRepository) : IRepository<Flight>
{
    private readonly Dictionary<string, Flight> _flights = [];

    public int Size => _flights.Count;

    public bool Add(Flight entity)
    {
        if (Exists(entity) || !aircraftRepository.Exists(entity.Aircraft))
            return false;

        Set(entity);
        return true;
    }

    public bool Delete(string id) => _flights.Remove(id.ToUpper());

    public bool Delete(Flight entity) => Delete(entity.Identifier);

    public void DeleteAll() => _flights.Clear();

    public bool Exists(string id) => _flights.ContainsKey(id.ToUpper());

    public bool Exists(Flight entity) => Exists(entity.Identifier);

    public Flight? GetById(string id) => _flights.GetValueOrDefault(id.ToUpper());

    public IEnumerable<Flight> GetAll() => _flights.Values;

    public bool Update(Flight entity)
    {
        if (!Exists(entity))
            return false;

        Set(entity);
        return true;
    }

    public void Set(Flight entity) => _flights[entity.Identifier.ToUpper()] = entity;
}