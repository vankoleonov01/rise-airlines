﻿using Airlines.Persistence.InMemory.Models;
namespace Airlines.Persistence.InMemory.Repository;

public class AircraftRepository : IRepository<Aircraft>
{
    private readonly Dictionary<string, Aircraft> _aircrafts = [];

    public int Size => _aircrafts.Count;

    public bool Add(Aircraft entity)
    {
        if (Exists(entity))
            return false;

        Set(entity);
        return true;
    }

    public bool Delete(string id) => _aircrafts.Remove(id.ToUpper());

    public bool Delete(Aircraft entity) => Delete(entity.Model);

    public void DeleteAll() => _aircrafts.Clear();

    public bool Exists(string id) => _aircrafts.ContainsKey(Aircraft.NormalizeModelName(id));

    public bool Exists(Aircraft entity) => Exists(entity.Model);

    public Aircraft? GetById(string id) => _aircrafts.GetValueOrDefault(id.ToUpper());

    public Aircraft GetByFlight(Flight flight) => GetById(flight.Aircraft)!;

    public IEnumerable<Aircraft> GetAll() => _aircrafts.Values;

    public bool Update(Aircraft entity)
    {
        if (!Exists(entity))
            return false;

        Set(entity);
        return true;
    }
    public void Set(Aircraft entity) => _aircrafts[entity.Model] = entity;
}