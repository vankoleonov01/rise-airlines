﻿using Airlines.Persistence.InMemory.Models;

namespace Airlines.Persistence.InMemory.Repository;

public class AirportRepository : IRepository<Airport>
{
    private readonly Dictionary<string, List<Airport>> _cityAirports = [];
    private readonly Dictionary<string, List<Airport>> _countryAirports = [];

    public int Size => GetAll().Count();
    public int CitySize => _cityAirports.Count;
    public int CountrySize => _countryAirports.Count;

    public bool Add(Airport entity)
    {
        if (Exists(entity))
            return false;

        Set(entity);
        return true;
    }

    public bool Delete(string id)
    {
        var key = id.ToUpper();
        var airport = GetById(key);
        if (airport != null)
        {
            _ = _cityAirports[airport.City.ToUpper()].Remove(airport);
            _ = _countryAirports[airport.Country.ToUpper()].Remove(airport);
            return true;
        }
        return false;
    }

    public bool Delete(Airport entity) => Delete(entity.Identifier);

    public void DeleteAll()
    {
        _cityAirports.Clear();
        _countryAirports.Clear();
    }

    public bool Exists(string id) => _cityAirports.Values.Any(list => list.Any(x => string.Equals(x.Identifier, id, StringComparison.OrdinalIgnoreCase)));

    public bool Exists(Airport entity) => Exists(entity.Identifier);

    public Airport? GetById(string id) => _cityAirports.Values.SelectMany(list => list.Where(x => string.Equals(x.Identifier, id, StringComparison.OrdinalIgnoreCase))).FirstOrDefault();

    public IEnumerable<Airport> GetAll() => _cityAirports.Values.SelectMany(list => list);

    public IEnumerable<Airport> GetAllByCountry(string country) => _countryAirports.GetValueOrDefault(country.ToUpper()) ?? Enumerable.Empty<Airport>();

    public IEnumerable<Airport> GetAllByCity(string city) => _cityAirports.GetValueOrDefault(city.ToUpper()) ?? Enumerable.Empty<Airport>();

    public bool Update(Airport entity)
    {
        if (!Exists(entity))
            return false;

        Set(entity);
        return true;
    }

    public void Set(Airport entity)
    {
        var cityKey = entity.City.ToUpper();
        if (!_cityAirports.ContainsKey(cityKey))
            _cityAirports[cityKey] = [];
        _cityAirports[cityKey].Add(entity);

        var countryKey = entity.Country.ToUpper();
        if (!_countryAirports.ContainsKey(countryKey))
            _countryAirports[countryKey] = [];
        _countryAirports[countryKey].Add(entity);
    }
}