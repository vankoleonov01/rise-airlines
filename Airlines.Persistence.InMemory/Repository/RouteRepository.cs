﻿using Airlines.Persistence.InMemory.Models;
namespace Airlines.Persistence.InMemory.Repository;

public class RouteRepository : IRepository<Route>
{
    private readonly Dictionary<string, Route> _routes = [];

    public int Size => _routes.Count;

    public bool Add(Route entity)
    {
        if (Exists(entity))
            return false;

        _routes[entity.Identifier!.ToUpper()] = entity;
        return true;
    }

    public bool Delete(string id) => _routes.Remove(id.ToUpper());

    public bool Delete(Route entity) => Delete(entity.Identifier!);

    public void DeleteAll() => _routes.Clear();

    public bool Exists(string id) => _routes.ContainsKey(id.ToUpper());

    public bool Exists(Route entity) => Exists(entity.Identifier!);

    public Route? GetById(string id) => _routes.GetValueOrDefault(id.ToUpper());

    public IEnumerable<Route> GetAll() => _routes.Values;

    public bool Update(Route entity)
    {
        if (!Exists(entity))
            return false;

        _routes[entity.Identifier!.ToUpper()] = entity;
        return true;
    }
}