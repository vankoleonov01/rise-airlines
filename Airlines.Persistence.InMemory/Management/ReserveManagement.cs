using Airlines.Persistence.InMemory.Models;
using Airlines.Persistence.InMemory.Models.Interfaces;

namespace Airlines.Persistence.InMemory.Management;

public static class ReserveManagement
{
    public static bool IsAircraftCargoPossible(List<IFlight>? flights, List<CargoAircraft>? cargo, double cargoWeight, double cargoVolume, string flightID, string type)
    {
        foreach (var flight in flights!)
        {
            if (flight.Identifier.Equals(flightID.ToUpper()))
            {
                Console.WriteLine("Flight Exist!\nSearching aircrafts... ");
                if (type == "cargo")
                {
                    foreach (var aircraft in cargo!)
                    {
                        if (flight.Aircraft.Contains(aircraft.Model) && aircraft.CargoWeight > cargoWeight && aircraft.CargoVolume > cargoVolume)
                        {
                            Console.WriteLine($"Aircraft Exist and is {type} ");
                            return true;
                        }
                    }
                }
            }
        }
        Console.WriteLine($"Aircraft is not {type}");
        return false;
    }

    public static bool BaggageCalculation(double smallBaggageCount, double largeBaggageCount)
    {
        var smallBaggageMaxWeight = 15;
        var largeBaggageMaxWeight = 30;

        return smallBaggageCount < smallBaggageMaxWeight && largeBaggageMaxWeight > largeBaggageCount;
    }

    public static bool IsAircraftPassengerPossible(List<IFlight>? flights, List<PassengerAircraft>? passenger, double smallBaggageCount, double largeBaggageCount, int seats, string flightID, string type)
    {
        foreach (var flight in flights!)
        {
            if (flight.Identifier.Equals(flightID.ToUpper()))
            {
                Console.WriteLine("Flight Exist!\nSearching aircrafts... ");
                if (type == "ticket")
                {
                    foreach (var aircraft in passenger!)
                    {
                        if (flight.Aircraft.Contains(aircraft.Model) && BaggageCalculation(smallBaggageCount, largeBaggageCount) && seats > 0)
                        {
                            Console.WriteLine($"Aircraft Exist and is {type} ");
                            return true;
                        }
                    }
                }
            }
        }
        Console.WriteLine($"Aircraft is not {type}");
        return false;
    }

    public static bool IsCargoReservationPossible(List<IFlight>? flights, List<CargoAircraft>? cargo, string flightId, double cargoWeight, double cargoVolume, string type)
    {
        var result = IsAircraftCargoPossible(flights, cargo, cargoWeight, cargoVolume, flightId, type);

        return result;
    }

    public static bool IsPassengerTicketPossible(List<IFlight>? flights, List<PassengerAircraft>? passenger, string flightId, int seats, double smallBaggageCount, double largeBaggageCount, string type)
    {
        var result = IsAircraftPassengerPossible(flights, passenger, smallBaggageCount, largeBaggageCount, seats, flightId, type);
        return result;
    }
}