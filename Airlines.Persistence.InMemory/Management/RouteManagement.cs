using Airlines.Persistence.InMemory.Models.Interfaces;

namespace Airlines.Persistence.InMemory.Management;

public class RouteManagement
{
    private readonly LinkedList<IFlight>? _route;
    public LinkedList<IFlight>? Route { get; }

    public RouteManagement(LinkedList<IFlight>? route)
    {
        _route = route;
        Route = _route;
    }

    public void ProcessRouteCommand(string[] tokens, IEnumerable<IFlight>? flights)
    {
        if (tokens.Length <= 1)
        {
            Console.WriteLine("Invalid route command.");
            return;
        }

        var action = tokens[1].ToLower();

        switch (action)
        {
            case "new":
                CreateNewRoute();
                break;
            case "add":
                if (tokens.Length > 2)
                {
                    var flightIdentifier = tokens[2];
                    var flight = flights!.FirstOrDefault(f => f.Identifier.Equals(flightIdentifier, StringComparison.OrdinalIgnoreCase));
                    if (flight != null)
                    {
                        AddFlight(flight);
                    }
                    else
                    {
                        Console.WriteLine($"Flight with identifier '{flightIdentifier}' not found.");
                    }
                }
                else
                {
                    Console.WriteLine("Flight identifier is missing.");
                }
                break;
            case "remove":
                RemoveFlightAtEnd();
                break;
            case "print":
                PrintRoute();
                break;
            default:
                Console.WriteLine("Invalid route command.");
                break;
        }
    }

    public void CreateNewRoute()
    {
        _route!.Clear();
        Console.WriteLine("New route created.");
    }

    public void PrintRoute()
    {
        Console.WriteLine("Route:");
        foreach (var flight in _route!)
        {
            Console.WriteLine(flight);
        }
    }

    public void AddFlight(IFlight flight)
    {
        if (_route!.Last != null && _route.Last.Value.ArrivalAirport != flight.DepartureAirport)
        {
            Console.WriteLine("Error: New flight does not connect to the route.");
            return;
        }

        _ = _route.AddLast(flight);
        Console.WriteLine($"Flight {flight.Identifier} added to the route.");
    }

    public void RemoveFlightAtEnd()
    {
        if (_route!.Count == 0)
        {
            Console.WriteLine("Error: Route is empty.");
            return;
        }

        var removedFlight = _route.Last?.Value;
        _route.RemoveLast();
        Console.WriteLine($"Flight {removedFlight?.Identifier} removed from the route.");
    }

    public void FlightConnection(IFlight flight)
    {
        if (_route!.Last != null && _route.Last.Value.ArrivalAirport != flight.DepartureAirport)
            Console.WriteLine("Error: New flight does not logically connect to the route.");
        else
            Console.WriteLine("Flight connection is valid.");
    }
}