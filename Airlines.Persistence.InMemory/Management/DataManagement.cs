using Airlines.Persistence.InMemory.Exceptions;
using Airlines.Persistence.InMemory.Models;
using Airlines.Persistence.InMemory.Models.Interfaces;

namespace Airlines.Persistence.InMemory.Management;

public class DataManagement(
    List<IAirport>? airports,
    List<IAirline>? airlines,
    List<IFlight>? flights,
    LinkedList<IFlight>? route,
    List<CargoAircraft>? cargoAircrafts,
    List<PassengerAircraft>? passengerAircrafts)
{
    private readonly List<IAirport>? _airports = airports;
    private readonly List<IAirline>? _airlines = airlines;
    private readonly RouteManagement _routeManager = new(route);

    public List<IAirport>? Airports { get; } = airports;
    public List<IAirline>? Airlines { get; } = airlines;
    public List<IFlight>? Flights { get; } = flights;
    public LinkedList<IFlight>? Routes { get; } = route;
    public List<CargoAircraft>? CargoAircrafts { get; } = cargoAircrafts;
    public List<PassengerAircraft>? PassengerAircrafts { get; } = passengerAircrafts;

    public bool AirlineExists(string airlineName) => _airlines!.Any(airline => airline.Name.Split(' ').Any(name => name.Trim().Equals(airlineName, StringComparison.OrdinalIgnoreCase)));

    public IEnumerable<IAirport> ListAirportsInCityOrCountry(string inputData, string from)
    {
        return from.Equals("City", StringComparison.OrdinalIgnoreCase)
            ? _airports!.Where(airport => airport.City.Equals(inputData, StringComparison.OrdinalIgnoreCase))
            : from.Equals("Country", StringComparison.OrdinalIgnoreCase)
                ? _airports!.Where(airport => airport.Country.Equals(inputData, StringComparison.OrdinalIgnoreCase))
                : [];
    }

    public void ProcessCommand(string command)
    {
        var tokens = command.Split(' ');
        var action = tokens[0].ToLower();
        try
        {
            ValidateInput(tokens, 2);
            switch (action)
            {
                case "exist":
                    if (tokens.Length > 1)
                    {
                        var airlineName = string.Join(" ", tokens[1..]);
                        Console.WriteLine(AirlineExists(airlineName));
                    }
                    else
                        Console.WriteLine("Airline name is missing.");
                    break;

                case "list":
                    if (tokens.Length > 2)
                    {
                        var inputData = tokens[1];
                        var from = tokens[2];
                        var airports = ListAirportsInCityOrCountry(inputData, from);
                        foreach (var airport in airports)
                        {
                            Console.WriteLine(airport);
                        }
                    }
                    else
                        Console.WriteLine("Input data or 'from' parameter is missing.");
                    break;

                case "search":
                    if (tokens.Length > 1)
                    {
                        var searchTerm = string.Join(" ", tokens[1..]);
                        SearchData(searchTerm);
                    }
                    else
                        Console.WriteLine("Search term is missing.");
                    break;

                case "sort":
                    if (tokens.Length > 1)
                    {
                        var dataType = tokens[1].ToLower();
                        var sortOrder = tokens.Length > 2 ? tokens[2].ToLower() : "ascending";
                        SortData(dataType, sortOrder);
                    }
                    else
                        Console.WriteLine("Data type is missing.");
                    break;

                case "route":
                    ProcessRouteCommand(tokens);
                    break;

                case "reserve":
                    if (tokens.Length > 1)
                    {
                        switch (tokens[1])
                        {
                            case "cargo":
                                {
                                    var result = ReserveManagement.IsCargoReservationPossible(Flights, CargoAircrafts, tokens[2], double.Parse(tokens[3]), double.Parse(tokens[4]), "cargo");
                                    Console.WriteLine(result);
                                    break;
                                }
                            case "ticket":
                                {
                                    var result = ReserveManagement.IsPassengerTicketPossible(Flights, PassengerAircrafts, tokens[2], int.Parse(tokens[3]), double.Parse(tokens[4]), double.Parse(tokens[5]), "passenger");
                                    Console.WriteLine(result);
                                    break;
                                }
                            default:
                                Console.WriteLine("Invalid Command!");
                                break;
                        }
                    }
                    else
                    {
                        Console.WriteLine("Reservation type is missing.");
                    }
                    break;

                default:
                    throw new InvalidCommandException("Invalid command.");
            }
        }
        catch (InvalidInputException ex)
        {
            Console.WriteLine(ex.Message);
        }
        catch (InvalidCommandException ex)
        {
            Console.WriteLine(ex.Message);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"An unexpected error occurred: {ex.Message}");
        }
    }

    private static void PrintSortedData<T>(IEnumerable<T> data)
    {
        Console.WriteLine($"Sorted {typeof(T).Name}:");
        foreach (var item in data)
        {
            Console.WriteLine(item);
        }
    }

    public void SearchData(string searchTerm)
    {
        var foundAirport = SearchAirport(searchTerm);
        var foundAirline = SearchAirline(searchTerm);
        var foundFlight = SearchFlight(searchTerm);

        if (foundFlight != null)
        {
            Console.WriteLine($"Flight Found: {foundFlight}");
        }
        else if (foundAirport != null)
        {
            Console.WriteLine($"Airport Found: {foundAirport}");
        }
        else if (foundAirline != null)
        {
            Console.WriteLine($"Airline Found: {foundAirline}");
        }
        else
        {
            Console.WriteLine("Not found");
        }
    }
    public IAirport? SearchAirport(string searchTerm)
    {
        return Airports!.FirstOrDefault(a =>
            a.Name.Equals(searchTerm, StringComparison.OrdinalIgnoreCase) ||
            a.City.Equals(searchTerm, StringComparison.OrdinalIgnoreCase) ||
            a.Country.Equals(searchTerm, StringComparison.OrdinalIgnoreCase));
    }

    public IAirline? SearchAirline(string searchTerm)
    {
        return (Airlines ?? throw new InvalidOperationException()).FirstOrDefault(airline =>
            airline.Name.Equals(searchTerm, StringComparison.OrdinalIgnoreCase));
    }

    public IFlight? SearchFlight(string searchTerm) => Flights!.FirstOrDefault(f => f.ToString()!.Contains(searchTerm, StringComparison.OrdinalIgnoreCase));

    public void SortData(string dataType, string sortOrder)
    {
        switch (dataType)
        {
            case "airports":
                var sortedAirports = sortOrder.Equals("ascending", StringComparison.OrdinalIgnoreCase)
                    ? (_airports ?? throw new InvalidOperationException()).OrderBy(a => a.Identifier)
                    : (_airports ?? throw new InvalidOperationException()).OrderByDescending(a => a.Identifier);
                PrintSortedData(sortedAirports);
                break;

            case "airlines":
                var sortedAirlines = sortOrder.Equals("ascending", StringComparison.OrdinalIgnoreCase)
                    ? (_airlines ?? throw new InvalidOperationException()).OrderBy(a => a.Name)
                    : (_airlines ?? throw new InvalidOperationException()).OrderByDescending(a => a.Name);
                PrintSortedData(sortedAirlines);
                break;

            case "flights":
                var sortedFlights = sortOrder.Equals("ascending", StringComparison.OrdinalIgnoreCase)
                    ? (Flights ?? throw new InvalidOperationException()).OrderBy(f => f.ToString())
                    : (Flights ?? throw new InvalidOperationException()).OrderByDescending(f => f.ToString());
                PrintSortedData(sortedFlights);
                break;
        }
    }
    public void ProcessRouteCommand(string[] tokens) => _routeManager.ProcessRouteCommand(tokens, Flights);
    private static void ValidateInput(IReadOnlyCollection<string> tokens, int expectedLength)
    {
        if (tokens.Count < expectedLength)
        {
            throw new InvalidInputException("Invalid command length.");
        }
    }
}