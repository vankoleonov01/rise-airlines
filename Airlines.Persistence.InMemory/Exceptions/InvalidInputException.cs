﻿namespace Airlines.Persistence.InMemory.Exceptions;
public class InvalidInputException(string message) : Exception(message);

