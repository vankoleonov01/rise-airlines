﻿namespace Airlines.Persistence.InMemory.Exceptions;
public class ExecutionSortException : Exception
{
    public ExecutionSortException(string message) : base(message) { }
    public ExecutionSortException(string message, Exception innerException) : base(message, innerException) { }
}
