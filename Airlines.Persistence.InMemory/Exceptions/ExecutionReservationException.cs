﻿namespace Airlines.Persistence.InMemory.Exceptions;

public class ExecutionReservationException : Exception
{
    public ExecutionReservationException() { }

    public ExecutionReservationException(string message) : base(message) { }

    public ExecutionReservationException(string message, Exception innerException) : base(message, innerException) { }
}
