﻿namespace Airlines.Persistence.InMemory.Exceptions;
public class ExecutionSearchException : Exception
{
    public ExecutionSearchException() { }

    public ExecutionSearchException(string message) : base(message) { }

    public ExecutionSearchException(string message, Exception innerException) : base(message, innerException) { }
}

