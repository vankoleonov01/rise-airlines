﻿namespace Airlines.Persistence.InMemory.Exceptions;
public class InvalidAirlineNameException(string message) : Exception(message);
