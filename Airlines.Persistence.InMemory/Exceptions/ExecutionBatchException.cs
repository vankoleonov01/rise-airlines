﻿namespace Airlines.Persistence.InMemory.Exceptions;
public class ExecutionBatchException : Exception
{
    public ExecutionBatchException() { }

    public ExecutionBatchException(string message) : base(message) { }

    public ExecutionBatchException(string message, Exception innerException) : base(message, innerException) { }
}

