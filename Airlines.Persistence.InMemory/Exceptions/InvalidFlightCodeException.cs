﻿namespace Airlines.Persistence.InMemory.Exceptions;
public class InvalidFlightCodeException(string message) : Exception(message);

