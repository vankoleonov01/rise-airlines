﻿namespace Airlines.Persistence.InMemory.Exceptions;

public class ExecutionRouteException : Exception
{
    public ExecutionRouteException() { }

    public ExecutionRouteException(string message) : base(message) { }

    public ExecutionRouteException(string message, Exception innerException) : base(message, innerException) { }
}
