﻿namespace Airlines.Persistence.InMemory.Exceptions;
public class InvalidAirlineListException(string message, Exception innerException) : Exception(message, innerException);