using Airlines.Persistence.InMemory.Entities;
using Airlines.Persistence.InMemory.EntitiesRepository.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Airlines.Persistence.InMemory.EntitiesRepository;

public class AirportRepository : IAirportRepository, IDisposable
{
    public AirportRepository() { }

    public async Task<List<Airport>> GetAllAirportsAsync(int page, int pageSize)
    {
        try
        {
            await using var context = new RiseAirlinesDBContext();
            return await context.Airports.Skip((page - 1) * pageSize).Take(pageSize).ToListAsync();
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error getting airports from the database: {ex.Message}");
            return [];
        }
    }

    public async Task<List<Airport>> GetAirportsByNameAsync(string name)
    {
        try
        {
            await using var context = new RiseAirlinesDBContext();
            return await context.Airports.Where(a => a.Name == name).ToListAsync();
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error getting airports by name from the database: {ex.Message}");
            return [];
        }
    }

    public async Task<Airport?> GetAirportByIdAsync(int id)
    {
        try
        {
            await using var context = new RiseAirlinesDBContext();
            return await context.Airports.FirstOrDefaultAsync(a => a.AirportId == id);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error getting airport from the database: {ex.Message}");
            return null;
        }
    }

    public async Task<bool> AddAirportAsync(Airport airport)
    {
        try
        {
            await using var context = new RiseAirlinesDBContext();
            _ = await context.Airports.AddAsync(airport);
            var success = await context.SaveChangesAsync() > 0;
            if (success)
            {
                Console.WriteLine("Airport inserted successfully.");
            }
            return success;
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error inserting airport into the database: {ex.Message}");
            return false;
        }
    }

    public async Task<bool> UpdateAirportAsync(Airport airport)
    {
        try
        {
            await using var context = new RiseAirlinesDBContext();
            _ = context.Airports.Update(airport);
            var success = await context.SaveChangesAsync() > 0;

            if (success)
            {
                Console.WriteLine("Airport updated successfully.");
            }

            return success;
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error updating airport in the database: {ex.Message}");
            return false;
        }
    }

    public async Task<bool> DeleteAirportAsync(int id)
    {
        try
        {
            await using var context = new RiseAirlinesDBContext();
            var airport = await context.Airports.FirstOrDefaultAsync(a => a.AirportId == id);
            if (airport is null) return false;
            _ = context.Airports.Remove(airport);
            var success = await context.SaveChangesAsync() > 0;

            if (success)
            {
                Console.WriteLine("Airport deleted successfully.");
            }

            return success;
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error deleting airport from the database: {ex.Message}");
            return false;
        }
    }

    public async Task<int> GetTotalAirportsCount()
    {
        try
        {
            await using var context = new RiseAirlinesDBContext();
            return await context.Airports.CountAsync();
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error getting total airports count from the database: {ex.Message}");
            return 0;
        }
    }

    public async Task<List<Airport>> SearchAndFilterAsync(string filterCriteria, string filterValue, DateOnly? founded)
    {
        try
        {
            await using var context = new RiseAirlinesDBContext();
            var query = context.Airports.AsQueryable();

            if (!string.IsNullOrEmpty(filterCriteria) && !string.IsNullOrEmpty(filterValue))
            {
                switch (filterCriteria)
                {
                    case "Name":
                        query = query.Where(a => a.Name.Contains(filterValue));
                        break;
                    case "Country":
                        query = query.Where(a => a.Country.Contains(filterValue));
                        break;
                    case "City":
                        query = query.Where(a => a.City.Contains(filterValue));
                        break;
                    case "Code":
                        query = query.Where(a => a.Code.Contains(filterValue));
                        break;
                    case "RunwaysCount":
                        query = query.Where(a => a.RunwaysCount.ToString()!.Contains(filterValue));
                        break;
                    default:
                        break;
                }
            }

            if (founded != null)
            {
                query = query.Where(a => a.Founded == founded);
            }

            return await query.ToListAsync();
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error searching and filtering airports from the database: {ex.Message}");
            return [];
        }
    }
    public void Dispose() => GC.SuppressFinalize(this);
}