using Airlines.Persistence.InMemory.Entities;
using System.Linq.Expressions;

namespace Airlines.Persistence.InMemory.EntitiesRepository.Interfaces;
public interface IAirlineRepository
{
    public Task<List<Airline>> GetAllAirlinesAsync(int page, int pageSize);
    public Task<Airline?> GetAirlineByIdAsync(int id);
    public Task<bool> AddAirlineAsync(Airline airline);
    public Task<bool> UpdateAirlineAsync(Airline airline);
    public Task<bool> DeleteAirlineAsync(int id);
    public Task<int> GetTotalAirlinesCount();
    public Task<List<Airline>> SearchAndFilterAsync(string filterCriteria, string filterValue, DateOnly? founded);
}