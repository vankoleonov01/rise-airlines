using Airlines.Persistence.InMemory.Entities;

namespace Airlines.Persistence.InMemory.EntitiesRepository.Interfaces;
public interface IFlightRepository
{
    public Task<List<Flight>> GetAllFlightsAsync(int page, int pageSize);
    public Task<Flight?> GetFlightByIdAsync(int id);
    public Task<bool> AddFlightAsync(Flight flight);
    public Task<bool> UpdateFlightAsync(Flight flight);
    public Task<bool> UpdateFlightDepartureDateTimeAsync(int id, DateTime departureDateTime);
    public Task<bool> UpdateFlightArrivalDateTimeAsync(int id, DateTime arrivalDateTime);
    public Task<bool> DeleteFlightAsync(int id);
    public Task<int> GetTotalFlightsCount();
    public Task<List<Flight>> SearchAndFilterAsync(string filterCriteria, string filterValue, DateTime? departureDate, DateTime? arrivalDate);
}