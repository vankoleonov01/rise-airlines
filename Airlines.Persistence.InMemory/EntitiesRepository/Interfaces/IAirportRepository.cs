using Airlines.Persistence.InMemory.Entities;
using System.Linq.Expressions;

namespace Airlines.Persistence.InMemory.EntitiesRepository.Interfaces;
public interface IAirportRepository
{
    public Task<List<Airport>> GetAllAirportsAsync(int page, int pageSize);
    public Task<List<Airport>> GetAirportsByNameAsync(string name);
    public Task<Airport?> GetAirportByIdAsync(int id);
    public Task<bool> AddAirportAsync(Airport airport);
    public Task<bool> UpdateAirportAsync(Airport airport);
    public Task<bool> DeleteAirportAsync(int id);
    public Task<int> GetTotalAirportsCount();
    public Task<List<Airport>> SearchAndFilterAsync(string filterCriteria, string filterValue, DateOnly? founded);
}