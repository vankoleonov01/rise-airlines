using Airlines.Persistence.InMemory.Entities;
using Airlines.Persistence.InMemory.EntitiesRepository.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Airlines.Persistence.InMemory.EntitiesRepository;
public class FlightRepository : IFlightRepository, IDisposable
{
    public async Task<List<Flight>> GetAllFlightsAsync(int page, int pageSize)
    {
        try
        {
            await using var context = new RiseAirlinesDBContext();
            return await context.Flights
                .Include(f => f.FromAirport)
                .Include(f => f.ToAirport)
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error getting paginated flights from the database: {ex.Message}");
            return [];
        }
    }

    public async Task<Flight?> GetFlightByIdAsync(int id)
    {
        try
        {
            await using var context = new RiseAirlinesDBContext();
            return await context.Flights.FirstOrDefaultAsync(f => f.FlightId == id);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error getting flight from the database: {ex.Message}");
            return null;
        }
    }

    public async Task<bool> AddFlightAsync(Flight flight)
    {
        try
        {
            await using var context = new RiseAirlinesDBContext();
            _ = await context.Flights.AddAsync(flight);
            var success = await context.SaveChangesAsync() > 0;
            if (success)
            {
                Console.WriteLine("Flight inserted successfully.");
            }
            return success;
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error inserting flight into the database: {ex.Message}");
            return false;
        }
    }

    public async Task<bool> UpdateFlightAsync(Flight flight)
    {
        try
        {
            await using var context = new RiseAirlinesDBContext();
            _ = context.Flights.Update(flight);
            var success = await context.SaveChangesAsync() > 0;
            if (success)
            {
                Console.WriteLine("Flight updated successfully.");
            }
            return success;
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error updating flight in the database: {ex.Message}");
            return false;
        }
    }

    public async Task<bool> UpdateFlightDepartureDateTimeAsync(int id, DateTime departureDateTime)
    {
        try
        {
            await using var context = new RiseAirlinesDBContext();
            var flight = await context.Flights.FirstOrDefaultAsync(f => f.FlightId == id);
            if (flight is null) return false;
            flight.DepartureDateTime = departureDateTime;
            var success = await context.SaveChangesAsync() > 0;
            if (success)
            {
                Console.WriteLine("Flight departure time updated successfully.");
            }
            return success;
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error updating flight departure time in the database: {ex.Message}");
            return false;
        }
    }

    public async Task<bool> UpdateFlightArrivalDateTimeAsync(int id, DateTime arrivalDateTime)
    {
        try
        {
            await using var context = new RiseAirlinesDBContext();
            var flight = await context.Flights.FirstOrDefaultAsync(f => f.FlightId == id);
            if (flight is null) return false;
            flight.ArrivalDateTime = arrivalDateTime;
            var success = await context.SaveChangesAsync() > 0;
            if (success)
            {
                Console.WriteLine("Flight arrival time updated successfully.");
            }
            return success;
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error updating flight arrival time in the database: {ex.Message}");
            return false;
        }
    }

    public async Task<bool> DeleteFlightAsync(int id)
    {
        try
        {
            await using var context = new RiseAirlinesDBContext();
            var flight = await context.Flights.FirstOrDefaultAsync(f => f.FlightId == id);
            if (flight is null) return false;
            _ = context.Flights.Remove(flight);
            var success = await context.SaveChangesAsync() > 0;
            if (success)
            {
                Console.WriteLine("Flight deleted successfully.");
            }
            return success;
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error deleting flight from the database: {ex.Message}");
            return false;
        }
    }

    public async Task<int> GetTotalFlightsCount()
    {
        try
        {
            await using var context = new RiseAirlinesDBContext();
            return await context.Flights.CountAsync();
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error getting total flights count from the database: {ex.Message}");
            return 0;
        }
    }

    public async Task<List<Flight>> SearchAndFilterAsync(string filterCriteria, string filterValue, DateTime? departureDate, DateTime? arrivalDate)
    {
        try
        {
            await using var context = new RiseAirlinesDBContext();
            var query = context.Flights.Include(f => f.FromAirport).Include(f => f.ToAirport).AsQueryable();

            if (!string.IsNullOrEmpty(filterCriteria) && !string.IsNullOrEmpty(filterValue))
            {
                switch (filterCriteria)
                {
                    case "FlightNumber":
                        query = query.Where(f => f.FlightNumber.Contains(filterValue));
                        break;
                    case "FromAirport":
                        query = query.Where(f => f.FromAirport != null && f.FromAirport.Name.Contains(filterValue));
                        break;
                    case "ToAirport":
                        query = query.Where(f => f.ToAirport != null && f.ToAirport.Name.Contains(filterValue));
                        break;
                    default:
                        break;
                }
            }

            if (departureDate != null)
            {
                query = query.Where(f => f.DepartureDateTime.Date == departureDate.Value.Date);
            }

            if (arrivalDate != null)
            {
                query = query.Where(f => f.ArrivalDateTime.Date == arrivalDate.Value.Date);
            }

            return await query.ToListAsync();
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error searching and filtering flights from the database: {ex.Message}");
            return [];
        }
    }
    public void Dispose() => GC.SuppressFinalize(this);
}