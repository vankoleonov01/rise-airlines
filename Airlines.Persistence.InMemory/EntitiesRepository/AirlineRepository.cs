using Airlines.Persistence.InMemory.Entities;
using Airlines.Persistence.InMemory.EntitiesRepository.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Airlines.Persistence.InMemory.EntitiesRepository;

public class AirlineRepository : IAirlineRepository, IDisposable
{
    public AirlineRepository() { }

    public async Task<List<Airline>> GetAllAirlinesAsync(int page, int pageSize)
    {
        try
        {
            await using var context = new RiseAirlinesDBContext();
            return await context.Airlines.Skip((page - 1) * pageSize).Take(pageSize).ToListAsync();
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error getting airlines from the database: {ex.Message}");
            return [];
        }
    }


    public async Task<Airline?> GetAirlineByIdAsync(int id)
    {
        try
        {
            await using var context = new RiseAirlinesDBContext();
            return await context.Airlines.FirstOrDefaultAsync(a => a.AirlineId == id);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error getting airline from the database: {ex.Message}");
            return null;
        }
    }

    public async Task<bool> AddAirlineAsync(Airline airline)
    {
        try
        {
            await using var context = new RiseAirlinesDBContext();
            _ = await context.Airlines.AddAsync(airline);
            var success = await context.SaveChangesAsync() > 0;

            if (success)
            {
                Console.WriteLine("Airline inserted successfully.");
            }

            return success;
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error inserting airline into the database: {ex.Message}");
            return false;
        }
    }

    public async Task<bool> UpdateAirlineAsync(Airline airline)
    {
        try
        {
            await using var context = new RiseAirlinesDBContext();
            _ = context.Airlines.Update(airline);
            var success = await context.SaveChangesAsync() > 0;

            if (success)
            {
                Console.WriteLine("Airline updated successfully.");
            }

            return success;
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error updating airline in the database: {ex.Message}");
            return false;
        }
    }

    public async Task<bool> DeleteAirlineAsync(int id)
    {
        try
        {
            await using var context = new RiseAirlinesDBContext();
            var airline = await context.Airlines.FirstOrDefaultAsync(a => a.AirlineId == id);
            if (airline is null) return false;
            _ = context.Airlines.Remove(airline);
            var success = await context.SaveChangesAsync() > 0;

            if (success)
            {
                Console.WriteLine("Airline deleted successfully.");
            }

            return success;
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error deleting airline from the database: {ex.Message}");
            return false;
        }
    }
    public async Task<int> GetTotalAirlinesCount()
    {
        try
        {
            await using var context = new RiseAirlinesDBContext();
            return await context.Airlines.CountAsync();
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error getting total airlines count from the database: {ex.Message}");
            return 0;
        }
    }

    public async Task<List<Airline>> SearchAndFilterAsync(string filterCriteria, string filterValue, DateOnly? founded)
    {
        try
        {
            await using var context = new RiseAirlinesDBContext();
            var query = context.Airlines.AsQueryable();

            if (!string.IsNullOrEmpty(filterCriteria) && !string.IsNullOrEmpty(filterValue))
            {
                switch (filterCriteria)
                {
                    case "Name":
                        query = query.Where(a => a.Name.Contains(filterValue));
                        break;
                    case "FleetSize":
                        query = query.Where(a => a.FleetSize.ToString().Contains(filterValue));
                        break;
                    case "Description":
                        query = query.Where(a => a.Description.Contains(filterValue));
                        break;
                    default:
                        break;
                }
            }

            if (founded != null)
            {
                query = query.Where(a => a.Founded == founded);
            }

            return await query.ToListAsync();
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error searching and filtering airlines from the database: {ex.Message}");
            return [];
        }
    }

    public void Dispose() => GC.SuppressFinalize(this);
}