﻿using Airlines.Persistence.InMemory.Exceptions;
using Airlines.Persistence.InMemory.Models;
using Airlines.Persistence.InMemory.Models.Interfaces;
using System.Globalization;

namespace Airlines.Persistence.InMemory;

public static class FileReader
{
    public static List<IAirport>? ReadAirportsFromFile(string path)
    {
        List<IAirport>? airports = [];

        using var reader = new StreamReader(path);
        var isHeader = true;
        while (!reader.EndOfStream)
        {
            var line = reader.ReadLine();
            if (!isHeader && line != null)
            {
                var values = line.Split(',').Select(value => value.Trim()).ToArray();
                if (values.Length == 4)
                {
                    var identifier = values[0].Trim();
                    var name = values[1].Trim();
                    var city = values[2].Trim();
                    var country = values[3].Trim();
                    airports.Add(new Airport(identifier, name, city, country));
                }
                else
                {
                    Console.WriteLine("Error: Line doesn't have enough values.");
                }
            }
            isHeader = false;
        }

        return airports;
    }

    public static List<IAirline>? ReadAirlinesFromFile(string path)
    {
        List<IAirline>? airlines = [];

        using var reader = new StreamReader(path);
        var isHeader = true;
        while (!reader.EndOfStream)
        {
            var line = reader.ReadLine();
            if (!isHeader && line != null)
            {
                var values = line.Split(',').Select(value => value.Trim()).ToArray();
                var name = values[0].Trim();
                airlines.Add(new Airline(name));
            }
            isHeader = false;
        }

        return airlines;
    }

    public static List<IFlight> ReadFlightsFromFile(string path)
    {
        List<IFlight>? flights = [];

        using var reader = new StreamReader(path);
        var isHeader = true;
        while (!reader.EndOfStream)
        {
            var line = reader.ReadLine();
            if (!isHeader && line != null)
            {
                var values = line.Split(',').Select(value => value.Trim()).ToArray();
                if (values.Length == 6)
                {
                    var identifier = values[0].Trim();
                    var departureAirport = values[1].Trim();
                    var arrivalAirport = values[2].Trim();
                    var aircraft = values[3].Trim();
                    var price = values[4].Equals("-") ? 0 : double.Parse(values[4]);
                    var duration = values[5].Equals("-") ? 0 : double.Parse(values[5]);
                    flights.Add(new Flight(identifier, departureAirport, arrivalAirport, aircraft, price, duration));
                }
                else
                {
                    Console.WriteLine("Error: Line doesn't have enough values.");
                }
            }
            isHeader = false;
        }

        return flights;
    }

    public static Dictionary<string, List<object>> ReadAircraftsFromFile(string path)
    {
        List<CargoAircraft> cargoAircrafts = [];
        List<PassengerAircraft> passengerAircrafts = [];
        List<PrivateAircraft> privateAircrafts = [];
        Dictionary<string, List<object>> listOfAircrafts = [];

        using (StreamReader reader = new StreamReader(path))
        {
            bool isHeader = true;
            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                if (!isHeader && line != null)
                {
                    var values = line.Split(',').Select(value => value.Trim()).ToArray();
                    var model = values[0];
                    var weight = values[1].Equals("-") ? 0 : double.Parse(values[1]);
                    var volume = values[2].Equals("-") ? 0 : double.Parse(values[2], NumberStyles.Any, CultureInfo.InvariantCulture);
                    var seats = values[3].Equals("-") ? 0 : int.Parse(values[3]);

                    if (weight > 0 && volume > 0 && seats == 0)
                    {
                        CargoAircraft newCargoAircraft = new CargoAircraft(model, weight, volume, "cargo");
                        cargoAircrafts.Add(newCargoAircraft);
                    }
                    else if (seats > 0 && volume > 0 && weight > 0)
                    {
                        PassengerAircraft newPassengerAircraft = new PassengerAircraft(model, weight, volume, seats, "passenger");
                        passengerAircrafts.Add(newPassengerAircraft);
                    }
                    else if (weight == 0 && volume == 0 && seats > 0)
                    {
                        PrivateAircraft newPrivateAircraft = new PrivateAircraft(model, seats, "private");
                        privateAircrafts.Add(newPrivateAircraft);
                    }
                }
                isHeader = false;
            }
        }
        listOfAircrafts.Add("cargo", cargoAircrafts.Cast<object>().ToList());
        listOfAircrafts.Add("passenger", passengerAircrafts.Cast<object>().ToList());
        listOfAircrafts.Add("private", privateAircrafts.Cast<object>().ToList());
        return listOfAircrafts;
    }
    public static Tree ReadFlightsRouteFile(string path)
    {
        var newTree = new Tree();
        var flights = ReadFlightsFromFile(path);

        try
        {
            if (!File.Exists(path))
            {
                throw new InvalidInputException("Missing File!");
            }
            using var reader = new StreamReader(path);
            var isHeader = true;
            var start = string.Empty;
            var lastAdded = string.Empty;
            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                if (!isHeader && line != null)
                {
                    var values = line.Split(',').Select(value => value.Trim()).ToArray();
                    if (Airport.AirportValidation(values[0]))
                    {
                        start = values[0];
                        newTree.Insert(values[0]);
                    }
                    else if (Flight.FlightValidation(values[0]) && Tree.DoesFlightExist(flights, values[0]))
                    {
                        var airportArrival = Tree.GetArrivalAirport(flights, values[0]);
                        var airportDeparture = Tree.GetDepartureAirport(flights, values[0]);
                        if (airportArrival == start)
                        {
                            lastAdded = airportDeparture;
                            newTree.Insert(airportDeparture);
                        }
                        else if (lastAdded == airportArrival && newTree.Root != null)
                        {
                            foreach (var flight in newTree.Root.Children)
                            {
                                if (flight.Data == lastAdded)
                                {
                                    flight.Children.Add(new Node(airportDeparture));
                                }
                            }
                        }
                    }
                }
                isHeader = false;
            }
        }
        catch (InvalidInputException ex)
        {
            Console.WriteLine($"{ex.Message}");
        }

        return newTree;
    }

    public static Graph ReadFlightsGraphFile(string path)
    {
        var graph = new Graph();
        try
        {
            if (!File.Exists(path))
            {
                throw new InvalidInputException($"{path} not exist");
            }
            using StreamReader reader = new StreamReader(path);
            bool isHeader = true;
            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                if (!isHeader && line != null)
                {
                    var values = line.Split(',').Select(value => value.Trim()).ToArray();
                    if (values.Length == 6 && Airport.AirportValidation(values[1]) && Airport.AirportValidation(values[2]))
                    {
                        graph.AddEdge(values[1], values[2], int.Parse(values[4]), double.Parse(values[5], NumberStyles.Any, CultureInfo.InvariantCulture));
                    }
                    else
                    {
                        throw new InvalidInputException("Error: Line doesn't have enough values.");
                    }
                }
                isHeader = false;
            }
        }
        catch (InvalidInputException ex)
        {
            Console.WriteLine($"Missing File! {ex.Message}");
        }
        return graph;
    }
}