﻿namespace Airlines.Persistence.InMemory.Models;
public class Node(string item)
{
    public readonly string Data = item;
    public readonly List<Node> Children = [];
}