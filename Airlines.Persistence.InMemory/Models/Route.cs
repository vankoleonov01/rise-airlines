﻿using Airlines.Persistence.InMemory.Models.Interfaces;

namespace Airlines.Persistence.InMemory.Models;
public class Route(List<IFlight>? flights)
{
    public string? Identifier { get; set; }

    public List<IFlight>? _flights = flights;
}
