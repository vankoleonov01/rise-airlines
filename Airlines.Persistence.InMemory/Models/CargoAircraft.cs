﻿namespace Airlines.Persistence.InMemory.Models;
public class CargoAircraft(string name, double weight, double volume, string model) : Aircraft(name, model)
{
    public double? CargoWeight { get; set; } = weight;
    public double? CargoVolume { get; set; } = volume;

    public override string ToString() => $"Aircraft {Model} with cargo weight {CargoWeight} kg and volume {CargoVolume} m³";
}

