﻿namespace Airlines.Persistence.InMemory.Models;

public class Graph
{
    public readonly List<Vertex> Vertices = [];

    public void AddEdge(string departure, string destination, double price, double time)
    {
        var newVertexDeparture = new Vertex(departure);
        var newVertexDestination = new Vertex(destination);

        var existingVertex = Vertices.FirstOrDefault(v => v.Data == departure);
        if (existingVertex != null)
        {
            var existingEdge = existingVertex.Children.FirstOrDefault(e => e.Destination.Data == destination);
            if (existingEdge != null)
            {
                existingEdge.Time = time;
                existingEdge.Price = price;
            }
            else
            {
                existingVertex.Children.Add(new Edge(newVertexDestination, time, price));
            }
        }
        else
        {
            newVertexDeparture.Children.Add(new Edge(newVertexDestination, time, price));
            Vertices.Add(newVertexDeparture);
        }
    }
}