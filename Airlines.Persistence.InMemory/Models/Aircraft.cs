﻿using Airlines.Persistence.InMemory.Models.Interfaces;

namespace Airlines.Persistence.InMemory.Models;
public class Aircraft(string model, string type) : IAircraft
{
    public string Model { get; set; } = model;
    public string Type { get; set; } = type;
    public static string NormalizeModelName(string modelName)
        => modelName.ToUpper().Replace(" ", "");

    public override string ToString() => $"{Model}";

}
