using Airlines.Persistence.InMemory.Models.Interfaces;

namespace Airlines.Persistence.InMemory.Models;

public class Airport(string identifier, string name, string city, string country) : IAirport
{
    public string Identifier { get; set; } = identifier;
    public string Name { get; set; } = name;
    public string City { get; set; } = city;
    public string Country { get; set; } = country;
    internal static bool AirportValidation(string check)
       => check.All(char.IsLetter) && check.Length is >= 2 and <= 4;
    public override string ToString() => $"{Identifier}, {Name}, {City}, {Country}";
}
