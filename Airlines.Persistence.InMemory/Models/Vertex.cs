﻿namespace Airlines.Persistence.InMemory.Models;
public class Vertex(string data)
{
    public string Data { get; set; } = data;
    public List<Edge> Children { get; set; } = [];
}

