using Airlines.Persistence.InMemory.Exceptions;
using Airlines.Persistence.InMemory.Models.Interfaces;

namespace Airlines.Persistence.InMemory.Models;
public class Airline : IAirline
{
    public string Name { get; set; }

    public Airline(string name)
    {
        if (string.IsNullOrWhiteSpace(name))
        {
            throw new InvalidAirlineNameException("Airline name cannot be null or empty.");
        }
        Name = name;
    }
    public override string ToString() => $"{Name}";
}