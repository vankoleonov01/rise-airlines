﻿namespace Airlines.Persistence.InMemory.Models;
public class PrivateAircraft(string name, int seats, string model) : Aircraft(name, model)
{
    public int? Seats { get; set; } = seats;
    public override string ToString() => $"Aircraft {Model} with {Seats} seats";
}