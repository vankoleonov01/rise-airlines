﻿namespace Airlines.Persistence.InMemory.Models;
public class PassengerAircraft(string name, double weight, double volume, int seats, string model) : Aircraft(name, model)
{
    public double? CargoWeight { get; set; } = weight;
    public double? CargoVolume { get; set; } = volume;
    public int? Seats { get; set; } = seats;

    public override string ToString() => $"Aircraft {Model} with small baggage weight {CargoWeight} kg, large baggage volume {CargoVolume} m³, and {Seats} seats";
}
