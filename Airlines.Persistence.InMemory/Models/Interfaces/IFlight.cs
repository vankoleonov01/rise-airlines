namespace Airlines.Persistence.InMemory.Models.Interfaces;
public interface IFlight
{
    string Identifier { get; set; }
    string DepartureAirport { get; set; }
    string ArrivalAirport { get; set; }
    string Aircraft { get; set; }
}