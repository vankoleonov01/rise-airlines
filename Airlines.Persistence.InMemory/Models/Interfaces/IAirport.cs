namespace Airlines.Persistence.InMemory.Models.Interfaces;

public interface IAirport
{
    string Identifier { get; set; }
    string Name { get; set; }
    string City { get; set; }
    string Country { get; set; }
}