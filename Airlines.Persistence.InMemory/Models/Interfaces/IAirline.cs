namespace Airlines.Persistence.InMemory.Models.Interfaces;

public interface IAirline
{
    string Name { get; set; }
}