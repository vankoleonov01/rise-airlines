﻿namespace Airlines.Persistence.InMemory.Models.Interfaces;
public interface IAircraft
{
    string Model { get; set; }
    string Type { get; set; }
}
