﻿namespace Airlines.Persistence.InMemory.Models;
public class Edge(Vertex destination, double time, double price)
{
    public Vertex Destination { get; set; } = destination;
    public double Time { get; set; } = time;
    public double Price { get; set; } = price;
}