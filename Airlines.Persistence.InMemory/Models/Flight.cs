using Airlines.Persistence.InMemory.Exceptions;
using Airlines.Persistence.InMemory.Models.Interfaces;

namespace Airlines.Persistence.InMemory.Models;

public class Flight : IFlight
{
    public string Identifier { get; set; }
    public string DepartureAirport { get; set; }
    public string ArrivalAirport { get; set; }
    public string Aircraft { get; set; }
    public double Price { get; set; }
    public double Duration { get; set; }


    public Flight(string identifier, string departureAirport, string arrivalAirport, string aircraft, double price, double duration)
    {
        // Validate inputs
        if (string.IsNullOrWhiteSpace(identifier))
        {
            throw new InvalidFlightCodeException("Flight identifier cannot be null or empty.");
        }

        Identifier = identifier;
        DepartureAirport = departureAirport;
        ArrivalAirport = arrivalAirport;
        Aircraft = aircraft;
        Price = price;
        Duration = duration;
    }
    public static bool FlightValidation(string check)
       => !string.IsNullOrEmpty(check) && check.All(char.IsLetterOrDigit);

    public override string ToString() => $"Flight {Identifier}: From {DepartureAirport} to {ArrivalAirport} with {Aircraft}, Price {Price}, Duration{Duration}";
}