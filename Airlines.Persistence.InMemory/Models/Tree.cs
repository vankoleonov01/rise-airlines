﻿using Airlines.Persistence.InMemory.Models.Interfaces;

namespace Airlines.Persistence.InMemory.Models;

public class Tree
{
    public Node? Root;

    public Tree() { }

    public void Insert(string data) => Root = InsertRecursive(Root, data);

    public static bool DoesFlightExist(List<IFlight>? flights, string flightId) => flights?.Any(flight => flight.Identifier == flightId) ?? false;

    public static string GetArrivalAirport(List<IFlight>? flights, string flightId)
    {
        var flight = flights?.FirstOrDefault(f => f.Identifier == flightId);
        return flight?.DepartureAirport ?? string.Empty;
    }

    public bool AlreadyInserted(string flightId) => Root?.Children.Any(flight => flight.Data == flightId) ?? false;

    public Node? GetNode(string flightId) => Root?.Children.FirstOrDefault(flight => flight.Data == flightId);

    public static string GetDepartureAirport(List<IFlight>? flights, string flightId)
    {
        var flight = flights?.FirstOrDefault(f => f.Identifier == flightId);
        return flight?.ArrivalAirport ?? string.Empty;
    }

    public Node InsertRecursive(Node? root, string data)
    {
        if (root == null)
        {
            root = new Node(data);
            return root;
        }

        int index = 0;
        while (index < root.Children.Count && string.CompareOrdinal(data, root.Children[index].Data) > 0)
        {
            if (AlreadyInserted(data))
            {
                var node = GetNode(data);
                node?.Children.Insert(0, new Node(data));
            }
            index++;
        }

        root.Children.Insert(index, new Node(data));
        return root;
    }
}