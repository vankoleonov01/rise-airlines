﻿using Airlines.Business.Dto;
using Airlines.Persistence.InMemory.Entities;
using Airlines.Persistence.InMemory.EntitiesRepository.Interfaces;
using AutoMapper;
using Airlines.Business.Services.Interfaces;
using System.Linq.Expressions;

namespace Airlines.Business.Services;

public class AirportService(IAirportRepository airportRepository, IMapper airportMapper) : IAirportService
{
    private readonly IAirportRepository _airportRepository = airportRepository;
    private readonly IMapper _airportMapper = airportMapper;

    public async Task<bool> AddAirport(AirportDto airportDto)
    {
        if (airportDto == null)
        {
            throw new ArgumentNullException(nameof(airportDto), "AirportDto cannot be null");
        }
        var airport = _airportMapper.Map<Airport>(airportDto);
        _ = await _airportRepository.AddAirportAsync(airport);
        return true;
    }

    public Task DeleteAirport(int airportId) => _airportRepository.DeleteAirportAsync(airportId);

    public async Task UpdateAirport(AirportDto airportDto)
    {
        var airport = _airportMapper.Map<Airport>(airportDto);
        _ = await _airportRepository.UpdateAirportAsync(airport);
    }

    public async Task<List<AirportDto>> GetAirports(int page, int pageSize)
    {
        var airports = await _airportRepository.GetAllAirportsAsync(page, pageSize);
        var airportsList = _airportMapper.Map<List<AirportDto>>(airports);
        return airportsList;
    }

    public async Task<AirportDto> GetAirportByIdAsync(int airportId)
    {
        var airport = await _airportRepository.GetAirportByIdAsync(airportId);
        if (airport != null)
        {
            return _airportMapper.Map<AirportDto>(airport);
        }
        return null!;
    }

    public async Task<int> GetTotalAirportsCount() => await _airportRepository.GetTotalAirportsCount();

    public async Task<List<AirportDto>> SearchAndFilterAirports(string filterCriteria, string filterValue, DateOnly? founded)
    {
        var airports = await _airportRepository.SearchAndFilterAsync(filterCriteria, filterValue!, founded);
        var airportsList = _airportMapper.Map<List<AirportDto>>(airports);
        return airportsList;
    }
}