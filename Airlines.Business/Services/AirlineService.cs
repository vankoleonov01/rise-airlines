﻿using Airlines.Business.Dto;
using Airlines.Persistence.InMemory.Entities;
using Airlines.Persistence.InMemory.EntitiesRepository.Interfaces;
using AutoMapper;
using Airlines.Business.Services.Interfaces;
using Airlines.Business.Mapping;

namespace Airlines.Business.Services;

public class AirlineService(IAirlineRepository airlineRepository, IMapper airlineMapper) : IAirlineService
{
    private readonly IAirlineRepository _airlineRepository = airlineRepository;
    private readonly IMapper _airlineMapper = airlineMapper;
    public async Task<bool> AddAirline(AirlineDto airlineDto)
    {
        if (airlineDto == null)
        {
            throw new ArgumentNullException(nameof(airlineDto), "AirlineDto cannot be null");
        }
        var airline = _airlineMapper.Map<Airline>(airlineDto);
        _ = await _airlineRepository.AddAirlineAsync(airline);
        return true;
    }

    public Task DeleteAirline(int airlineId) => _airlineRepository.DeleteAirlineAsync(airlineId);

    public async Task UpdateAirline(AirlineDto airlineDto)
    {
        if (airlineDto == null)
        {
            throw new ArgumentNullException(nameof(airlineDto), "AirlineDto cannot be null");
        }
        var airline = _airlineMapper.Map<Airline>(airlineDto);
        _ = await _airlineRepository.UpdateAirlineAsync(airline);
    }

    public async Task<List<AirlineDto>> GetAirlines(int page, int pageSize)
    {
        var airlines = await _airlineRepository.GetAllAirlinesAsync(page, pageSize);
        var airlinesList = _airlineMapper.Map<List<AirlineDto>>(airlines);
        return airlinesList;
    }

    public async Task<AirlineDto> GetAirlineByIdAsync(int airlineId)
    {
        var airline = await _airlineRepository.GetAirlineByIdAsync(airlineId);
        if (airline != null)
        {
            return _airlineMapper.Map<AirlineDto>(airline);
        }
        return null!;
    }
    public async Task<int> GetTotalAirlinesCount() => await _airlineRepository.GetTotalAirlinesCount();

    public async Task<List<AirlineDto>> SearchAndFilterAirlines(string filterCriteria, string filterValue, DateOnly? founded)
    {
        var airlines = await _airlineRepository.SearchAndFilterAsync(filterCriteria, filterValue!, founded);
        var airlinesList = _airlineMapper.Map<List<AirlineDto>>(airlines);
        return airlinesList;
    }
}