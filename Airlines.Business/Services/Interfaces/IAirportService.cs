﻿using Airlines.Business.Dto;
namespace Airlines.Business.Services.Interfaces;

public interface IAirportService
{
    Task<bool> AddAirport(AirportDto airportDto);
    Task DeleteAirport(int airportId);
    Task UpdateAirport(AirportDto airportDto);
    Task<List<AirportDto>> GetAirports(int page, int pageSize);
    Task<AirportDto> GetAirportByIdAsync(int airportId);
    Task<int> GetTotalAirportsCount();
    Task<List<AirportDto>> SearchAndFilterAirports(string filterCriteria, string filterValue, DateOnly? founded);
}