﻿using Airlines.Business.Dto;

namespace Airlines.Business.Services.Interfaces;

public interface IAirlineService
{
    Task<bool> AddAirline(AirlineDto airlineDto);
    Task DeleteAirline(int airlineId);
    Task UpdateAirline(AirlineDto airlineDto);
    Task<List<AirlineDto>> GetAirlines(int page, int pageSize);
    Task<AirlineDto> GetAirlineByIdAsync(int airlineId);
    Task<int> GetTotalAirlinesCount();
    Task<List<AirlineDto>> SearchAndFilterAirlines(string filterCriteria, string filterValue, DateOnly? founded);
}