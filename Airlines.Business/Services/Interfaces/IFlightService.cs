﻿using Airlines.Business.Dto;

namespace Airlines.Business.Services.Interfaces;

public interface IFlightService
{
    Task<bool> AddFlight(FlightDto flightDto);
    Task DeleteFlight(int flightId);
    Task UpdateFlight(FlightDto flightDto);
    Task<List<FlightDto>> GetFlights(int page, int pageSize);
    Task<FlightDto> GetFlightByIdAsync(int flightId);
    Task<int> GetTotalFlightsCount();
    Task<List<FlightDto>> SearchAndFilterFlights(string filterCriteria, string filterValue, DateTime? departureDate, DateTime? arrivalDate);

}