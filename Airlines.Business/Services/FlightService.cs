﻿using Airlines.Business.Dto;
using Airlines.Persistence.InMemory.Entities;
using Airlines.Persistence.InMemory.EntitiesRepository.Interfaces;
using AutoMapper;
using Airlines.Business.Services.Interfaces;
using Airlines.Business.Mapping;

namespace Airlines.Business.Services;

public class FlightService(IFlightRepository flightRepository, IMapper flightMapper, IAirportRepository airportRepository) : IFlightService
{
    private readonly IFlightRepository _flightRepository = flightRepository;
    private readonly IAirportRepository _airportRepository = airportRepository;
    private readonly IMapper _flightMapper = flightMapper;

    public async Task<bool> AddFlight(FlightDto flightDto)
    {
        if (flightDto == null)
        {
            throw new ArgumentNullException(nameof(flightDto), "FlightDto cannot be null");
        }
        var flight = _flightMapper.Map<Flight>(flightDto);
        _ = await _flightRepository.AddFlightAsync(flight);
        return true;
    }

    public Task DeleteFlight(int flightId) => _flightRepository.DeleteFlightAsync(flightId);

    public async Task UpdateFlight(FlightDto flightDto)
    {
        var flight = _flightMapper.Map<Flight>(flightDto);
        _ = await _flightRepository.UpdateFlightAsync(flight);
    }

    public async Task<List<FlightDto>> GetFlights(int page, int pageSize)
    {
        var flights = await _flightRepository.GetAllFlightsAsync(page, pageSize);
        var flightsList = _flightMapper.Map<List<FlightDto>>(flights);
        return flightsList;
    }

    public async Task<FlightDto> GetFlightByIdAsync(int flightId)
    {
        var flight = await _flightRepository.GetFlightByIdAsync(flightId);

        if (flight != null)
        {
            var fromAirport = await _airportRepository.GetAirportByIdAsync(flight.FromAirportId);
            var toAirport = await _airportRepository.GetAirportByIdAsync(flight.ToAirportId);

            flight.FromAirport = fromAirport;
            flight.ToAirport = toAirport;
        }

        return _flightMapper.Map<FlightDto>(flight);
    }
    public async Task<int> GetTotalFlightsCount() => await _flightRepository.GetTotalFlightsCount();
    public async Task<List<FlightDto>> SearchAndFilterFlights(string filterCriteria, string filterValue, DateTime? departureDate, DateTime? arrivalDate)
    {
        var flights = await _flightRepository.SearchAndFilterAsync(filterCriteria, filterValue!, departureDate, arrivalDate);
        var flightsList = _flightMapper.Map<List<FlightDto>>(flights);
        return flightsList;
    }
}