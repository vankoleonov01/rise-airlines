using Airlines.Business.Commands.Exist;
using Airlines.Business.Commands.List;
using Airlines.Business.Commands.Reserve;
using Airlines.Business.Commands.Route;
using Airlines.Business.Commands.Search;
using Airlines.Business.Commands.Sort;
using Airlines.Persistence.InMemory;
using Airlines.Persistence.InMemory.Exceptions;
using Airlines.Persistence.InMemory.Management;
using Airlines.Persistence.InMemory.Models;
using Airlines.Persistence.InMemory.Models.Interfaces;

namespace Airlines.Business.Commands;

public class CommandFactory
{
    private static readonly List<CargoAircraft>? _cargoAircrafts = [];
    private static readonly List<PassengerAircraft>? _passengerAircrafts = [];
    private static readonly Queue<ICommand> _batchCommands = new();
    private static readonly RouteManagement _routeManager = new(new LinkedList<IFlight>());
    private static bool _batchMode;

    internal static ICommand? CreateCommand(string[] tokens, DataManagement? dataManagement, List<IAirport>? airports, List<IFlight>? flights, List<IAirline>? airlines, List<CargoAircraft>? cargoAircrafts, List<PassengerAircraft>? passengerAircrafts, LinkedList<IFlight>? route)
    {
        if (tokens.Length == 0)
        {
            throw new InvalidCommandException("Invalid command length.");
        }

        ArgumentNullException.ThrowIfNull(dataManagement);
        if (cargoAircrafts != null)
        {
            ArgumentNullException.ThrowIfNull(passengerAircrafts);
            var action = tokens[0].ToLower();

            switch (action)
            {
                case "search":
                    if (tokens.Length > 1)
                    {
                        var searchTerm = string.Join(" ", tokens[1..]);
                        return new SearchCommand(dataManagement, searchTerm);
                    }
                    else
                    {
                        Console.WriteLine("Search term is missing.");
                    }
                    break;

                case "sort":
                    if (tokens.Length > 1)
                    {
                        var dataType = tokens[1].ToLower();
                        var sortOrder = tokens.Length > 2 ? tokens[2].ToLower() : "ascending";
                        return new SortCommand(dataType, sortOrder);
                    }
                    else
                    {
                        Console.WriteLine("Data type is missing.");
                    }
                    break;

                case "exist":
                    if (tokens.Length > 1)
                    {
                        var airlineName = string.Join(" ", tokens[1..]);
                        return new ExistCommand(airlineName, airlines);
                    }
                    else
                    {
                        Console.WriteLine("Airline name is missing.");
                    }
                    break;

                case "list":
                    if (tokens.Length > 2)
                    {
                        var inputData = tokens[1];
                        var from = tokens[2];
                        return new ListCommand(inputData, from, airports);
                    }
                    else
                    {
                        Console.WriteLine("Input data or 'from' parameter is missing.");
                    }
                    break;

                case "route":
                    if (tokens.Length > 1)
                    {
                        var subAction = tokens[1].ToLower();
                        var graphRead = FileReader.ReadFlightsGraphFile(@"../../../flight_in.csv");

                        switch (subAction)
                        {
                            case "new":
                                return new RouteCommand(route);
                            case "add":
                                if (tokens.Length > 4)
                                {
                                    var flightIdentifier = tokens[2];
                                    var departureAirport = tokens[3];
                                    var arrivalAirport = tokens[4];
                                    var aircraft = tokens[5];
                                    var price = double.Parse(tokens[6]);
                                    var duration = double.Parse(tokens[7]);
                                    return new AddRouteCommand(flightIdentifier, departureAirport, arrivalAirport, aircraft, price, duration, _routeManager);
                                }
                                else
                                {
                                    Console.WriteLine("Insufficient parameters. Expected: flightIdentifier, departureAirport, arrivalAirport, aircraft");
                                }
                                break;

                            case "find":
                                var flightTree = FileReader.ReadFlightsRouteFile(@"../../../routes_in.csv");
                                var findRouteExecute = FindRouteCommand.FindRoute(flights, flightTree, tokens[2]);
                                findRouteExecute.PrintPath();
                                break;

                            case "check":
                                var checkRouteExecute = CheckRouteCommand.CheckRoute(flights, graphRead, tokens[2], tokens[3]);
                                checkRouteExecute.Execute(airports, airlines, flights);
                                break;

                            case "search":
                                var searchRouteExecute = SearchRouteCommand.SearchRoute(flights!, graphRead, tokens[2], tokens[3], tokens[4]);
                                searchRouteExecute.Execute(airports, airlines, flights);
                                break;

                            case "remove":
                                return new RemoveRouteCommand(_routeManager);
                            case "print":
                                return new PrintRouteCommand(_routeManager);
                            default:
                                Console.WriteLine("Invalid route command.");
                                break;
                        }
                    }
                    else
                    {
                        Console.WriteLine("Invalid route command.");
                    }
                    break;

                case "reserve":
                    if (tokens.Length > 1)
                    {
                        if (tokens[1] == "cargo")
                        {
                            return new ReserveCargoCommand(_cargoAircrafts!);
                        }
                        else if (tokens[1] == "ticket")
                        {
                            return new ReservePassengerCommand(_passengerAircrafts!);
                        }
                        else
                        {
                            Console.WriteLine("Invalid reservation type.");
                        }
                    }
                    else
                    {
                        Console.WriteLine("Reservation type is missing.");
                    }
                    break;

                case "batch":
                    StartBatchMode();
                    break;

                case "start":
                    ExecuteBatchCommands(airports, airlines, flights);
                    break;

                case "cancel":
                    CancelBatchMode();
                    break;

                default:
                    if (_batchMode)
                    {
                        AddToBatch(CreateSingleCommand(tokens, airports, flights, airlines)!);
                    }
                    else
                    {
                        throw new InvalidCommandException("Invalid command. Please try again!");
                    }
                    break;
            }
            return null;
        }

        throw new ArgumentNullException(nameof(cargoAircrafts));
    }

    internal static void StartBatchMode()
    {
        _batchMode = true;
        Console.WriteLine("Batch mode activated. Enter commands to add to the batch queue.");
    }

    internal static void ExecuteBatchCommands(List<IAirport>? airports, List<IAirline>? airlines, List<IFlight>? flights)
    {
        while (_batchCommands.Count > 0)
        {
            var command = _batchCommands.Dequeue();
            command.Execute(airports ?? [], airlines ?? [], flights ?? []);
        }
        _batchMode = false;
    }

    internal static void CancelBatchMode()
    {
        _batchCommands.Clear();
        _batchMode = false;
        Console.WriteLine("Batch mode canceled. Batch queue cleared.");
    }

    internal static void AddToBatch(ICommand command)
    {
        _batchCommands.Enqueue(command);
        Console.WriteLine("Command added to batch queue.");
    }
    internal static ICommand? CreateSingleCommand(string[] tokens, List<IAirport>? airports, List<IFlight>? flights, List<IAirline>? airlines)
    {
        ArgumentNullException.ThrowIfNull(tokens);
        ArgumentNullException.ThrowIfNull(airports);
        ArgumentNullException.ThrowIfNull(flights);
        ArgumentNullException.ThrowIfNull(airlines);
        return null;
    }
}