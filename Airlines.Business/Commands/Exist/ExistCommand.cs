using Airlines.Persistence.InMemory.Exceptions;
using Airlines.Persistence.InMemory.Models.Interfaces;

namespace Airlines.Business.Commands.Exist;

public class ExistCommand(string airlineName, List<IAirline>? airlines) : ICommand
{
    private readonly string _airlineName = airlineName ?? throw new ArgumentNullException(nameof(airlineName));
    private readonly List<IAirline> _airlines = airlines ?? throw new ArgumentNullException(nameof(airlines));

    public void Execute(List<IAirport>? airports, List<IAirline>? airlines, List<IFlight>? flights)
    {
        try
        {
            Console.WriteLine($"Checking existence of airline: {_airlineName}");
            bool exists = AirlineExists(_airlineName);
            Console.WriteLine($"Airline exists: {exists}");
        }
        catch (Exception ex)
        {
            Console.WriteLine($"An error occurred while checking airline existence: {ex.Message}");
            throw new InvalidAirlineListException($"Error checking airline existence: {ex.Message}", ex);
        }
    }

    internal bool AirlineExists(string airlineName)
    {
        return _airlines == null
            ? throw new InvalidAirlineNameException("Airlines list is null.")
            : _airlines.Any(airline => airline.Name.Split(' ').Any(name => name.Trim().Equals(airlineName, StringComparison.OrdinalIgnoreCase)));
    }
}