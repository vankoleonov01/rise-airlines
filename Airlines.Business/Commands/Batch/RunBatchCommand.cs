using Airlines.Persistence.InMemory.Exceptions;
using Airlines.Persistence.InMemory.Models.Interfaces;

namespace Airlines.Business.Commands.Batch;

public class RunBatchCommand(BatchModel batchModel) : ICommand
{
    private readonly BatchModel _batchModel = batchModel ?? throw new ArgumentNullException(nameof(batchModel));

    public static string? Output => string.Empty;

    public static bool ContinueProgramAfterExecution => true;

    public void Execute(List<IAirport>? airports, List<IAirline>? airlines, List<IFlight>? flights)
    {
        if (_batchModel == null)
        {
            throw new ArgumentNullException(nameof(_batchModel), "Batch model cannot be null.");
        }

        try
        {
            if (_batchModel.IsBatchModeActive)
            {
                _batchModel.Commands();
                _batchModel.IsBatchModeActive = false;
            }
        }
        catch (Exception ex)
        {
            throw new ExecutionBatchException("Error occurred while executing batch command.", ex);
        }
    }
}