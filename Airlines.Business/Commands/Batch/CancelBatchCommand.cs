using Airlines.Persistence.InMemory.Models.Interfaces;

namespace Airlines.Business.Commands.Batch;
public class CancelBatchCommand(BatchModel batchModel) : ICommand
{
    private readonly BatchModel _batchModel = batchModel;

    public static string? Output => string.Empty;

    public static bool ContinueProgramAfterExecution => true;

    public void Execute(List<IAirport>? airports, List<IAirline>? airlines, List<IFlight>? flights)
    {
        try
        {
            _batchModel.IsBatchModeActive = false;
            _batchModel.CleanCommands();
        }
        catch (Exception ex)
        {
            Console.WriteLine($"An error occurred while canceling batch mode: {ex.Message}");
        }
    }
}