namespace Airlines.Business.Commands.Batch;

public class BatchModel
{
    public BatchModel()
    {
        IsBatchModeActive = false;
        CleanCommands();
    }

    public bool IsBatchModeActive { get; set; }
    public Action Commands { get; set; } = DoNothingFunc;

    internal void AddCommand(Action action) => Commands += action;

    internal void CleanCommands() => Commands = DoNothingFunc;
    private static void DoNothingFunc() { }
}