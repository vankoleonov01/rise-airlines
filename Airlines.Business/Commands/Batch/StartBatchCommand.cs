using Airlines.Persistence.InMemory.Exceptions;
using Airlines.Persistence.InMemory.Models.Interfaces;

namespace Airlines.Business.Commands.Batch;

public class StartBatchCommand(BatchModel batchModel) : ICommand
{
    private readonly BatchModel _batchModel = batchModel ?? throw new ArgumentNullException(nameof(batchModel));
    public static string? Output => string.Empty;
    public static bool ContinueProgramAfterExecution => true;

    public void Execute(List<IAirport>? airports, List<IAirline>? airlines, List<IFlight>? flights)
    {
        try
        {
            if (_batchModel.IsBatchModeActive)
            {
                throw new ExecutionBatchException("Batch mode is already active.");
            }
            else
            {
                _batchModel.IsBatchModeActive = true;
                _batchModel.CleanCommands();
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"An error occurred while starting batch mode: {ex.Message}");
            throw new ExecutionBatchException($"Error starting batch mode: {ex.Message}", ex);
        }
    }
}