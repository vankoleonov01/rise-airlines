using Airlines.Persistence.InMemory.Exceptions;
using Airlines.Persistence.InMemory.Models.Interfaces;

namespace Airlines.Business.Commands.List;

public class ListCommand(string inputData, string from, List<IAirport>? airports) : ICommand
{
    private readonly string _inputData = inputData ?? throw new ArgumentNullException(nameof(inputData));
    private readonly string _from = from ?? throw new ArgumentNullException(nameof(from));
    private readonly List<IAirport> _airports = airports ?? throw new ArgumentNullException(nameof(airports));

    public void Execute(List<IAirport>? airports, List<IAirline>? airlines, List<IFlight>? flights)
    {
        Console.WriteLine($"Listing {_inputData} from {_from}");

        try
        {
            if (_airports == null || _airports.Count == 0)
            {
                throw new InvalidInputException("No airports available.");
            }

            var filteredAirports = _from.Equals("City", StringComparison.OrdinalIgnoreCase)
                ? _airports.Where(airport => airport.City.Equals(_inputData, StringComparison.OrdinalIgnoreCase))
                : _from.Equals("Country", StringComparison.OrdinalIgnoreCase)
                    ? _airports.Where(airport => airport.Country.Equals(_inputData, StringComparison.OrdinalIgnoreCase))
                    : throw new InvalidInputException("Invalid 'from' parameter.");
            if (!filteredAirports.Any())
            {
                throw new InvalidInputException($"No {_inputData} found in {_from}.");
            }

            foreach (var airport in filteredAirports)
            {
                Console.WriteLine(airport);
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
    }
}