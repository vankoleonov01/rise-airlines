using Airlines.Persistence.InMemory.Exceptions;
using Airlines.Persistence.InMemory.Management;
using Airlines.Persistence.InMemory.Models.Interfaces;

namespace Airlines.Business.Commands.Search;

public class SearchCommand(DataManagement? dataManagement, string? searchTerm) : ICommand
{
    private readonly DataManagement _dataManagement = dataManagement ?? throw new ArgumentNullException(nameof(dataManagement), "DataManagement object cannot be null.");
    private readonly string _searchTerm = searchTerm ?? throw new ArgumentNullException(nameof(searchTerm), "Search term cannot be null.");

    public void Execute(List<IAirport>? airports, List<IAirline>? airlines, List<IFlight>? flights)
    {
        try
        {
            _dataManagement.SearchData(_searchTerm);
        }
        catch (Exception ex)
        {
            throw new ExecutionSearchException("Error occurred while executing search command.", ex);
        }
    }
}