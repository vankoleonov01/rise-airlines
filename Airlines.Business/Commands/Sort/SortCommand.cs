using Airlines.Persistence.InMemory.Exceptions;
using Airlines.Persistence.InMemory.Models.Interfaces;

namespace Airlines.Business.Commands.Sort;

public class SortCommand : ICommand
{
    private readonly string? _dataType;
    private readonly string? _sortOrder;

    public SortCommand(string? dataType, string? sortOrder)
    {
        if (string.IsNullOrWhiteSpace(dataType))
        {
            throw new ArgumentException("Data type cannot be null or empty.", nameof(dataType));
        }

        if (string.IsNullOrWhiteSpace(sortOrder))
        {
            throw new ArgumentException("Sort order cannot be null or empty.", nameof(sortOrder));
        }

        _dataType = dataType;
        _sortOrder = sortOrder;
    }

    public void Execute(List<IAirport>? airports, List<IAirline>? airlines, List<IFlight>? flights)
    {
        try
        {
            Console.WriteLine($"Executing sort command for data type: {_dataType} and sort order: {_sortOrder}");

            switch (_dataType?.ToLower())
            {
                case "airlines":
                    SortAndPrint(airlines, airline => airline.Name);
                    break;

                case "airports":
                    SortAndPrint(airports, airport => airport.Name);
                    break;

                case "flights":
                    SortAndPrint(flights, flight => flight.Identifier);
                    break;

                default:
                    throw new InvalidDataException($"Invalid data type: {_dataType}");
            }
        }
        catch (Exception ex)
        {
            throw new ExecutionSortException("Error occurred while executing sort command.", ex);
        }
    }

    private void SortAndPrint<T>(List<T>? data, Func<T, string> selector)
    {
        if (data is { Count: 0 })
        {
            throw new ArgumentException("Value cannot be an empty collection.", nameof(data));
        }

        if (data == null)
        {
            return;
        }

        var sortedData = _sortOrder != null && _sortOrder.Equals("ascending", StringComparison.OrdinalIgnoreCase)
            ? data.OrderBy(selector)
            : data.OrderByDescending(selector);

        foreach (var item in sortedData)
        {
            Console.WriteLine(item);
        }
    }
}