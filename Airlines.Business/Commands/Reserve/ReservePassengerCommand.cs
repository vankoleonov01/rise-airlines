using Airlines.Persistence.InMemory.Exceptions;
using Airlines.Persistence.InMemory.Management;
using Airlines.Persistence.InMemory.Models;
using Airlines.Persistence.InMemory.Models.Interfaces;

namespace Airlines.Business.Commands.Reserve;

public class ReservePassengerCommand(List<PassengerAircraft>? passengerAircrafts) : ICommand
{
    private readonly List<PassengerAircraft>? _passengerAircrafts = passengerAircrafts ?? throw new ArgumentNullException(nameof(passengerAircrafts));

    public void Execute(List<IAirport>? airports, List<IAirline>? airlines, List<IFlight>? flights)
    {
        try
        {
            Console.WriteLine("Enter flight ID:");
            string flightId = Console.ReadLine()?.Trim()!;

            if (string.IsNullOrEmpty(flightId))
            {
                throw new InvalidInputException("Flight ID cannot be empty.");
            }

            Console.WriteLine("Enter number of seats:");
            if (!int.TryParse(Console.ReadLine()?.Trim(), out int seats) || seats <= 0)
            {
                throw new InvalidInputException("Invalid number of seats. Please enter a positive integer value.");
            }

            Console.WriteLine("Enter number of small baggage:");
            if (!double.TryParse(Console.ReadLine()?.Trim(), out double smallBaggageCount) || smallBaggageCount < 0)
            {
                throw new InvalidInputException("Invalid number of small baggage. Please enter a non-negative numeric value.");
            }

            Console.WriteLine("Enter number of large baggage:");
            if (!double.TryParse(Console.ReadLine()?.Trim(), out double largeBaggageCount) || largeBaggageCount < 0)
            {
                throw new InvalidInputException("Invalid number of large baggage. Please enter a non-negative numeric value.");
            }

            bool result = ReserveManagement.IsPassengerTicketPossible(flights, _passengerAircrafts, flightId, seats, smallBaggageCount, largeBaggageCount, "passenger");
            Console.WriteLine(result ? "Passenger reservation is possible." : "Passenger reservation is not possible.");
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
    }
}