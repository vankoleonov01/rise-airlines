using Airlines.Persistence.InMemory.Exceptions;
using Airlines.Persistence.InMemory.Management;
using Airlines.Persistence.InMemory.Models;
using Airlines.Persistence.InMemory.Models.Interfaces;

namespace Airlines.Business.Commands.Reserve;

public class ReserveCargoCommand(List<CargoAircraft>? cargoAircrafts) : ICommand
{
    private readonly List<CargoAircraft>? _cargoAircrafts = cargoAircrafts ?? throw new ArgumentNullException(nameof(cargoAircrafts));

    public void Execute(List<IAirport>? airports, List<IAirline>? airlines, List<IFlight>? flights)
    {
        try
        {
            Console.WriteLine("Enter flight ID:");
            string flightId = Console.ReadLine()?.Trim()!;

            if (string.IsNullOrEmpty(flightId))
            {
                throw new InvalidInputException("Flight ID cannot be empty.");
            }

            Console.WriteLine("Enter cargo weight:");
            if (!double.TryParse(Console.ReadLine()?.Trim(), out double cargoWeight) || cargoWeight <= 0)
            {
                throw new InvalidInputException("Invalid cargo weight. Please enter a positive numeric value.");
            }

            Console.WriteLine("Enter cargo volume:");
            if (!double.TryParse(Console.ReadLine()?.Trim(), out double cargoVolume) || cargoVolume <= 0)
            {
                throw new InvalidInputException("Invalid cargo volume. Please enter a positive numeric value.");
            }

            bool result = ReserveManagement.IsCargoReservationPossible(flights, _cargoAircrafts, flightId, cargoWeight, cargoVolume, "cargo");
            Console.WriteLine(result ? "Cargo reservation is possible." : "Cargo reservation is not possible.");
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
    }
}