﻿using Airlines.Persistence.InMemory.Models.Interfaces;

namespace Airlines.Business.Commands;
public interface ICommand
{
    void Execute(List<IAirport>? airports, List<IAirline>? airlines, List<IFlight>? flights);
}
