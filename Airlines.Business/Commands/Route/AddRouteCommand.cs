using Airlines.Persistence.InMemory.Exceptions;
using Airlines.Persistence.InMemory.Management;
using Airlines.Persistence.InMemory.Models;
using Airlines.Persistence.InMemory.Models.Interfaces;

namespace Airlines.Business.Commands.Route;

public class AddRouteCommand(string flightIdentifier, string departureAirport, string arrivalAirport, string aircraft, double price, double duration, RouteManagement routeManager) : ICommand
{
    private readonly string _flightIdentifier = flightIdentifier ?? throw new ArgumentNullException(nameof(flightIdentifier));
    private readonly string _departureAirport = departureAirport ?? throw new ArgumentNullException(nameof(departureAirport));
    private readonly string _arrivalAirport = arrivalAirport ?? throw new ArgumentNullException(nameof(arrivalAirport));
    private readonly string _aircraft = aircraft ?? throw new ArgumentNullException(nameof(aircraft));
    public readonly double _price = price;
    public readonly double _duration = duration;
    private readonly RouteManagement _routeManager = routeManager ?? throw new ArgumentNullException(nameof(routeManager));

    public void Execute(List<IAirport>? airports, List<IAirline>? airlines, List<IFlight>? flights)
    {
        try
        {
            Console.WriteLine($"Adding flight {_flightIdentifier} to the route.");
            IFlight flight = new Flight(_flightIdentifier, _departureAirport, _arrivalAirport, _aircraft, _price, _duration);
            _routeManager.AddFlight(flight);
        }
        catch (ExecutionRouteException ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
    }
}