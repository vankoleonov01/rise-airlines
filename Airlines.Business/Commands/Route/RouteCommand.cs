using Airlines.Persistence.InMemory.Exceptions;
using Airlines.Persistence.InMemory.Models.Interfaces;

namespace Airlines.Business.Commands.Route;

public class RouteCommand(LinkedList<IFlight>? route) : ICommand
{
    private readonly LinkedList<IFlight> _route = route ?? throw new ArgumentNullException(nameof(route));

    public void Execute(List<IAirport>? airports, List<IAirline>? airlines, List<IFlight>? flights)
    {
        try
        {
            Console.WriteLine("Creating a new route.");
            _route.Clear();
        }
        catch (Exception ex)
        {
            Console.WriteLine($"An error occurred while creating a new route: {ex.Message}");
            throw new ExecutionRouteException("Error creating a new route.", ex);
        }
    }
}