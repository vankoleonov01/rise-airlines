﻿using Airlines.Persistence.InMemory.Models;
using Airlines.Persistence.InMemory.Models.Interfaces;

namespace Airlines.Business.Commands.Route;

public class SearchRouteCommand(List<IFlight> flights, Graph graph, string startAirport, string departureAirport, string byWeight) : Persistence.InMemory.Models.Route(flights), ICommand
{
    public Graph _graph = graph;
    public string _startAirport = startAirport.ToUpper();
    public string _departureAirport = departureAirport.ToUpper();
    public string _byWeight = byWeight;

    public static SearchRouteCommand SearchRoute(List<IFlight> flights, Graph graph, string startAirport, string departureAirport, string byWeight)
    {
        var newRoute = new SearchRouteCommand(flights, graph, startAirport, departureAirport, byWeight);

        return newRoute;
    }

    public void Execute()
    {
        try
        {
            List<Edge> path;
            if (_byWeight == "time")
            {
                Console.WriteLine("Time");
                path = FindRouteByTime();
            }
            else
            {
                path = _byWeight == "price"
                    ? FindRouteByPrice()
                    : _byWeight == "shortest" ? FindShortestRoute() : throw new ArgumentException("Invalid weight type specified.");
            }

            if (path.Count == 0)
            {
                Console.WriteLine("No route found.");
                return;
            }

            Console.WriteLine($"Route from {_startAirport} to {_departureAirport}:");
            Console.WriteLine($"{"Destination",-20} {"Price",-10} {"Time (Hours)"}");
            foreach (var edge in path)
            {
                Console.WriteLine($"- {edge.Destination.Data,-20} ${edge.Price,-10} {edge.Time,10:F2}");
            }
        }
        catch (ArgumentException ex)
        {
            Console.WriteLine(ex.Message);
        }
    }

    public List<Edge> FindRouteByTime()
    {
        var pathFinder = new FindPathCommand(_graph);
        return pathFinder.FindPathByWeight(_startAirport, _departureAirport, "time");
    }

    public List<Edge> FindRouteByPrice()
    {
        var pathFinder = new FindPathCommand(_graph);
        return pathFinder.FindPathByWeight(_startAirport, _departureAirport, "price");
    }

    public List<Edge> FindShortestRoute()
    {
        var pathFinder = new FindPathCommand(_graph);
        return pathFinder.FindPathByWeight(_startAirport, _departureAirport, "shortest");
    }

    public void Execute(List<IAirport>? airports, List<IAirline>? airlines, List<IFlight>? flights) => Execute();
}

