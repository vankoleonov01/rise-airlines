using Airlines.Persistence.InMemory.Exceptions;
using Airlines.Persistence.InMemory.Management;
using Airlines.Persistence.InMemory.Models.Interfaces;

namespace Airlines.Business.Commands.Route;

public class RemoveRouteCommand(RouteManagement routeManager) : ICommand
{
    private readonly RouteManagement _routeManager = routeManager ?? throw new ArgumentNullException(nameof(routeManager));

    public void Execute(List<IAirport>? airports, List<IAirline>? airlines, List<IFlight>? flights)
    {
        try
        {
            Console.WriteLine("Removing the last flight from the route.");
            _routeManager.RemoveFlightAtEnd();
        }
        catch (ExecutionRouteException ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
    }
}