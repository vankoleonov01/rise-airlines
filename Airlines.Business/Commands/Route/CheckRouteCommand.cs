﻿using Airlines.Persistence.InMemory.Models;
using Airlines.Persistence.InMemory.Models.Interfaces;

namespace Airlines.Business.Commands.Route;
public class CheckRouteCommand(List<IFlight>? flights, Graph graph, string startAirport, string departureAirport) : Persistence.InMemory.Models.Route(flights), ICommand
{
    public Graph _graph = graph;
    public string _startAirport = startAirport.ToUpper();
    public string _departureAirport = departureAirport.ToUpper();

    internal static CheckRouteCommand CheckRoute(List<IFlight>? flights, Graph graph, string startAirport, string departureAirport)
    {
        var newRoute = new CheckRouteCommand(flights, graph, startAirport, departureAirport);

        return newRoute;
    }

    public bool CheckAirports()
    {
        foreach (var vertex in _graph.Vertices)
        {
            if (vertex.Data == _startAirport)
            {
                foreach (var child in vertex.Children)
                {
                    if (child.Destination.Data == _departureAirport)
                    {
                        return true;
                    }
                }
            }
            else
            {
                return false;
            }
        }
        return false;
    }

    public void Execute(List<IAirport>? airports, List<IAirline>? airlines, List<IFlight>? flights) => Console.WriteLine(CheckAirports());
}
