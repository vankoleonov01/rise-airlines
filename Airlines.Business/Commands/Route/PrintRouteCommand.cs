using Airlines.Persistence.InMemory.Exceptions;
using Airlines.Persistence.InMemory.Management;
using Airlines.Persistence.InMemory.Models.Interfaces;

namespace Airlines.Business.Commands.Route;

public class PrintRouteCommand(RouteManagement routeManager) : ICommand
{
    private readonly RouteManagement _routeManager = routeManager ?? throw new ArgumentNullException(nameof(routeManager));

    public void Execute(List<IAirport>? airports, List<IAirline>? airlines, List<IFlight>? flights)
    {
        try
        {
            Console.WriteLine("Printing the current route:");
            _routeManager.PrintRoute();
        }
        catch (ExecutionRouteException ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
    }
}