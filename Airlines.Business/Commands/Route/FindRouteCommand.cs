﻿using Airlines.Persistence.InMemory.Models;
using Airlines.Persistence.InMemory.Models.Interfaces;

namespace Airlines.Business.Commands.Route;
public class FindRouteCommand(List<IFlight>? flights, Tree flightTrees, string find) : Persistence.InMemory.Models.Route(flights), ICommand
{
    private readonly Tree _flightTree = flightTrees;
    private readonly string _find = find.ToUpper();

    public static FindRouteCommand FindRoute(List<IFlight>? flights, Tree flightTrees, string find)
    {
        var newRoute = new FindRouteCommand(flights, flightTrees, find);

        return newRoute;
    }

    internal List<Node> FindPathToNode(string targetData)
    {
        List<Node> path = [];
        if (_flightTree.Root != null)
        {
            _ = FindPathRecursive(_flightTree.Root, targetData, path);
        }
        return path;
    }

    internal static bool FindPathRecursive(Node current, string targetData, List<Node> path)
    {
        if (current == null)
            return false;

        path.Add(current);

        if (current.Data == targetData)
            return true;

        foreach (var child in current.Children)
        {
            if (FindPathRecursive(child, targetData, path))
                return true;
        }

        path.RemoveAt(path.Count - 1);
        return false;
    }

    internal void PrintPath()
    {
        var path = FindPathToNode(_find);
        foreach (var node in path)
        {
            Console.WriteLine(node.Data);
        }
    }
    public void Execute(List<IAirport>? airports, List<IAirline>? airlines, List<IFlight>? flights) => PrintPath();
}