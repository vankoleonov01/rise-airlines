﻿using System.ComponentModel.DataAnnotations;

namespace Airlines.Business.Dto;

public class FlightDto
{
    public FlightDto() { }

    public int FlightId { get; set; }

    [Required(ErrorMessage = "Flight number is required.")]
    [StringLength(10, ErrorMessage = "Flight number cannot exceed 10 characters.")]
    public string FlightNumber { get; set; } = string.Empty;

    [Required(ErrorMessage = "Departure airport ID is required.")]
    public int FromAirportId { get; set; }

    [Required(ErrorMessage = "Destination airport ID is required.")]
    public int ToAirportId { get; set; }

    [Required(ErrorMessage = "Departure date and time is required.")]
    public DateTime DepartureDateTime { get; set; }

    [Required(ErrorMessage = "Arrival date and time is required.")]
    public DateTime ArrivalDateTime { get; set; }

    public DateTime? RealDepartureDateTime { get; set; }

    public DateTime? RealArrivalDateTime { get; set; }

    public bool? Cancelled { get; set; }

    [StringLength(100, ErrorMessage = "Cancellation reason cannot exceed 100 characters.")]
    public string CancelationReason { get; set; } = string.Empty;

    public DateTime? CreatedDate { get; set; }

    public DateTime? ModifiedDate { get; set; }

    public int? AirlineId { get; set; }

    public AirportDto? FromAirport { get; set; }

    public AirportDto? ToAirport { get; set; }
}