﻿using System.ComponentModel.DataAnnotations;

namespace Airlines.Business.Dto;

public class AirportDto
{
    public AirportDto() { }

    public int AirportId { get; set; }

    [Required(ErrorMessage = "Airport name is required.")]
    [StringLength(100, ErrorMessage = "Airport name cannot exceed 100 characters.")]
    public string Name { get; set; } = string.Empty;

    [Required(ErrorMessage = "Country name is required.")]
    [StringLength(100, ErrorMessage = "Country name cannot exceed 100 characters.")]
    public string Country { get; set; } = string.Empty;

    [Required(ErrorMessage = "City name is required.")]
    [StringLength(100, ErrorMessage = "City name cannot exceed 100 characters.")]
    public string City { get; set; } = string.Empty;

    [Required(ErrorMessage = "Airport code is required.")]
    [StringLength(3, MinimumLength = 3, ErrorMessage = "Airport code must be 3 characters.")]
    public string Code { get; set; } = string.Empty;

    [Range(0, int.MaxValue, ErrorMessage = "Runways count must be a positive number.")]
    public int? RunwaysCount { get; set; }

    public DateOnly? Founded { get; set; }

    public DateTime? CreatedDate { get; set; }

    public DateTime? ModifiedDate { get; set; }

    public ICollection<FlightDto>? FlightFromAirports { get; set; }

    public ICollection<FlightDto>? FlightToAirports { get; set; }
}