﻿using System.ComponentModel.DataAnnotations;

namespace Airlines.Business.Dto;

public class AirlineDto
{
    public AirlineDto() { }

    public int AirlineId { get; set; }

    [Required(ErrorMessage = "Name is required")]
    public string Name { get; set; } = string.Empty;

    [Required(ErrorMessage = "Founded date is required")]
    public DateOnly Founded { get; set; }

    [Required(ErrorMessage = "Fleet size is required")]
    [Range(0, int.MaxValue, ErrorMessage = "Fleet size must be a positive number")]
    public int FleetSize { get; set; }

    [Required(ErrorMessage = "Description is required")]
    public string Description { get; set; } = string.Empty;

    public DateTime? CreatedDate { get; set; }

    public DateTime? ModifiedDate { get; set; }
}