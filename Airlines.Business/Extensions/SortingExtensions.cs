﻿namespace Airlines.Business.Extensions;
public static class SortingExtensions
{
    /// <summary>
    /// Bubble sort algorithm to sort the array of strings.
    /// </summary>
    /// <param name="arr"></param>
    /// <returns></returns>
    public static string[] BubbleSort(this string[] arr, bool ascending = true)
    {
        // 1. Compare each element with the next element and swap if necessary.
        for (var i = 0; i < arr.Length - 1; i++)
            // 2. The largest element will be moved to the end of the array.
            for (var j = 0; j < arr.Length - 1 - i; j++)
                // 3.Swap the elements if the current element is greater than the next element.
                if ((ascending && string.Compare(arr[j], arr[j + 1]) > 0) || (!ascending && string.Compare(arr[j], arr[j + 1]) < 0))
                    (arr[j + 1], arr[j]) = (arr[j], arr[j + 1]);
        return arr;
    }

    /// <summary>
    /// Selection sort algorithm to sort the array of strings.
    /// </summary>
    /// <param name="arr"></param>
    /// <returns></returns>
    public static string[] SelectionSort(this string[] arr, bool ascending = true)
    {
        // 1. Find the minimum element in the unsorted part of the array and swap it with the first element.
        for (var i = 0; i < arr.Length - 1; i++)
        {
            var minIndex = i;
            // 2. Find the minimum element in the unsorted part of the array.
            for (var j = i + 1; j < arr.Length; j++)
                // 3. Swap the elements if the current element is less than the minimum element.
                if ((ascending && string.Compare(arr[j], arr[minIndex]) < 0) || (!ascending && string.Compare(arr[j], arr[minIndex]) > 0))
                    minIndex = j;
            // 4. Swap the elements if the minimum element is not the first element.
            if (minIndex != i)
                (arr[minIndex], arr[i]) = (arr[i], arr[minIndex]);
        }
        return arr;
    }
}
