namespace Airlines.Business.Extensions;
public class SearchingExtensions
{
    /// <summary>
    /// Binary search algorithm to search for a string in an array of strings.
    /// </summary>
    /// <param name="arr"></param>
    /// <param name="key"></param>
    /// <returns></returns>
    public static string? BinarySearch(string[] arr, string key)
    {
        // 1. Initialize the low and high indices of the array.
        var low = 0;
        var high = arr.Length - 1;

        // 2. Search the array until the low index is less than or equal to the high index.
        while (low <= high)
        {
            // 3. Find the middle element of the array.
            var mid = (low + high) / 2;

            // 4. If the middle element is equal to the key, return the element.
            if (arr[mid] == key)
                return arr[mid];

            // 5. If the middle element is less than the key, search the right half of the array
            else if (string.Compare(arr[mid], key) < 0)
                low = mid + 1;

            // 6. If the middle element is greater than the key, search the left half of the array.
            else
                high = mid - 1;
        }

        // Return null if the key is not found in the array.
        return null;
    }

    /// <summary>
    /// Linear search algorithm to search for a string in an array of strings.
    /// </summary>
    /// <param name="arr"></param>
    /// <param name="key"></param>
    /// <returns></returns>
    public static string? LinearSearch(string[] arr, string key)
    {
        for (var i = 0; i < arr.Length; i++)
            if (arr[i] == key)
                return arr[i];
        return null;
    }
}