﻿using Airlines.Business.Dto;
using Airlines.Persistence.InMemory.Entities;
using AutoMapper;

namespace Airlines.Business.Mapping;
public class AirportMapper : Profile
{
    public AirportMapper() => CreateMap<Airport, AirportDto>().ReverseMap();
}