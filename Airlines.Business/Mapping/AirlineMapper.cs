﻿using Airlines.Business.Dto;
using Airlines.Persistence.InMemory.Entities;
using AutoMapper;

namespace Airlines.Business.Mapping;
public class AirlineMapper : Profile
{
    public AirlineMapper() => CreateMap<Airline, AirlineDto>().ReverseMap();
}