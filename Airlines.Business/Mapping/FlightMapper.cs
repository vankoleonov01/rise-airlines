﻿using Airlines.Business.Dto;
using Airlines.Persistence.InMemory.Entities;
using AutoMapper;

namespace Airlines.Business.Mapping;

public class FlightMapper : Profile
{
    public FlightMapper() => CreateMap<Flight, FlightDto>().ReverseMap();
}