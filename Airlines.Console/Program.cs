using Airlines.Business.Commands;
using Airlines.Persistence.InMemory;
using Airlines.Business.Dto;
using Airlines.Persistence.InMemory.Entities;
using Airlines.Persistence.InMemory.EntitiesRepository;
using Airlines.Persistence.InMemory.Management;
using Airlines.Persistence.InMemory.Models;
using Airlines.Persistence.InMemory.Models.Interfaces;
using Airlines.Business.Services;
namespace Airlines.Console;
public static class Program
{
    public static async Task Main()
    {
        var airportList = FileReader.ReadAirportsFromFile(@"../../../airport_in.csv");
        var airlineList = FileReader.ReadAirlinesFromFile(@"../../../airline_in.csv");
        var flightList = FileReader.ReadFlightsFromFile(@"../../../flight_in.csv");
        _ = FileReader.ReadAircraftsFromFile(@"../../../aircraft_in.csv");
        _ = new LinkedList<IFlight>();
        List<CargoAircraft>? cargoAircrafts = [];
        List<PassengerAircraft>? passengerAircrafts = [];
        var dataManagement = new DataManagement(airportList, airlineList, flightList, new LinkedList<IFlight>(), cargoAircrafts, passengerAircrafts);
        var route = new LinkedList<IFlight>();

        await using var dbContext = new RiseAirlinesDBContext();
        var flightRepository = new FlightRepository();
        var airportRepository = new AirportRepository();
        var airlineRepository = new AirlineRepository();

        while (true)
        {
            System.Console.WriteLine("\nCommands: \n(search <search term>) \n(sort <data type> [order]) \n(exist <airline name>) \n(list <input data> <from>) \n(route new) \n(route add <flight identifier>) \n(route find <destination airport> \n(route check <start airport> <end airport>) \n(route search <start airport> <end airport>) \n(route remove) \n(route print) \n(reserve cargo <Flight Identifier> <Cargo Weight> <Cargo Volume>) \n(reserve ticket <Flight Identifier> <Seats> <Small Baggage Count> <Large Baggage Count>)  \n(exit)\n \nEnter a command:");
            var command = System.Console.ReadLine();

            if (command != null)
            {
                var tokens = command.Split(' ');

                if (tokens[0].Equals("exit", StringComparison.CurrentCultureIgnoreCase) || tokens[0].Equals("quit", StringComparison.CurrentCultureIgnoreCase))
                {
                    System.Console.WriteLine("Exiting program...");
                    break;
                }

                try
                {
                    var commandObject = CommandFactory.CreateCommand(tokens, dataManagement, airportList, flightList, airlineList, cargoAircrafts, passengerAircrafts, route);
                    commandObject?.Execute(airportList, airlineList, flightList);
                }
                catch (Exception ex)
                {
                    System.Console.WriteLine($"Error: {ex.Message}");
                }
            }
        }
    }
}