# Airlines Rise Application

This is a simple console application for managing airports, airlines, and flights. It allows users to enter and validate data for airports, airlines, and flights, print the validated data, and search for specific data.

## Features

- **Data Entry:** Users can enter data for airports, airlines, and flights.
- **Validation:** Data entered by users is validated based on predefined rules for each entity type.
- **Data Printing:** Validated data for airports, airlines, and flights can be printed.
- **Search Functionality:** Users can search for specific data (airport, airline, or flight).

## Usage

1. Run the application.
2. Follow the prompts to enter data for airports, airlines, and flights.
3. Validated data will be printed.
4. Optionally, users can search for specific data.

## Requirements

- .NET Core 8.0

## How to Run

1. Clone the repository.
2. Navigate to the project directory.
3. Run the command `dotnet run` in your terminal.

## Contributing

Contributions are welcome! If you find any bugs or have suggestions for improvements, feel free to open an issue or create a pull request.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.
